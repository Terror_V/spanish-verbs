﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpanishVerbs.Test
{
    public static class SpanishDictTestResource
    {
        public static string Page = @"<!DOCTYPE html><html lang=""en"" data-placeholder-focus=""false""><head><meta charset=""utf-8""><link rel=""canonical"" href=""http://www.spanishdict.com/conjugate/tener""><link rel=""publisher"" href=""https://plus.google.com/106413713006018165072""><title>Tener Conjugation | Conjugate Tener in Spanish</title><meta name=""description"" content=""Conjugate Tener in every Spanish verb tense including preterite, imperfect, future, conditional, and subjunctive.""><meta property=""fb:app_id"" content=""123097127645""><meta property=""fb:admins"" content=""1675823057""><meta property=""og:type"" content=""article""><meta property=""og:site_name"" content=""SpanishDict""><meta name=""twitter:card"" content=""summary""><meta name=""twitter:site"" content=""@SpanishDict""><meta name=""twitter:creator"" content=""@SpanishDict""><meta property=""og:title"" content=""Check out the conjugation for &quot;tener&quot; on SpanishDict!""><meta property=""og:description"" content=""Find out why SpanishDict is the web's most popular, free Spanish translation, dictionary, and conjugation site.""><meta property=""og:image"" content=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon.png""><meta property=""og:url"" content=""http://www.spanishdict.com/conjugate/tener?utm_source=social&amp;utm_medium=facebook&amp;utm_campaign=share""><meta name=""apple-itunes-app"" content=""app-id=332510494, affiliate-data=at=1010l3IG&amp;ct=Smart%20Banner&amp;pt=300896, app-argument=http://www.spanishdict.com/conjugate/tener""><meta name=""google-play-app"" content=""app-id=com.spanishdict.spanishdict&amp;referer=utm_source%3Dsmart-banner""><meta name=""turbolinks-cache-control"" content=""no-cache""><meta name=""google"" content=""notranslate""><link rel=""icon"" sizes=""192x192"" href=""//n1.global.ssl.fastly.net/img/common/apple-touch-icons/apple-touch-icon.png""><link rel=""apple-touch-icon"" href=""//n1.global.ssl.fastly.net/img/common/apple-touch-icons/apple-touch-icon.png""><script data-turbolinks-eval=""false"">(function(global) {

  getCurrentTime = function() {
    if (window.performance && window.performance.now) {
      return Math.round(window.performance.now());
    }
    return Date.now();
  }

  global.SD_ENV                = 'production';
  global.SD_LOG_LEVEL          = 'warn';
  global.SD_GA_UA              = 'UA-329211-2';
  global.SD_MP                 = '0d9d3502c20e893b403e6018cccf0388';
  global.SD_GA_IS_MOBILE       = 'false';
  global.SD_NO_ADS             = 'false' === 'true';
  global.SD_FULLSTORY          = 'false' === 'true';
  global.SD_TEST_GROUP         = '100';
  global.SD_SERV_GROUP         = 57;
  global.SD_GEO_CODE           = 'BG';
  global.SD_MEGAEXAMPLES_HOST  = 'translate1.spanishdict.com';
  global.SD_TURBOLINKS         = 'true' === 'true';
  global.DNS_REFRESH           = '' === 'true';
  global.PO_URL                = 'po-intake2.spanishdict.com';
  global.SD_LANG               = 'en';  // UI lang.
  global.SD_IS_MOBILE          = 'false' === 'true';
  global.SD_AD_DOCSTART        = getCurrentTime();
  global.SD_AD_EXPERIMENT      = 'whitelist-stop';
  global.SD_AD_CONFIG={
    isMobile: false,
    deviceType: ''
  };
  global.SD_MAX_QUERY_LENGTH   = 500;
})(this);</script><script>(function() {
  window.SD_PAGE_CATEGORY          = 'conjugation';
  window.SD_DATE_TIME              = '2018-02-03T12:29:00+00:00';
  window.SD_IS_INITIAL_VISIT       = 'false';
  window.SD_STEALTH_FETCH_EXAMPLES = 'false' === 'true';
  window.SD_DEFAULT_FROM_LANG      = 'en';
  window.SD_QUICKDEF               = 'to have';
  window.SD_MT_KEY                 = '';
  window.SD_AD_TEST                = '';
  window.SD_AD_LIST                = [""adTop"",""adSide1"",""adSide2"",""adSideLong"",""adContent"",""adBot""];
  window.SD_USER_SURVEY            = '' === 'true';
  window.SD_NPS_SURVEY             = 'false' === 'true';
  window.SD_WOTD_OVERLAY           = 'false' === 'true';
  window.SD_WOTD_BANNER            = 'false' === 'true';
  window.SD_TRANSLATION_IMAGE_PROPS = {};
})();</script><script src=""//n1.global.ssl.fastly.net/dist/desktop-commons-min-0b81d4de1979b18fca0f559fd9d6d3ad.js"" type=""text/javascript"" crossorigin=""anonymous"" defer data-turbolinks-track=""reload""></script><script src=""//n1.global.ssl.fastly.net/dist/desktop-min-9d5d86536c1e90a78802a2a6cb726951.js"" type=""text/javascript"" crossorigin=""anonymous"" defer data-turbolinks-track=""reload"" class=""sd-app-bundle""></script><script src=""https://cdn.speedcurve.com/js/lux.js?id=320032"" id=""sd-lux"" async defer></script><script>var LUX = LUX || {};
if (false) {
  LUX.samplerate = 100;
} else {
  LUX.samplerate = 7;
}

var SD_platform = false ? 'mobile' : 'desktop';
LUX.label = SD_platform + '_conjugation';

var sdLux = document.getElementById('sd-lux');
sdLux.onload = function sdSendLuxData() {
  var isHTTPS = 'https:' === document.location.protocol;
  LUX.addData('is_https', isHTTPS);
  LUX.addData('environment', 'production');
  LUX.addData('is_mobile', 'false');
  LUX.addData('is_initial_visit', window.SD_IS_INITIAL_VISIT);
  LUX.addData('test_group', '100');
  LUX.addData('no_ads', 'false');
  LUX.addData('is_rw_proxy', 'false');
  LUX.addData('cdn_shootout', 'false');
  LUX.addData('nginx_cache', '');
  LUX.addData('conj_translations', '');
}</script><link rel=""dns-prefetch"" href=""https://n1.global.ssl.fastly.net""><link rel=""dns-prefetch"" href=""http://cdn1.spanishdict.com""><link rel=""dns-prefetch"" href=""http://cdn.mxpnl.com""><link rel=""dns-prefetch"" href=""http://api.mixpanel.com""><link rel=""dns-prefetch"" href=""http://www.google-analytics.com""><link rel=""dns-prefetch"" href=""http://translate1.spanishdict.com""><link rel=""dns-prefetch"" href=""http://spanishdict-d.openx.net""><meta name=""viewport"" content=""width=1024""><script src=""//n1.global.ssl.fastly.net/dist/main-min-4fdabf38c0781c04708c73a08eacdf82fb4de877-desktop.js"" crossorigin=""anonymous"" async data-turbolinks-track=""reload""></script><link rel=""preload"" href=""//spanishdict-d.openx.net/w/1.0/jstag?nc=1027916-SpanishDict"" as=""script""><link rel=""preload"" href=""//c.amazon-adsystem.com/aax2/apstag.js"" as=""script""><link rel=""preload"" href=""//n1.global.ssl.fastly.net/main/prebid-min-0-28-0.js"" as=""script"" crossorigin=""anonymous""><style type=""text/css"">.hidden{display:none}
/*! normalize.css v3.0.0 | MIT License | git.io/normalize */html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,details,figcaption,figure,footer,header,hgroup,main,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background:transparent}a:active,a:hover{outline:0}abbr[title]{border-bottom:1px dotted}b,strong{font-weight:700}dfn{font-style:italic}h1{font-size:2em;margin:.67em 0}mark{background:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:1em 40px}hr{box-sizing:content-box;height:0}pre{overflow:auto}code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em}button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0}button{overflow:visible}button,select{text-transform:none}button,html input[type=button],input[type=reset],input[type=submit]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}input{line-height:normal}input[type=checkbox],input[type=radio]{box-sizing:border-box;padding:0}input[type=number]::-webkit-inner-spin-button,input[type=number]::-webkit-outer-spin-button{height:auto}input[type=search]{-webkit-appearance:textfield;box-sizing:content-box}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{border:0;padding:0}textarea{overflow:auto}optgroup{font-weight:700}table{border-collapse:collapse;border-spacing:0}td,th{padding:0}@media print{*{text-shadow:none!important;color:#000!important;background:transparent!important;box-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:"" ("" attr(href) "")""}abbr[title]:after{content:"" ("" attr(title) "")""}a[href^=""#""]:after,a[href^=""javascript:""]:after{content:""""}blockquote,pre{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}img,tr{page-break-inside:avoid}img{max-width:100%!important}h2,h3,p{orphans:3;widows:3}h2,h3{page-break-after:avoid}select{background:#fff!important}.navbar{display:none}.table td,.table th{background-color:#fff!important}.btn>.caret,.dropup>.btn>.caret{border-top-color:#000!important}.label{border:1px solid #000}.table{border-collapse:collapse!important}.table-bordered td,.table-bordered th{border:1px solid #ddd!important}}.tooltip{position:absolute;z-index:1030;display:block;visibility:visible;font-size:12px;line-height:1.4;opacity:0;filter:alpha(opacity=0)}.tooltip.in{opacity:.9;filter:alpha(opacity=90)}.tooltip.top{margin-top:-3px;padding:5px 0}.tooltip.right{margin-left:3px;padding:0 5px}.tooltip.bottom{margin-top:3px;padding:5px 0}.tooltip.left{margin-left:-3px;padding:0 5px}.tooltip-inner{max-width:200px;padding:3px 8px;color:#fff;text-align:center;text-decoration:none;background-color:#3e86c7;border-radius:4px;font-weight:400}.tooltip-arrow{position:absolute;width:0;height:0;border-color:transparent;border-style:solid}.tooltip.top .tooltip-arrow{bottom:0;left:50%;margin-left:-5px;border-width:5px 5px 0;border-top-color:#3e86c7}.tooltip.top-left .tooltip-arrow{bottom:0;left:5px;border-width:5px 5px 0;border-top-color:#3e86c7}.tooltip.top-right .tooltip-arrow{bottom:0;right:5px;border-width:5px 5px 0;border-top-color:#3e86c7}.tooltip.right .tooltip-arrow{top:50%;left:0;margin-top:-5px;border-width:5px 5px 5px 0;border-right-color:#3e86c7}.tooltip.left .tooltip-arrow{top:50%;right:0;margin-top:-5px;border-width:5px 0 5px 5px;border-left-color:#3e86c7}.tooltip.bottom .tooltip-arrow{top:0;left:50%;margin-left:-5px;border-width:0 5px 5px;border-bottom-color:#3e86c7}.tooltip.bottom-left .tooltip-arrow{top:0;left:5px;border-width:0 5px 5px;border-bottom-color:#3e86c7}.tooltip.bottom-right .tooltip-arrow{top:0;right:5px;border-width:0 5px 5px;border-bottom-color:#3e86c7}.icon-menu-nav-selector{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAOCAMAAAD32Kf8AAAABlBMVEUAAAD///+l2Z/dAAAAAXRSTlMAQObYZgAAABNJREFUeAFjYMQGcIjSCAw1NwAATQAAf4ykiYAAAAAASUVORK5CYII=) no-repeat}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.icon-menu-nav-selector{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAcCAMAAAAgPt3ZAAAABlBMVEUAAAD///+l2Z/dAAAAAXRSTlMAQObYZgAAABlJREFUeAFjYCQaDLxSmoBRMJoGhhIYTQMApiAB+SgxINUAAAAASUVORK5CYII=) no-repeat;background-size:23px 14px}}.search-expand-icon{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAPCAMAAADXs89aAAAA2FBMVEUAAAD///+qqqqqqqqzs7PMzMyxsbGqqqqqqqqtra2np6enp6eqqqqoqKiqqqqmpqaqqqrIyMKmpqanp6elpaWnp6enp6enp6elpaWoqKimpqampqanp6enp6elpaXFxcWnp6empqanp6enp6empqanp6enp6empqanp6elpaWnp6elpaWlpaWlpaXExMLFxcOmpqalpaWlpaWlpaWmpqampqalpaWmpqalpaXExMOlpaWlpaWlpaWmpqampqampqalpaWlpaXFxcOmpqampqalpaWlpaXExMLuhOl0AAAARnRSTlMAAQMGCgoNDxIZGiAhIycoKiouNDY3OkNESU1QVF1eYGJkZWhwcXR4fICFmZyfoKq9wcTHycvNzs/V2N7g5Ort8vX19vz9AmRq2AAAAJ9JREFUGNONzsd6AQEUQOFDDKKX6J0QdfTeGfe+/xtZiHwyLJzlvzoAOGdrH8997yMv1D1K8nbF6eE4zdm1KpN2a2OFbDzffsCXlAFMvWVCJgakpQ5gLFVVdWkA/lpnbN2Y6ElVT1Ggaa069dQvU1LVEhCUngM+74ypJkBW8kDhj42+ARCWYSLe2F263v+DP2eRRWwgFdu4K+wBZ8DxaFerKxVML0tBOwAAAABJRU5ErkJggg==) no-repeat}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.search-expand-icon{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAeCAMAAABg6AyVAAABaFBMVEUAAAD///////+qqqq/v7/MzMy/v7+qqqqzs7PMzMyqqqqxsbG2travr6+mpqampqaqqqqtra2qqqqlpaWqqqqlpaWoqKimpqaoqKiqqqrIyMKqqqqoqKinp6eoqKilpaWnp6enp6empqanp6empqapqammpqalpaWnp6eoqKimpqaoqKinp6elpaWmpqanp6empqbFxcWnp6elpaWnp6empqampqanp6empqampqanp6elpaWmpqampqampqalpaWlpaWlpaWlpaWnp6elpaWmpqampqbExMKlpaWmpqampqalpaWmpqalpaWlpaWmpqampqampqalpaWmpqampqalpaWmpqalpaWlpaWmpqalpaXExMOlpaWlpaWmpqalpaWlpaWmpqampqalpaWmpqampqampqalpaWmpqampqalpaWlpaWmpqalpaXFxcOlpaWmpqalpaWmpqampqalpaWmpqalpaXExMLRPWNfAAAAdnRSTlMAAQIDBAUICQoKDA0OEBQXGBweIiQlJigpKiotLzEyMzQ3OTo8QUVHS0xQUlRVWVpfYGNmaG1wcXV7fH2BhIeIi46Rk5mam6Cio6eoqa22t7jAxMXGx8jKz9DV1djb3N3e3+Lj5OXo6evt7/Hz9fX3+fr7/P3+vDKm9wAAAVpJREFUeAG11Ed7ElEchfFDsCT2EnVSNBp7NOrETmKJxbGLHQV7EURBGHn9+j5y4f/MZTHMwvzWZ/Gujox2F4vF08om9wyerlE2p+DNJmWz4SvVcWW0THO/Mppod+aV1fqtW7RS8iPKZtuVF5/j37XSUqBhRm7E9LSOaYjrALVHD98D7VmlOgqdQpCTdKgOl9QX/RkQSboGV+Wcg8c2Hiv72/KYpFdwQM4++Cgz2UhuG5OSNDU9vUrOWW+sMDkOlZQPlur+WJEf7IwevHi31ASwsWVbsDN+6xdd3+7Y2LIt2JltApWbF+Z25qdsbNl+8OYqlCbU5caeyAs+AfW1cmZs7GVbsJbhgXrO2DiZbcHSbbgnZ+MH+KRBYShzHr7v0D+7ngA/lGZPB6qX548X7rf4EsPiEaUo0Pdu+1vgpdIsVADi14ujWvgJz5UqFxw+OeOea93c3tX6L/4Cy0iHCMwk5ywAAAAASUVORK5CYII=) no-repeat;background-size:23px 14px}}.no-touch .search-expand-icon:hover{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAPCAMAAADXs89aAAAAzFBMVEUAAAD///9Vqv9VqtVNmcw7icREiMxHjsY9hcw7icRAh8c+g8k6g8U7g8RAhsY9hsg9hcg7hMQ9hMY8hsc+hMY9hcY8g8c7hcc8hMc9g8Y9g8U8hMU8gsY6gsU8gsY7gsQ6g8U7hMQ7hMY7g8Q7hMY8hMY8hMU8g8U7gsU6gsU7g8Q7hMU7g8Q7g8U6g8U6g8U7gsQ7g8Q6g8U7g8U6g8U6g8U7g8U7gsQ7g8U7g8Q6g8U6g8U7g8Q6gsU7g8U6gsU6g8U7g8Q6gsU6gsQPeIbCAAAAQ3RSTlMAAQMGCg0PEhkaICEjJygqLjQ2NzpDRElNUFRdXmBiZGVocHF0eHyAhZmcn6CqvcHEx8nLzc7P1dje4OTq7fL19vz9PTX78wAAAJxJREFUeAGNyNlWglAYgNEPOs0FNA9FQ1SKHgdFxRnxf/938rBQl8iN+3KTsXujc8p+Zi5lx+17DvYezZPohT1f0q1Wxuk1Rf3JEdzKB4aWnIYnD3iUAEPFkokVcPEddtK8cROziQv8p8MweFg3vmkfuJK6BWebRovGeJZX4G3bqqEwHGnd3fxOl7VTCv4WIgOvKZ8UKecE7EuLHSsIfBQ22RT97wAAAABJRU5ErkJggg==) no-repeat}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.no-touch .search-expand-icon:hover{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAeCAMAAABg6AyVAAABWVBMVEUAAAD///+A//9Vqv9Av/9mmcxAn99VjsZNmcxAldU7icRJkshAj89AjMxDhchAispAicg8iMw8h8tAh8Y+g8g8hslAhsY+g8c9hsg+gsY8gsk+gsY9hcc8gsg7hMQ8hsc6hsU+hMY8hMg7hcQ7hcg9hcY9hcU8g8Y9g8Y7g8c9g8U8hMY8hMY8gsY7hMc6gsU7g8Y8gsY7hMQ6g8U7hMY7g8Q7g8Q6g8U8hMU7g8Y7gsY8g8U7gsQ6g8U7gsQ7g8Y6gsU7gsQ6gsU8g8U7hMU7g8Q6g8U7g8U6g8U7g8U7g8Q7g8Q7gsQ7g8Q6gsU6gsU7gsQ6g8U7gsU7g8Q7gsQ6gsU7g8U7g8Q7gsQ7g8U6gsU7g8U7gsQ7g8Q6gsU7g8Q6gsU6g8U7gsU6g8U6gsU7gsQ6gsU7gsQ6gsU7gsQ6gsU7gsQ6gsU6g8U7gsQ7g8Q6gsU6g8U6gsTm4RaQAAAAcnRSTlMAAQIDBAUICQoMDQ4QFBcYHB4iJCUmKCkqLS8xMjM0Nzk6PEFFR0tMUFJUVVlaX2BjZmhtcHF1e3x9gYSHiIuOkZOZmpugoqOnqKmttre4wMTFxsfIys/Q1djb3N3e3+Lj5OXo6evt7/Hz9ff5+vv8/f7QzXWQAAABUUlEQVR4AbXO2fcRcQCG8Rcl7YtqUEq7Uo12tGhp2tMu2pdIxOT5/y86mvFlOGfMRb/P9fue82hmX6PROKdoYi/hRVLRnIV3WxXN5u900oqozuCQIsqMxiVFtWnHdq2VRFzR7Lz++qv7p9usWVolftvFNzypFW4BdJ8++QiMCgp1AsYVKybpaA+uasphgSPpJtyQ5yI8M+NUi4BWStJbOCzPQfgsI9NnTj8jSdlcbp08FwJj2cyxNS9h1XpmvJTtzPKOXHnQHABmbLJnwZ703d/88+O+GZtsE+wpDID2ncvFPYmsGZvsYPC2DjT94/JYTiD4NPQ2yJM340C2CVYdHst33h8Hs02wdA8eyrPlE3zRItuWcQl+7tbE3ufAL4XZP4bOtdKpyqMh31yoHleIClMfdr0H3ihMuQ3gtqoplfvwSqFi1rEz+aQmNhYPrNd/8Rds5oITUoO8/QAAAABJRU5ErkJggg==) no-repeat;background-size:23px 14px}}.accents-audio-btn:hover,.audio-blue{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAMAAABhEH5lAAABHVBMVEUAAAD///+A//9Vqv9Av/9mmcxJkttNmcxGi9FOidhEiMxHjtVAjMxAispFic5CjtBGjcpGi8lAh80+ishAjMw+icdDi8hBiMk+h8tBhsdCiMpBiss/islCiMo/ichAh8o+hshAhsk/iMo+hsdAiclAh8pAiMhAh8c/hshAh8k/h8g/iMk+iMhAhsg/h8k+h8c/hsc+iMg/h8g/h8g/h8g/hsg/iMg/h8g/h8g/hsc/h8g/hsg+h8g/h8c/hsg/h8c+h8g/h8c/h8g+h8c/h8g+h8g/h8c/h8g/h8g+h8g+hsg/h8g+hsg/hsc+hsg+h8c+hsc/hsg+hsg+h8c/h8g+hsg+h8c/hsc/h8c+hsg+h8g/hsc/h8c+h8g+hsdP+EQhAAAAXnRSTlMAAQIDBAUHCgsNDxIUGBobHSEkJSgpLi8xNzo7PT5FSEpMTU5QW1xgYWhqbW9wdXeJi5GVmZqen6Glpqesrq+yubvDxcfIysza3d7f4uTm6erv8fL09fb3+Pn6+/z+u9h0pwAAAMhJREFUeAFdzOc6w2AcQPGTaDW19x6i1B5V1B7UqEESESTyv//LkDdPHvI6H38fDlkj97voDTvBhy4DL9dbMfSVSTNLpcGnZnk1oWWv1g4s+SJy24Gi7r3oqhNe67Y9Z6HIsLA/L0BmUSkqPvSzJpU8tbk3FJ/P8sSmDHHS0mhBpqh7GjW+urhsElX+9lKjN9jh7rxanTdSKmybxrHfw8RjGMb7piKwGrJO2sz7UUHRpPO9Qtb02+lGQosHo/w27roB/xprHZL1A4v0IhonIiOyAAAAAElFTkSuQmCC) no-repeat}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.accents-audio-btn:hover,.audio-blue{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAMAAADW3miqAAABp1BMVEUAAAD///9Vqv9Av/9mmcxVqtVJkttAn99VjuNNmcxGi9FOidhJkshAj89Lh9JHjtVJks5DkMhHj8xFic5AichEiMxAh8dAh80+ishBictAjMxDjMhBiMo+iMxDi8hBiMlCisxAiclCictBhsdAicg/hslCiMpBistAiMhBispCh8lBiMlAh8o/iMc+icdBh8g/h8k/h8k+iMlAh8pAh8dAh8k/h8g/iMdAiMg/iMlAhshAhsg/h8k+h8c+h8c/h8g+h8k/h8g/h8g+hsg/hsc/h8hAh8g+iMg/h8g/h8g+hsg/h8g/hsg/h8c/h8g+h8c/h8g/h8g+h8g/h8g+h8g/hsg/hsg+h8g/h8g+h8g/h8g/h8g+h8g/hsg/h8c+hsc+h8g/hsg/h8g+hsc/h8g+hsc/h8c/h8g/h8c/h8g+hsg/h8c/hsc+hsg/h8g/h8g+h8g/h8c/hsc/h8g+hsg/hsc+h8c/h8g/hsg+h8c+hsc/h8g/h8g+hsc+h8c/hsg/h8g+hsg+h8c/hsc/h8g+hsg/hsc+h8g/hsc/h8c+hsg+h8g+hscj+L1uAAAAjHRSTlMAAQMEBQYHCAkKCw0OEBESFRcZGhweICQlJygqKy0uLzI0Njc4OTo7PD9CR0hJUlNVWVpbYGRmaWttcHR1d3t9f4KGh4mKjI+RlZiZmpudoKGjqKqsra+wsbS1trm6u7y9wsPExsnKzM7Q0dLT1dbY2dvc3+Lk5efo6err7O3u7/Dx8vP09ff6+/z9/s65uSMAAAGqSURBVHgBhdNXVxNBHIbxN2Zl2agYsYu9FwV7F0EjAnZiERURLEo0KgoYNICFECXPh3b2nICTmFl+N+/Nc/U/M6oSb89lmhQtfh94qkiLegCmFCV2Gyby/IlsrsHU1mx01Anft2kuam2uq9Gk4McOzUeDjJ9ZrHlBMtQOv3bLjuDDlnJyMDtLCKb3yoqO54CZowrtKVFW2Cc7UnBhEmaPyOijeKc71LVLdhRaPwa/d0r6Ro9s5cjbJGPtCAwnJLhaK0oU22SsK0CrO4JLMs7D1yAiotlsMAktrijWDxkZXXDdFWnVNIS3bYEhZ6S7cNLMRsi5oxRcNtMIRXd0E86GR4BxZ1Q3BofM7oe3zqgDvvhm2+CWK6ovwTkZ7+CYJkg7jvkobvYA/FymF4x6NaO+hBn/I3RIpyDzJHTRsyP/dEySdw/yK6T6N5T1+tXvye8FDstouDFSMIrQH1RGmweAlCyrP8HgEitqSpeATlVY+R5eNvyLBoCZE6qSzMBQ0v5SDzfoP8tfQbZxLnrQvV21LH0Ow6NhFCXxDOCzovmPgbQW4F3Jv16jKn8Bv/7Ify0kGh4AAAAASUVORK5CYII=) no-repeat;background-size:19px 19px}}.video-blue{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAPCAMAAAAxmgQeAAAAJ1BMVEUAAAD///9OidhDhslAhslAh8c/h8g/hsc+h8g/h8g/h8g+hsg+hscVIrrwAAAADHRSTlMAAQ0mX2CKkrS15/mtjJMbAAAAOElEQVQY02Pg4EEHHAw8DOiAh1QxTmZMMR5uNiYMMR4eLhZGDDEeHnZMdayMhM3DZi9Z/uDEDBcAUFwD8jVe+p0AAAAASUVORK5CYII=) no-repeat}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.video-blue{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAeCAMAAAB3ypxcAAAATlBMVEUAAAD///9VqtVOidhAispDhslCictAh8o/iMc+hshAhslAh8c/h8k/h8g/h8g+h8c/h8g+hsc+h8g+hsg/h8g/h8g+h8c+hsg+h8g+hseMc/P/AAAAGXRSTlMAAQYNGCY2SElKX2B1ip+gtcnZ4uPn8vn+VSKcpQAAAGBJREFUOMvd1EkOgCAQRFFQFJwHQPn3v6hHoBasrPVLOulKt/GJalIwESHRgKkG/sfOQWK8W68wyLNVGFyjxCi7Uxg8a6cwuKd2TBpaDtdqIXmxrcoSq//zLYjPISivxn/Y6CC7U4b0CgAAAABJRU5ErkJggg==) no-repeat;background-size:19px 15px}}.audio-start{cursor:pointer}.audio-start.speaker-playing .audio-blue{background:url(//n1.global.ssl.fastly.net/img/desktop/speaker.gif);background-size:19px 19px;background-repeat:no-repeat;background-position:0 0}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.audio-start.speaker-playing .audio-blue{background:url(//n1.global.ssl.fastly.net/img/desktop/speaker@2x.gif);background-size:19px 19px;background-repeat:no-repeat;background-position:0 0}}.audio-start.speaker-playing .audio-blue-small,.audio-start.speaker-playing .audio-blue-small-variation-message{background:url(//n1.global.ssl.fastly.net/img/desktop/speaker-small.gif);background-size:13px 13px;background-repeat:no-repeat;background-position:0 0}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.audio-start.speaker-playing .audio-blue-small,.audio-start.speaker-playing .audio-blue-small-variation-message{background:url(//n1.global.ssl.fastly.net/img/desktop/speaker-small@2x.gif);background-size:13px 13px;background-repeat:no-repeat;background-position:0 0}}a.audio-start:hover{text-decoration:none}.audio-blue{display:block;width:19px;height:19px}.audio-blue-small,.audio-blue-small-variation-message{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -1469px -148px no-repeat;background-size:1591px 271px;display:inline-block;vertical-align:middle;min-width:14px;width:14px;height:13px;margin:0 5px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.audio-blue-small,.audio-blue-small-variation-message{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -1469px -148px no-repeat;background-size:1591px 271px}}.audio-blue-small-variation-message{margin:0;position:relative;top:-2px}.video-player{padding-top:10px;padding-bottom:10px}.video-player span{display:none}.video-blue{width:19px;height:19px}.media-links{display:inline-block;line-height:26px}.media-links a{display:inline-block;margin-left:13px}html{font-family:Helvetica Neue,Helvetica,Arial,sans-serif;-webkit-font-smoothing:antialiased}a{text-decoration:none;color:#3e86c7}a:hover{text-decoration:underline}body{background-color:#f7f7f7;min-width:0}@media only screen and (min-width:415px){body{min-width:1024px}}@media (min-width:1280px){body{min-width:1280px}}input:focus,textarea:focus{outline:none}textarea{border-style:none;border-color:Transparent;overflow:auto}.js-transition-off-during-page-load *{-webkit-transition:none!important;transition:none!important}.container{width:972px;padding:0 26px;margin:0 auto}@media (min-width:1280px){.container{width:1160px;padding:0 60px}}.card{background-color:#fff;border-radius:4px;border:1px solid #b0b0b0;padding:20px;font-size:14px;line-height:24px;margin-bottom:15px;color:#000;position:relative}.card-title{font-size:21px;font-weight:700;line-height:normal;margin-bottom:20px;display:block}a.card-title{color:#000}.card-spacer{margin-bottom:15px}.card-button-full{text-align:center;display:block;padding-top:20px;border-top:1px solid #b0b0b0;line-height:normal;margin:20px -20px 0;cursor:pointer}.card-tab{margin-top:0;padding-top:16px}.card-announcement,.card-tab{border-top:none;border-top-left-radius:0;border-top-right-radius:0}.card-description{font-size:14px;color:#a5a5a5;line-height:normal}.card-breadcrumb{border-top:none;border-top-left-radius:0;border-top-right-radius:0}.main-container{float:left;width:646px}@media (min-width:1280px){.main-container{width:800px}}.main-container-full{float:left;width:972px}@media (min-width:1280px){.main-container-full{width:1160px}}.sidebar-container{float:right;width:300px}.fluencia-sidebar-short{padding:10px}.fluencia-sidebar-title-short{display:inline-block;color:#333;font-size:14px;font-weight:700;height:30px;line-height:30px}.fluencia-sidebar-button-short{background-color:#3e86c7;color:#fff;text-align:center;line-height:30px;width:100px;height:30px;border-radius:4px;cursor:pointer;transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-webkit-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-moz-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-ms-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-o-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;float:right}.fluencia-sidebar-button-short:hover{background-color:#377ebe;box-shadow:inset 0 -2px rgba(0,0,0,.1);text-decoration:none}.wotd-sidebar .audio-start{display:inline-block;padding-top:0;padding-bottom:0;vertical-align:text-bottom}.wotd-sidebar-date{font-size:12px;font-weight:700;color:#a5a5a5;text-transform:uppercase;position:absolute;top:24px;right:20px}.wotd-sidebar-word{color:#000;font-size:18px;display:inline-block;padding-bottom:0}.wotd-sidebar-audio{margin-left:8px}.wotd-sidebar-translation{color:#3e86c7;line-height:normal;margin-bottom:20px}.wotd-sidebar-signup{border-radius:4px;display:block;background-color:#3e86c7;font-size:14px;line-height:40px;color:#fff;text-align:center;font-weight:700}.wotd-sidebar-signup:hover{text-decoration:none}.social-sidebar .sidebar-title{margin-bottom:20px}.social-sidebar-button{text-align:center;height:40px;width:32.5%;margin:0 auto;border-radius:2px;cursor:pointer;transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-webkit-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-moz-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-ms-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-o-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out}.social-sidebar-button.facebook{background-color:#3b5998;float:left}.social-sidebar-button.facebook:hover{background-color:#344e86;box-shadow:inset 0 -2px rgba(0,0,0,.1)}.social-sidebar-button.twitter{background-color:#55acee;display:block}.social-sidebar-button.twitter:hover{background-color:#3ea1ec;box-shadow:inset 0 -2px rgba(0,0,0,.1)}.social-sidebar-button.gplus{background-color:#dd4b39;float:right}.social-sidebar-button.gplus:hover{background-color:#d73925;box-shadow:inset 0 -2px rgba(0,0,0,.1)}.social-sidebar-button .social-logo{display:inline-block}.social-logo{width:21px;height:21px}.social-facebook-logo{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -1491px 0 no-repeat;background-size:1591px 271px;margin:10px auto 0 16px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.social-facebook-logo{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -1491px 0 no-repeat;background-size:1591px 271px}}.social-twitter-logo{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -1515px 0 no-repeat;background-size:1591px 271px;margin:12px auto 0 2px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.social-twitter-logo{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -1515px 0 no-repeat;background-size:1591px 271px}}.social-gplus-logo{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -1548px 0 no-repeat;background-size:1591px 271px;margin:12px auto 0 6px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.social-gplus-logo{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -1548px 0 no-repeat;background-size:1591px 271px}}.ad-wrapper{display:inline-block;text-align:right}.ad-centered{position:relative;-webkit-transform:translate(-50%);transform:translate(-50%);left:50%}.ad-unit{background-color:#eee}.ad-wrapper{margin-bottom:15px}.sd-ad-container{clear:both}.adTop.ad-unit{min-width:728px;height:90px;overflow:hidden}.adTop.ad-wrapper{position:relative;-webkit-transform:translate(-50%);transform:translate(-50%);left:50%;margin-top:15px}.adSide1.ad-unit,.adSide2.ad-unit{min-width:300px;min-height:250px}.adSideLong.ad-unit{min-width:160px;min-height:250px}.adContent.ad-unit{min-width:300px;min-height:250px}.adContent.ad-wrapper{position:relative;-webkit-transform:translate(-50%);transform:translate(-50%);left:50%}.adBot.ad-unit{min-width:728px;min-height:90px}.adBot.ad-wrapper{position:relative;-webkit-transform:translate(-50%);transform:translate(-50%);left:50%}.accents{color:#a5a5a5}.accents-letter{padding:0 16px;cursor:pointer}.accents-letter:first-child{padding-left:8px}.no-touch .accents-letter:hover{color:#3e86c7}.accents-descender{position:relative;bottom:3px}.accents-audio-btn{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -953px -148px no-repeat;background-size:1591px 271px;width:19px;height:19px;margin-left:10px;cursor:pointer}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.accents-audio-btn{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -953px -148px no-repeat;background-size:1591px 271px}}.search{margin:0 auto 15px}.search .accents{display:none;position:absolute;top:16px;left:10px;margin-top:127px}.search-tagline{color:#fff;display:block;margin:18px auto;font-size:22px;font-weight:700;text-align:center}.search-tagline .span{display:inline}.search-tagline.tagline-dynamic-text{font-size:16px;margin-top:10px;margin-bottom:35px}.search-component{margin:0 auto}.search-form{width:100%}.search-form-input{-webkit-appearance:none;display:block;width:100%;*width:480px;height:53px;*height:34px;box-sizing:border-box;margin:auto;padding:8px 154px 10px 10px;resize:none;font-size:18px;line-height:36px;color:#585858;letter-spacing:.025em;overflow-y:auto;border-radius:4px;border:none}.search-form-input::-moz-placeholder{color:#a5a5a5;opacity:1}.search-form-input:-ms-input-placeholder{color:#a5a5a5}.search-form-input::-webkit-input-placeholder{color:#a5a5a5}.search-form-input:-moz-placeholder,.search-form-input::-moz-placeholder{color:#a5a5a5;font-size:""100%""}.search-form-input:-ms-input-placeholder{color:#a5a5a5;font-size:""100%""}.search-form-input::-webkit-input-placeholder{color:#a5a5a5;font-size:""100%""}.search-form-input.has-dropdown{border-bottom-right-radius:0;border-bottom-left-radius:0}.search-form-parts{background-color:#fff;border:1px solid #b0b0b0;border-radius:4px;position:relative;margin:0 auto}.search-form-parts.search-expand{height:180px}.search-form-parts.search-expand.has-dropdown{border-bottom:1px solid #b0b0b0!important;border-bottom-right-radius:0;border-bottom-left-radius:0}.search-form-parts.search-expand .search-form-input{height:127px;display:block;padding-right:10px;border-bottom:1px solid #b0b0b0;line-height:26px;border-bottom-right-radius:0;border-bottom-left-radius:0}.search-form-parts.search-expand .search-collapse-icon,.search-form-parts.search-expand button[type=submit]{margin-top:127px}.search-form-parts.search-expand .search-expand-icon{display:none}.search-form-parts.search-expand .search-collapse-icon{display:inline-block}.search-form-parts.search-expand .accents{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}.search-form-parts.has-dropdown{border-bottom-right-radius:0;border-bottom-left-radius:0}.search-collapse-icon,.search-expand-icon{position:absolute;top:19px;right:126px;width:23px;height:14px;margin-right:10px;cursor:pointer}.search-collapse-icon{display:none;background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -888px -148px no-repeat;background-size:1591px 271px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.search-collapse-icon{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -888px -148px no-repeat;background-size:1591px 271px}}.no-touch .search-collapse-icon:hover{background-position:-918px -148px}.search-expand-icon{display:inline-block}.search-form button.search-form-button{-webkit-appearance:none;position:absolute;top:3px;right:3px;width:120px;height:47px;background-color:#54b948;border:none;border-radius:3px;overflow:hidden;color:#fff;font-size:18px;letter-spacing:.05em;transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-webkit-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-moz-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-ms-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-o-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out}.search-form button.search-form-button:hover{background-color:#4eaf43;box-shadow:inset 0 -2px rgba(0,0,0,.1)}.search-form button.search-form-button:focus{outline:0}.search-form button.search-form-button[disabled]:hover{background-color:#54b948;box-shadow:none}.char-limit{position:absolute;top:143px;right:143px;display:none}.gray-char-limit{color:#a5a5a5}.red-char-limit{color:red}#hidden-search{color:transparent;font-size:18px;cursor:default;position:absolute;letter-spacing:.025em;padding-left:10px}.menu-bar{position:relative;margin:0 -26px;z-index:1}.menu-container{width:200px;height:50px;-webkit-touch-callout:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;-webkit-tap-highlight-color:rgba(255,255,255,0);cursor:default}.menu-selector{height:40px;font-size:12px;letter-spacing:.075em;padding:10px 0 0 30px;text-transform:uppercase;cursor:pointer;font-weight:700;background-color:#3e86c7;position:relative;z-index:2}.menu-selector-text{display:inline-block;position:relative;top:-3px;padding:10px 0}.no-touch .menu-selector-text:after{-webkit-transform:translateZ(0);opacity:.7;display:inline-block;position:absolute;left:0;bottom:5px;content:"""";width:100%;height:2px;box-sizing:border-box}.no-touch .menu-selector-text:hover{text-decoration:none}.no-touch .menu-selector-text:hover:after{background-color:#fff}.menu{padding-top:5px;color:#fff;background-color:#3e86c7;font-size:12px;letter-spacing:.05em;text-transform:none;width:200px;position:absolute;z-index:1;transition:top .4s ease 0s;-webkit-transition:top .4s ease 0s;-moz-transition:top .4s ease 0s;-ms-transition:top .4s ease 0s;-o-transition:top .4s ease 0s}.menu a:hover{text-decoration:none}.menu-expanded .menu{top:0;margin-top:50px}.menu-collapsed .menu{top:-600px}.menu-expanded .menu-collapse{display:block}.menu-expanded .icon-menu-caret{display:none}.menu-item{padding:12px 30px;color:#fff}.menu-item:hover{background-color:#286fb7}.menu-divider{border-top:1px solid #508dc5}.menu-title{text-transform:uppercase;font-weight:700}.menu-bottom{padding:12px 0;margin-top:12px}.menu-collapse{cursor:pointer;position:absolute;left:157px;top:19px;background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -268px -87px no-repeat;background-size:1591px 271px;height:11px;width:11px;display:none}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.menu-collapse{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -268px -87px no-repeat;background-size:1591px 271px}}.no-touch .menu-collapse:after{-webkit-transform:translateZ(0);opacity:.7;display:inline-block;position:absolute;left:0;bottom:-3px;content:"""";width:100%;height:2px;box-sizing:border-box}.no-touch .menu-collapse:hover{text-decoration:none}.no-touch .menu-collapse:hover:after{background-color:#fff}.menu-nav .menu-container{position:absolute;left:0}.menu-nav .menu-selector-text{float:left}.icon-menu-nav-selector{top:-3px;float:left;display:inline-block;width:21px;height:14px;margin-right:14px;margin-top:7px}.menu-user{position:absolute;right:0}.menu-user-img{float:left;height:32px;width:32px;margin:0 5px 0 0;display:inline-block;position:relative;top:-2px;left:0}.menu-user-img,.menu-user-img img{border-radius:50%}.menu-user-img-default{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -763px -150px no-repeat;background-size:1591px 271px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.menu-user-img-default{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -763px -150px no-repeat;background-size:1591px 271px}}.menu-selector-text{max-width:75px;height:12px;overflow:hidden}.menu-user-name{left:4px;white-space:nowrap}.icon-menu-caret{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -69px -87px no-repeat;background-size:1591px 271px;width:11px;height:7px;position:absolute;right:37px;top:22px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.icon-menu-caret{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -69px -87px no-repeat;background-size:1591px 271px}}.menu-session-links{margin-right:31px;height:50px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:end;justify-content:flex-end;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;font-size:12px;font-weight:700;letter-spacing:.075em;text-transform:uppercase}.menu-session-links .menu-login,.menu-session-links .menu-signup{color:#fff;text-decoration:none}.menu-session-links .menu-login{margin-right:20px}.menu-session-links .menu-signup{padding:10px 20px;border:1px solid #fff;border-radius:4px}.menu-white .menu-selector{background-color:transparent}.menu-white .menu{color:#3e86c7;background-color:#fff}.menu-white .menu-item{color:#3e86c7}.menu-white .menu-item:hover{background-color:#eef7ff}.menu-white .menu-divider{border-color:#dadada}.menu-white .menu-collapse{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -285px -87px no-repeat;background-size:1591px 271px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.menu-white .menu-collapse{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -285px -87px no-repeat;background-size:1591px 271px}}.menu-white .menu-collapsed .menu-selector{transition:background-color .2s ease .2s;-webkit-transition:background-color .2s ease .2s;-moz-transition:background-color .2s ease .2s;-ms-transition:background-color .2s ease .2s;-o-transition:background-color .2s ease .2s}.menu-white .menu-collapsed .menu-selector-text{transition:color .2s ease .2s;-webkit-transition:color .2s ease .2s;-moz-transition:color .2s ease .2s;-ms-transition:color .2s ease .2s;-o-transition:color .2s ease .2s}.menu-white .menu-collapsed .icon-menu-nav-selector{transition:background .2s ease .2s;-webkit-transition:background .2s ease .2s;-moz-transition:background .2s ease .2s;-ms-transition:background .2s ease .2s;-o-transition:background .2s ease .2s}.menu-white .menu-expanded .menu-selector{color:#3e86c7;background-color:#fff}.menu-white .menu-expanded .icon-menu-nav-selector{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -35px -87px no-repeat;background-size:1591px 271px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.menu-white .menu-expanded .icon-menu-nav-selector{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -35px -87px no-repeat;background-size:1591px 271px}}.no-touch .menu-white .menu-collapse:after{-webkit-transform:translateZ(0);opacity:.7;display:inline-block;position:absolute;left:0;bottom:-3px;content:"""";width:100%;height:2px;box-sizing:border-box}.no-touch .menu-white .menu-collapse:hover{text-decoration:none}.no-touch .menu-white .menu-collapse:hover:after{background-color:#3e86c7}.no-touch .menu-white .menu-expanded .menu-selector-text:after{-webkit-transform:translateZ(0);opacity:.7;display:inline-block;position:absolute;left:0;bottom:5px;content:"""";width:100%;height:2px;box-sizing:border-box}.no-touch .menu-white .menu-expanded .menu-selector-text:hover{text-decoration:none}.header-wrapper,.no-touch .menu-white .menu-expanded .menu-selector-text:hover:after{background-color:#3e86c7}.header-wrapper{width:100%}.header-container{color:#fff;height:50px}.header-logo{display:block;position:relative;top:14px;margin:0 auto;width:151px;height:25px}.feedback-tab{display:none;z-index:1000}@media (min-width:1025px){.feedback-tab{display:block;position:fixed;top:188px;right:-80px;width:120px;height:120px;transform:rotate(-90deg);-webkit-transform:rotate(-90deg);-moz-transform:rotate(-90deg);-o-transform:rotate(-90deg);-ms-transform:rotate(-90deg)}.feedback-tab button{background-color:#b0b0b0;font-size:14px;color:#fff;border-top-right-radius:4px;border-top-left-radius:4px;border:none;outline:none;padding:12px 18px;transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-webkit-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-moz-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-ms-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-o-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out}.feedback-tab button:hover{background-color:#a8a8a8;box-shadow:inset 0 -2px rgba(0,0,0,.1);text-decoration:none}}.lg-article{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;border-bottom:1px solid #ededed}.lg-article-link{display:inline-block;overflow:visible;line-height:20px}.lg-article-container:last-child .lg-article{padding-bottom:0;border-bottom:none}.lg-quiz-link{border-radius:3px;background:#54b948;color:#fff;font-weight:700;text-transform:uppercase;padding:3px 5px;font-size:11px;position:relative;top:-1px}.lg-quiz-link:hover{text-decoration:none}.lg-article-icon{min-width:14px;height:16px;margin-right:10px}.lg-articles .lg-article-container:not(first-child){margin-top:12px}.lg-articles .lg-article-container:last-child .lg-article{padding-bottom:0;border-bottom:none}.lg-articles .lg-article-link{font-size:16px}.lg-popular-articles{margin-top:18px}.lg-popular-articles-view-all{margin-top:20px;font-size:16px;font-weight:700;display:inline-block}.lg-article-container{width:100%;margin-top:12px}.lg-article-container:first-child{margin-top:0}.lg-article{padding-bottom:12px;height:calc(100% - 12px)}.lg-popular-articles-hubpage{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between}.lg-popular-articles-hubpage .lg-article-link{font-size:16px}.lg-popular-articles-hubpage .lg-article-container{width:calc(50% - 24px)}.lg-popular-articles-hubpage .lg-article-container:nth-last-child(-n+2) .lg-article{padding-bottom:0;border-bottom:none;height:100%}.lg-popular-articles-hubpage .lg-article-container:nth-child(-n+2){margin-top:0}.smartbanner-show{margin-top:80px}.smartbanner-show .smartbanner{display:block}.smartbanner{position:absolute;left:0;top:0;display:none;width:100%;height:80px;line-height:80px;font-family:Helvetica Neue,sans-serif;background:#f4f4f4;z-index:9998;-webkit-font-smoothing:antialiased;overflow:hidden;-webkit-text-size-adjust:none}.smartbanner-container{margin:0 auto;white-space:nowrap}.smartbanner-close{display:inline-block;vertical-align:middle;margin:0 5px;font-family:ArialRoundedMTBold,Arial;font-size:20px;text-align:center;color:#888;text-decoration:none;border:0;border-radius:14px;-webkit-font-smoothing:subpixel-antialiased}.smartbanner-close:active,.smartbanner-close:hover{color:#aaa}.smartbanner-icon{width:57px;height:57px;margin-right:12px;background-size:cover;border-radius:10px}.smartbanner-icon,.smartbanner-info{display:inline-block;vertical-align:middle}.smartbanner-info{width:44%;font-size:11px;line-height:1.2em;font-weight:700}.smartbanner-title{font-size:13px;line-height:18px}.smartbanner-button{position:absolute;right:20px;top:0;bottom:0;margin:auto 0;height:24px;font-size:14px;line-height:24px;text-align:center;font-weight:700;color:#6a6a6a;text-transform:uppercase;text-decoration:none;text-shadow:0 1px 0 hsla(0,0%,100%,.8)}.smartbanner-button:active,.smartbanner-button:hover{color:#aaa}.smartbanner-ios{background:#f4f4f4;background:-webkit-linear-gradient(top,#f4f4f4,#cdcdcd);background:linear-gradient(180deg,#f4f4f4,#cdcdcd);box-shadow:0 1px 2px rgba(0,0,0,.5);line-height:80px}.smartbanner-ios .smartbanner-close{border:0;width:18px;height:18px;line-height:18px;color:#888;text-shadow:0 1px 0 #fff}.smartbanner-ios .smartbanner-close:active,.smartbanner-ios .smartbanner-close:hover{color:#aaa}.smartbanner-ios .smartbanner-icon{background:rgba(0,0,0,.6);background-size:cover;box-shadow:0 1px 3px rgba(0,0,0,.3)}.smartbanner-ios .smartbanner-info{color:#6a6a6a;text-shadow:0 1px 0 hsla(0,0%,100%,.8)}.smartbanner-ios .smartbanner-title{color:#4d4d4d;font-weight:700}.smartbanner-ios .smartbanner-button{padding:0 10px;min-width:10%;color:#6a6a6a;background:#efefef;background:-webkit-linear-gradient(top,#efefef,#dcdcdc);background:linear-gradient(180deg,#efefef,#dcdcdc);border-radius:3px;box-shadow:inset 0 0 0 1px #bfbfbf,0 1px 0 hsla(0,0%,100%,.6),inset 0 2px 0 hsla(0,0%,100%,.7)}.smartbanner-ios .smartbanner-button:active,.smartbanner-ios .smartbanner-button:hover{background:#dcdcdc;background:-webkit-linear-gradient(top,#dcdcdc,#efefef);background:linear-gradient(180deg,#dcdcdc,#efefef)}.smartbanner-android{box-shadow:inset 0 4px 0 #88b131;line-height:82px}.smartbanner-android .smartbanner-close{border:0;width:17px;height:17px;line-height:17px;margin-right:7px;color:#b1b1b3;background:#1c1e21;text-shadow:0 1px 1px #000;box-shadow:inset 0 1px 2px rgba(0,0,0,.8),0 1px 1px hsla(0,0%,100%,.3)}.smartbanner-android .smartbanner-close:active,.smartbanner-android .smartbanner-close:hover{color:#eee}.smartbanner-android .smartbanner-icon{background-color:transparent;box-shadow:none}.smartbanner-android .smartbanner-info{color:#ccc;text-shadow:0 1px 2px #000}.smartbanner-android .smartbanner-title{color:#fff;font-weight:700}.smartbanner-android .smartbanner-button{min-width:12%;color:#d1d1d1;padding:0;background:none;border-radius:0;box-shadow:0 0 0 1px #333,0 0 0 2px #dddcdc}.smartbanner-android .smartbanner-button:active,.smartbanner-android .smartbanner-button:hover{background:none}.smartbanner-android .smartbanner-button-text{text-align:center;display:block;padding:0 10px;background:#42b6c9;background:-webkit-linear-gradient(top,#42b6c9,#39a9bb);background:linear-gradient(180deg,#42b6c9,#39a9bb);text-transform:none;text-shadow:none;box-shadow:none}.smartbanner-android .smartbanner-button-text:active,.smartbanner-android .smartbanner-button-text:hover{background:#2ac7e1}.smartbanner-windows{background:#f4f4f4;background:-webkit-linear-gradient(top,#f4f4f4,#cdcdcd);background:linear-gradient(180deg,#f4f4f4,#cdcdcd);box-shadow:0 1px 2px rgba(0,0,0,.5);line-height:80px}.smartbanner-windows .smartbanner-close{border:0;width:18px;height:18px;line-height:18px;color:#888;text-shadow:0 1px 0 #fff}.smartbanner-windows .smartbanner-close:active,.smartbanner-windows .smartbanner-close:hover{color:#aaa}.smartbanner-windows .smartbanner-icon{background:rgba(0,0,0,.6);background-size:cover;box-shadow:0 1px 3px rgba(0,0,0,.3)}.smartbanner-windows .smartbanner-info{color:#6a6a6a;text-shadow:0 1px 0 hsla(0,0%,100%,.8)}.smartbanner-windows .smartbanner-title{color:#4d4d4d;font-weight:700}.smartbanner-windows .smartbanner-button{padding:0 10px;min-width:10%;color:#6a6a6a;background:#efefef;background:-webkit-linear-gradient(top,#efefef,#dcdcdc);background:linear-gradient(180deg,#efefef,#dcdcdc);border-radius:3px;box-shadow:inset 0 0 0 1px #bfbfbf,0 1px 0 hsla(0,0%,100%,.6),inset 0 2px 0 hsla(0,0%,100%,.7)}.smartbanner-windows .smartbanner-button:active,.smartbanner-windows .smartbanner-button:hover{background:#dcdcdc;background:-webkit-linear-gradient(top,#dcdcdc,#efefef);background:linear-gradient(180deg,#dcdcdc,#efefef)}.smartbanner.smartbanner-android{background:#fff;box-shadow:0 3px rgba(0,0,0,.1)}.smartbanner.smartbanner-android .smartbanner-title{color:#464646}.smartbanner.smartbanner-android .smartbanner-info{color:#888;text-shadow:none}.smartbanner.smartbanner-android .smartbanner-close{background:none;box-shadow:none;text-shadow:none}.smartbanner.smartbanner-android .smartbanner-button{color:#fff;box-shadow:none;background:#54b948;height:40px;line-height:40px;border-radius:5px 5px}.smartbanner.smartbanner-android .smartbanner-button-text{background:#54b948;border-radius:5px 5px}.turbolinks-progress-bar{background-color:#9ad0fd}.turbolinks-hide-progress-bar .turbolinks-progress-bar{display:none}.wotd-cta{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;color:#333}.wotd-cta-content{margin:0 10px;-webkit-box-flex:1;-webkit-flex-grow:1;-ms-flex-positive:1;flex-grow:1}.wotd-cta-title{font-weight:700}.wotd-cta-button{display:block;border-radius:4px;letter-spacing:.8px;text-transform:uppercase;font-size:12px;font-weight:700;white-space:nowrap}.wotd-cta-button:hover{text-decoration:none}.wotd-cta-icon{width:35px;height:35px}.wotd-cta-title{margin-bottom:6px;font-size:16px;line-height:19px}.wotd-cta-text{font-size:14px;line-height:16px}.wotd-cta-button{padding:10px 20px}.wotd-banner{background:#eef7ff}.has-dropdown .wotd-banner{display:none}.wotd-banner .wotd-cta-button{border:1px solid rgba(62,134,199,.5);color:#3e86c7}.wotd-banner{margin:0;padding:20px;border-top:1px solid #b0b0b0;border-radius:0 0 4px 4px}.fluencia-img{display:inline-block;vertical-align:middle;background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -444px -87px no-repeat;background-size:1591px 271px;width:21px;height:16px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.fluencia-img{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -444px -87px no-repeat;background-size:1591px 271px}}.fluencia-text{display:inline;color:#3e86c7;margin-left:10px}a.fluencia-link{padding:10px 20px 10px 0}a.fluencia-link:hover{text-decoration:none}a.fluencia-link:hover .fluencia-text{text-decoration:underline}.nav-content-item-label{position:absolute;color:#fff;font-size:8px;background-color:#3e86c7;text-transform:uppercase;font-weight:700;line-height:12px;padding:0 2px}.nav-content-item-text{padding-bottom:9px}.nav-content{line-height:18px;padding:0;border-bottom:0;margin:20px -20px;zoom:1}.nav-content:after,.nav-content:before{content:"" "";display:table}.nav-content:after{clear:both;display:block;visibility:hidden;height:0;content:"".""}.nav-content>:last-child{border-right:0;width:100%}.nav-content-item{position:relative;float:left;padding:0 20px 4px 0;text-align:center;font-size:14px}.nav-content-item:hover{color:#000}.nav-content-item a{padding:12px 0 9px;text-decoration:none;font-weight:700;color:#858585}.nav-content-item a:active,.nav-content-item a:hover{color:#000}.nav-content-item.nav-active{border:0;border-top:2px solid #3e86c7;background-color:#fff}.nav-content-item.nav-active a{border:0;padding:0;font-size:15px;color:#000;cursor:default}.nav-content-item.nav-active:not(:first-child):before{background-color:#ededed;width:1px;height:100%;display:table-cell;content:"""";position:absolute;top:-2px;padding-top:2px;left:0}.nav-content-item:first-child{border-left:0}.nav-content-cell,.nav-content-item{display:table-cell}.nav-content-item{background-color:#f7f7f7;border:1px solid #ededed;border-right:0;padding:9px 20px 12px;float:none;font-weight:700}.nav-content-item-label{top:0;right:0;left:auto;bottom:auto}.quickdef{line-height:normal;font-weight:700}.quickdef .el{font-size:34px;display:inline-block;color:#3e86c7}.quickdef .el a:hover{text-decoration:underline}.quickdef a{color:#3e86c7;text-decoration:none}.conj-irregular{color:red}.conj-match{background-color:#eef7ff}.conj-footnote{line-height:normal;margin-top:20px}.conj-row{margin:5px 0;font-size:14px;line-height:normal}.conj-basic-label{color:#a5a5a5}.conj-basic-label:hover{border-bottom:1px dashed #b0b0b0;text-decoration:none}.icon-conj-verb-to-verb{display:inline-block;background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -324px -87px no-repeat;background-size:1591px 271px;width:17px;height:13px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.icon-conj-verb-to-verb{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -324px -87px no-repeat;background-size:1591px 271px}}.conj-verb-to-verb-label{padding-left:5px}.conj-basics-row{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;margin:0}.conj-basics-row .audio-start:not(.speaker-playing){visibility:hidden}.conj-basics-row:hover .audio-start{visibility:visible}.vtable-wrapper{overflow-x:auto;-webkit-overflow-scrolling:touch}.vtable-wrapper::-webkit-scrollbar{-webkit-appearance:none}.vtable-wrapper::-webkit-scrollbar:horizontal{height:11px}.vtable-wrapper::-webkit-scrollbar-thumb{border-radius:8px;border:2px solid #fff;background-color:rgba(0,0,0,.5)}.vtable-wrapper::-webkit-scrollbar-track{background-color:#fff;border-radius:2px}.vtable-header{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;margin:15px 0 10px}.vtable-label{color:#333;font-size:18px;font-weight:700}.vtable-title-text{font-weight:700;color:#333;line-height:14px}.vtable-label-link:hover,.vtable-title-link:hover{text-decoration:none}.vtable-label-link-text,.vtable-title-link-text{border-bottom:1px dashed #b0b0b0}.icon-question-mark{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -806px -148px no-repeat;background-size:1591px 271px;width:14px;height:14px;display:inline-block;vertical-align:-1.5px;margin-left:6px}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.icon-question-mark{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -806px -148px no-repeat;background-size:1591px 271px}}.vtable-pronoun{width:120px}.vtable-title,.vtable-word{width:220px;border-style:none none none solid;border-width:1px;border-color:#ededed}.vtable-pronoun,.vtable-title,.vtable-word{padding:7px 6px}.vtable-pronoun,.vtable-word{font-size:13px}.vtable-word .audio-start:not(.speaker-playing){visibility:hidden}.vtable-word:hover .audio-start{visibility:visible}.vtable-word-text{cursor:default}.vtable-word-contents{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.vtable{line-height:normal;border-collapse:separate;border:1px solid #ededed;border-radius:4px}.vtable tr:hover{background-color:#f7f7f7}.source .audio-blue{display:inline-block;line-height:26px}.source .video-blue{display:inline-block;line-height:22px}.source-text{font-size:34px;display:inline-block;margin:0}.spelling h5{font-weight:400;margin:0}.spelling .spelling-row{padding:5px 0}.spelling a{text-decoration:none;color:#3e86c7}.announcement .suggestion{padding-right:12px}.announcement .suggestion:hover{text-decoration:underline}.announcement .spelling-hidden{display:none}.announcement .spelling-show-more{cursor:pointer;text-decoration:none;color:#3e86c7}.announcement .spelling-show-more:hover{text-decoration:underline}.home-fluencia ul{margin:0;padding:0;list-style:none}.home-fluencia li{padding:10px 0 6px}.home-fluencia-img{border-radius:4px;max-width:100%}.home-fluencia-link{font-weight:700;clear:both;background-color:#3e86c7;color:#fff;text-align:center;line-height:40px;width:50%;height:40px;border-radius:4px;cursor:pointer;display:block;margin:0 auto;transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-webkit-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-moz-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-ms-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out;-o-transition:background-color .1s ease-in-out,box-shadow .1s ease-in-out}.home-fluencia-link:hover{background-color:#377ebe;box-shadow:inset 0 -2px rgba(0,0,0,.1);text-decoration:none}.home-fluencia-check{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -656px -88px no-repeat;background-size:1591px 271px;width:27px;height:27px;display:inline-block;margin-right:10px;vertical-align:middle;margin-bottom:4px;float:left}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.home-fluencia-check{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -656px -88px no-repeat;background-size:1591px 271px}}.home-fluencia-variation2 .home-fluencia-left{display:inline-block;width:150px;height:107px;vertical-align:top}@media (min-width:1280px){.home-fluencia-variation2 .home-fluencia-left{width:270px;height:187px}}.home-fluencia-variation2 .home-fluencia-right{display:inline-block;width:430px;padding-left:20px}@media (min-width:1280px){.home-fluencia-variation2 .home-fluencia-right{width:460px}}.home-fluencia-variation2 .home-fluencia-list-item-title{display:inline-block;padding-left:10px}.home-fluencia-variation2 .home-fluencia-link{margin-top:10px;width:100%}.home-fluencia-title{font-size:21px;font-weight:700;line-height:normal}.home-fluencia-list{display:inline-block;width:210px}@media (min-width:1280px){.home-fluencia-list{width:225px}}.home-fluencia-icn-smiley{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -1120px -148px no-repeat;background-size:1591px 271px;width:16px;height:16px;display:inline-block}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.home-fluencia-icn-smiley{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -1120px -148px no-repeat;background-size:1591px 271px}}.home-fluencia-icn-chart{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -1145px -148px no-repeat;background-size:1591px 271px;width:16px;height:15px;display:inline-block}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.home-fluencia-icn-chart{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -1145px -148px no-repeat;background-size:1591px 271px}}.home-fluencia-icn-wand{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -1171px -148px no-repeat;background-size:1591px 271px;width:16px;height:16px;display:inline-block}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.home-fluencia-icn-wand{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -1171px -148px no-repeat;background-size:1591px 271px}}.home-fluencia-icn-devices{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28.png) -1197px -148px no-repeat;background-size:1591px 271px;width:17px;height:16px;display:inline-block}@media (-webkit-min-device-pixel-ratio:1.5),(min-resolution:1.5dppx){.home-fluencia-icn-devices{background:url(//n1.global.ssl.fastly.net/img/desktop/sprite-a-28-2x.png) -1197px -148px no-repeat;background-size:1591px 271px}}.search-history-items a{display:block;width:180px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.announcement{border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-left-radius:0;border-bottom-right-radius:0;border-bottom:1px solid #ededed;background-color:#faf8e2}.tab-content .announcement{border-top:none;border-radius:0}.announcement-title{font-size:18px;line-height:30px}.announcement{border:1px solid #b0b0b0;padding:10px 20px;font-size:14px;line-height:24px;color:#000}.whitelist{display:none}
</style><style type=""text/css"">.button--2cnGw{display:block;width:100%;background-color:#3e86c7;color:#fff;text-align:center;font-size:14px;font-weight:700;line-height:17px;border-radius:4px;padding:11px 0;border:none;outline:none}.button--2cnGw:hover{text-decoration:none}.close--3pxDA{width:10px;height:10px;background:none;outline:none;border:none;padding:0;line-height:0}.brain--3rX5p{max-width:100%;max-height:100%}.vocabQuizButton--1BLm7{width:26px;height:26px;cursor:pointer}@media (min-width:768px){.vocabQuizButton--1BLm7{width:32px;height:32px}}.vocabQuizButton--1BLm7:active{transform:scale(.92)}.h1--2y0tP{margin-top:20px;margin-bottom:10px;font-size:30px;line-height:37px;text-align:center}.h1--2y0tP,.h2--3Cfph{color:#333;font-weight:700}.h2--3Cfph{margin-top:4px;margin-bottom:3px;font-size:16px;line-height:22px}@media (min-width:768px){.h2--3Cfph{font-size:21px;line-height:28px}}.container--oSvSv{display:flex;align-items:flex-start}.body--3BBOQ{flex-grow:1;flex-shrink:1;flex-basis:0%}.body--3BBOQ :first-child{margin-top:0}body[data-is-mobile=false] .container--3iGG6{margin:15px 0;padding:20px;border:1px solid #ededed}.brain--1EYWj{width:60px;height:53px;margin-right:10px}@media (min-width:768px){.brain--1EYWj{width:120px;height:106px;margin-right:20px}}.description--36QOP{font-size:16px;line-height:22px}.button--QyutT{margin:10px 0 20px}.buttonForDesktop--3DkB2{padding-left:30px;padding-right:30px;margin-bottom:0;display:inline-block;width:auto}
</style><script>!function(e){""use strict"";var n=function(n,t,i){function o(e){return r.body?e():void setTimeout(function(){o(e)})}var d,r=e.document,a=r.createElement(""link""),l=i||""all"";if(t)d=t;else{var f=(r.body||r.getElementsByTagName(""head"")[0]).childNodes;d=f[f.length-1]}var s=r.styleSheets;a.rel=""stylesheet"",a.href=n,a.media=""only x"",o(function(){d.parentNode.insertBefore(a,t?d:d.nextSibling)});var u=function(e){for(var n=a.href,t=s.length;t--;)if(s[t].href===n)return e();setTimeout(function(){u(e)})};return a.addEventListener&&a.addEventListener(""load"",function(){this.media=l}),a.onloadcssdefined=u,u(function(){a.media!==l&&(a.media=l)}),a};""undefined""!=typeof exports?exports.loadCSS=n:e.loadCSS=n}(""undefined""!=typeof global?global:this);

loadCSS('//n1.global.ssl.fastly.net/dist/desktop-min-e991c7a753e0efd435cbe745566e0eff.css')
</script><script>(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(""."");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;""undefined""!==typeof d?c=b[d]=[]:d=""mixpanel"";c.people=c.people||[];c.toString=function(b){var a=""mixpanel"";""mixpanel""!==d&&(a+="".""+d);b||(a+="" (stub)"");return a};c.people.toString=function(){return c.toString(1)+"".people (stub)""};i=""disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user"".split("" "");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement(""script"");a.type=""text/javascript"";a.async=!0;a.src=""//cdn.mxpnl.com/libs/mixpanel-2.2.min.js"";e=f.getElementsByTagName(""script"")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
if (window.SD_MP) {
  mixpanel.init(window.SD_MP);
}</script><script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-329211-2', 'auto');</script></head><body data-datetime=""2018-02-03T12:29:00+00:00"" data-is-mobile=""false"" data-asset-host=""https://n1.global.ssl.fastly.net"" class=""lang-en js-transition-off-during-page-load""><script>(function() {
  window.SD_PAGE_CATEGORY          = 'conjugation';
  window.SD_DATE_TIME              = '2018-02-03T12:29:00+00:00';
  window.SD_IS_INITIAL_VISIT       = 'false';
  window.SD_STEALTH_FETCH_EXAMPLES = 'false' === 'true';
  window.SD_DEFAULT_FROM_LANG      = 'en';
  window.SD_QUICKDEF               = 'to have';
  window.SD_MT_KEY                 = '';
  window.SD_AD_TEST                = '';
  window.SD_AD_LIST                = [""adTop"",""adSide1"",""adSide2"",""adSideLong"",""adContent"",""adBot""];
  window.SD_USER_SURVEY            = '' === 'true';
  window.SD_NPS_SURVEY             = 'false' === 'true';
  window.SD_WOTD_OVERLAY           = 'false' === 'true';
  window.SD_WOTD_BANNER            = 'false' === 'true';
  window.SD_TRANSLATION_IMAGE_PROPS = {};
})();</script><div class=""header-wrapper""><div class=""header header-container container""><div class=""menu-bar""><div class=""menu-nav menu-collapsed""><div class=""menu-container""><div class=""menu-selector""><div class=""icon-menu-nav-selector""></div><div class=""menu-selector-text"">Menu</div><div class=""menu-collapse""></div></div><div class=""menu""><a href=""/""><div class=""menu-item"">Home</div></a><a href=""/translation""><div class=""menu-item"">Translation</div></a><a href=""/conjugation""><div class=""menu-item"">Conjugation</div></a><a href=""/learn""><div class=""menu-item"">Learn Spanish</div></a><a href=""/flashcards""><div class=""menu-item"">Flashcards</div></a><a href=""/guide""><div class=""menu-item"">Language Guide</div></a><a href=""/wordoftheday""><div class=""menu-item"">Word of the Day</div></a><a href=""/verbos/tener"" data-turbolinks=""false"" class=""go-language""><div class=""menu-item menu-divider"">Ver en español</div></a><a href=""/traductor"" data-turbolinks=""false"" class=""go-language""><div class=""menu-item menu-divider"">Traductor</div></a></div></div></div><div class=""menu-user menu-collapsed""><div class=""menu-session-links""><a href=""/users/login"" class=""menu-login"">Log In</a><a href=""/users/signup"" data-signup-origin=""Nav Button"" class=""menu-signup"">Sign Up</a></div></div><a href=""/"" class=""header-logo""><svg viewBox=""0 0 200 34"" xmlns=""http://www.w3.org/2000/svg""><path d=""M185 11.6v1.2h3V21c0 1.9.6 3.3 1.7 4.4 1.1 1 2.7 1.5 4.8 1.5l2.2-.1a16.7 16.7 0 0 0 1.5-.5 4 4 0 0 0 1.1-.4l.7-.4-1.8-4.9-.4.4c-.3.2-.7.2-1.1.2-1.2 0-1.7-.5-1.7-1.7v-6.8h3.7V8H195V3h-7v5h-3v1.5c-.5-.6-1-1-1.8-1.3-.6-.3-1.3-.6-2.2-.8a11.8 11.8 0 0 0-2.8-.4 11 11 0 0 0-4 .7 11 11 0 0 0-3.2 2 10.2 10.2 0 0 0-2.2 3.2 10 10 0 0 0-.8 4.1c0 1.5.3 3 .8 4.1a8.3 8.3 0 0 0 2.2 3.2c.8.8 1.9 1.5 3.2 2a11 11 0 0 0 6.8.3 10.4 10.4 0 0 0 4-2l1-1-4.4-4c-.2.5-.6.9-1.1 1.3-.6.4-1.2.6-2 .6-.9 0-1.7-.4-2.4-1.1-.8-.8-1.1-1.9-1.1-3.3 0-1.5.3-2.5 1-3.3a3.2 3.2 0 0 1 2.5-1.2c.8 0 1.5.3 2 .6.5.4 1 .9 1.1 1.4l3.4-3zM22.3 21.9a7.9 7.9 0 0 1-2.3 2.7 11 11 0 0 1-3.7 1.8c-1.5.4-3.1.6-5 .6a19.2 19.2 0 0 1-7.5-1.5 10.2 10.2 0 0 1-2.2-1.4A17.3 17.3 0 0 1 0 22.8l4-5 1 1c.5.5 1 .8 1.6 1.2l2 1a6.3 6.3 0 0 0 2.3.4c1 0 2-.1 2.6-.5.7-.3 1-.7 1-1.3 0-.7-.3-1.2-.9-1.6-.6-.4-1.7-.7-3.3-1-2-.3-3.6-.7-4.9-1.3-1.3-.7-2.3-1.4-3-2.1A6.2 6.2 0 0 1 .8 11a9.6 9.6 0 0 1-.4-2.8c0-1 .3-1.9.7-3a7.7 7.7 0 0 1 2.1-2.6c1-.8 2-1.4 3.4-2C8 .4 9.6 0 11.3 0c1.5 0 2.8.1 4 .3a12 12 0 0 1 5.3 2.2L22 3.9l-4 4.4-1.1-1a29.2 29.2 0 0 0-1.5-.8l-1.7-.6a6.5 6.5 0 0 0-1.6-.2 6 6 0 0 0-2.4.4c-.6.3-.9.8-.9 1.6 0 .5.3 1 .9 1.3.6.3 1.5.5 2.6.7l4.3 1c1.2.5 2.4 1 3.3 1.7a7 7 0 0 1 2.2 2.5c.5 1 .8 2.2.8 3.6a8.4 8.4 0 0 1-.7 3.4zM167 17h-6l-1-16h8l-1 16zm.7 7.6A4.1 4.1 0 0 1 164 27a3.8 3.8 0 0 1-1.5-.3 4 4 0 0 1-2.2-2.1 4.2 4.2 0 0 1-.3-1.6c0-.6.1-1.1.3-1.6A4.1 4.1 0 0 1 164 19l1.5.3a5 5 0 0 1 1.3.9c.3.3.7.7.9 1.2.2.5.3 1 .3 1.6a4.2 4.2 0 0 1-.3 1.6zM159 14c0 2-.3 4-1 5.5a11.7 11.7 0 0 1-7.3 6.7 17 17 0 0 1-5.4.8H134V1h11.3c2 0 3.8.3 5.4.8a11.8 11.8 0 0 1 7.3 6.7c.7 1.6 1 3.4 1 5.5zm-7.4-3c-.3-.8-.6-1.5-1.2-2.1a5 5 0 0 0-2-1.4c-.7-.3-1.6-.4-2.7-.4L142 7v13h3.8c1 0 1.8-.2 2.6-.5a5.7 5.7 0 0 0 2-1.5 6.2 6.2 0 0 0 1.2-2 7 7 0 0 0 .4-2.4c0-1-.2-1.9-.4-2.6zm-26.4 16l-.1-2.3v-8.5c0-1-.2-1.7-.7-2-.4-.5-1-.7-1.8-.7s-1.4.1-1.8.5c-.5.3-.8.6-1 1v12H113V1h6.7v9.2c.5-.6 1.3-1.2 2.3-1.7a8 8 0 0 1 4-.9c1 0 1.9.2 2.7.5.7.3 1.4.7 1.8 1.3.5.5.9 1.1 1.1 1.9.3.7.3 1.5.3 2.3v8.5l.1 4.9h-6.8zm-13.8-4a6 6 0 0 1-1.7 2.1 7.9 7.9 0 0 1-2.8 1.4 14.6 14.6 0 0 1-9.3-.6 27.7 27.7 0 0 1-2-1c-.7-.5-1.2-.8-1.6-1.3l3-4.3 1.1 1 1.4.9a6.9 6.9 0 0 0 3 .8c.8 0 1.4 0 1.8-.3.3-.2.5-.4.5-.9 0-.3-.2-.6-.7-.8-.5-.2-1.2-.4-2.1-.5-1 0-2-.2-3-.5-.8-.2-1.5-.6-2.2-1.2a5.3 5.3 0 0 1-1.6-2c-.3-.7-.5-1.6-.5-2.8 0-.8.2-1.5.5-2.2.4-.7.9-1.4 1.6-2 .7-.5 1.5-1 2.5-1.3 1-.4 2.2-.5 3.6-.5 2 0 3.6.3 4.9.8 1.3.5 2.3 1.1 3 1.8l-2.5 4c0-.3-.4-.5-.6-.7l-1.1-.7a4.3 4.3 0 0 0-1.4-.4l-1.4-.1a6 6 0 0 0-1.7.2c-.5.1-.7.4-.7.8 0 .5.2.9.7 1l2.3.5c2.7.3 4.7 1 5.8 2a5.8 5.8 0 0 1 1.2 6.9zM86 33l1-16h6l1 16h-8zm0-21c0-.6.1-1.1.3-1.6A4.1 4.1 0 0 1 90 8l1.5.3a4 4 0 0 1 2.2 2.1c.2.5.3 1 .3 1.6a4.2 4.2 0 0 1-.3 1.6A4.1 4.1 0 0 1 90 16a3.8 3.8 0 0 1-1.5-.3 5 5 0 0 1-1.3-.9c-.3-.3-.7-.7-.9-1.2A4.2 4.2 0 0 1 86 12zm-7.8 14l-.1-2.2v-8.4c0-1-.2-1.6-.7-2-.4-.5-1-.7-1.8-.7s-1.4.2-1.8.5c-.5.4-.8.6-1 1v11.7H66V7.6h6.7v2c.5-.7 1.3-1.2 2.3-1.8 1-.5 2.4-.8 4-.8 1 0 1.9.1 2.7.5a4.7 4.7 0 0 1 3 3.1c.2.7.2 1.4.2 2.3v8.3L85 26h-6.8zm-20-18.6a6.8 6.8 0 0 1 3 1.3c.8.6 1.4 1.4 2 2.4.5 1 .8 2.3.8 3.9v11.5h-6.6v-1.8c-.2.4-.5.7-.9 1l-1.3.7-1.5.5-1.4.1c-.8 0-1.7-.1-2.5-.4a7.2 7.2 0 0 1-2.4-1 5.5 5.5 0 0 1-1.7-2c-.5-.8-.7-1.8-.7-2.9 0-1.3.2-2.3.7-3.1.4-.8 1-1.5 1.7-2a7 7 0 0 1 2.4-.9 10.3 10.3 0 0 1 2.5-.3c.4 0 .9 0 1.4.2.5 0 1 .2 1.5.4l1.3.6.9.9v-1.8c0-.8-.3-1.5-1-2.1-.7-.6-1.5-.9-2.6-.9-1 0-2 .2-3 .6a6.3 6.3 0 0 0-2.5 1.6L46 10.1a8 8 0 0 1 2.1-1.5c.8-.5 1.7-.8 2.5-1l2.3-.5 2-.1c1.2 0 2.3.2 3.3.4zm-1.2 12c-.2-.5-.7-.8-1.3-1-.6-.3-1.1-.4-1.6-.4l-1 .1a3 3 0 0 0-1.8 1.2 2 2 0 0 0-.3 1.2c0 .5.1.9.3 1.2l.8.7 1 .5 1 .1c.5 0 1 0 1.6-.3.6-.2 1-.5 1.3-.9v-2.5zm-13.6 2c-.4 1.2-1 2.2-1.6 3-.8.9-1.6 1.5-2.6 2-1 .4-2 .6-3.2.6a7.7 7.7 0 0 1-3-.6 5 5 0 0 1-2.2-2V34H24V7.6h6.8v2c.6-1 1.3-1.6 2.2-2 .9-.5 1.9-.6 3-.6s2.2.2 3.1.6a7.5 7.5 0 0 1 2.6 2 9 9 0 0 1 1.7 3c.4 1.3.6 2.8.6 4.3 0 1.7-.1 3.1-.6 4.4zm-7.3-7.2a3 3 0 0 0-2.3-1.2c-.4 0-1 0-1.5.2a2.4 2.4 0 0 0-1.3.9v6.8c.3.5.7.7 1.2.9a5.8 5.8 0 0 0 1.5.2c1 0 1.6-.4 2.3-1.2a5 5 0 0 0 1-3.3c.1-1.5-.3-2.6-1-3.3z"" fill=""#FFF"" fill-rule=""evenodd""/></svg></a></div></div></div><div class=""content-container container""><div id=""adTop-container"" data-turbolinks-permanent class=""adTop sd-ad-container""><div class=""adTop ad-wrapper""><div id=""adTop"" class=""adTop ad-unit""></div></div></div><div class=""main-container""><div class=""search""><div class=""search-component search-form""><form action=""/conjugation"" method=""post"" accept-charset=""utf-8"" data-action=""conjugation"" class=""page-action search_query_to_url save-history""><span id=""hidden-search""></span><div class=""search-form-parts search-collapse""><input id=""hidden-input"" name=""isExpand"" type=""hidden"" value=""false""><textarea name=""text"" id=""query"" autofocus onkeydown=""if ((window.event ? event.keyCode : event.which) === 13) {this.value.trim().length &gt; 0 &amp;&amp; document.getElementsByClassName('page-action')[0].submit(); return false;}"" onfocus=""this.select(); this.onfocus=''"" data-suggest-host=""www.spanishdict.com"" placeholder=""Enter a Spanish verb&hellip;"" autocomplete=""off"" autocapitalize=""none"" aria-label=""Enter a Spanish verb"" maxlength=""500"" class=""search-form-input"">tener</textarea><div class=""accents""><span data-toggle=""tooltip"" title=""Add á to your text"" class=""accents-letter"">á</span><span data-toggle=""tooltip"" title=""Add é to your text"" class=""accents-letter"">é</span><span data-toggle=""tooltip"" title=""Add í to your text"" class=""accents-letter"">í</span><span data-toggle=""tooltip"" title=""Add ó to your text"" class=""accents-letter"">ó</span><span data-toggle=""tooltip"" title=""Add ú to your text"" class=""accents-letter"">ú</span><span data-toggle=""tooltip"" title=""Add ñ to your text"" class=""accents-letter"">ñ</span><span data-toggle=""tooltip"" title=""Add ¿ to your text"" class=""accents-letter accents-descender"">¿</span><span data-toggle=""tooltip"" title=""Add ¡ to your text"" class=""accents-letter accents-descender"">¡</span><span data-toggle=""tooltip"" title=""Hear audio of the words"" class=""accents-audio-btn""></span></div><div title=""Characters Remaining"" class=""char-limit""></div><div data-toggle=""tooltip"" title=""Show the accent buttons and expand the search box."" class=""search-expand-icon""></div><div data-toggle=""tooltip"" title=""Collapse the search box"" class=""search-collapse-icon""></div><button type=""submit"" class=""search-form-button"">Conjugate</button></div></form></div></div><div class=""conjugation""><div class=""card""><div class=""quickdef""><div class=""source""><h1 source-lang=""es"" class=""source-text"">tener</h1><span class=""media-links""><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tener&amp;key=a6677680aa08e6d22521effe7b7d1e58"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue""></span></a><a href=""/videos/tener""><span data-toggle=""tooltip"" title=""Watch a video translation"" class=""video-blue"">&nbsp;</span></a></span></div><div class=""lang""><div class=""el""><a href=""/translate/to"">to</a> <a href=""/translate/have"">have</a>, <a href=""/translate/to"">to</a> <a href=""/translate/be"">be</a></div><span class=""media-links""><a href=""https://audio1.spanishdict.com/audio?lang=en&amp;text=to-have%2C-to-be&amp;key=56eb01f0483d71104c8ad25f04723487"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue""></span></a></span></div></div><div class=""nav-content""><div class=""nav-content-cell""><div class=""nav-content-item""><a href=""/translate/tener"" data-sd-test-group-min=""51"" data-sd-test-group-max=""70""><span class=""nav-content-item-text"">Dictionary</span></a></div><div class=""nav-content-item nav-active""><a><span class=""nav-content-item-text"">Conjugation</span></a></div><div class=""nav-content-item""><a href=""/examples/tener"" data-sd-test-group-min=""51"" data-sd-test-group-max=""70""><span class=""nav-content-item-text"">Examples</span></a></div><div class=""nav-content-item""><a href=""/phrases/tener"" data-sd-test-group-min=""51"" data-sd-test-group-max=""70""><span class=""nav-content-item-text"">Phrases</span></a><span class=""nav-content-item-label"">new</span></div><div class=""nav-content-item""><a href=""/videos/tener"" data-sd-test-group-min=""51"" data-sd-test-group-max=""70""><span class=""nav-content-item-text"">Video</span></a></div></div><div class=""nav-content-item""></div></div><div><div class=""conj-row conj-basics-row""><a href=""/guide/present-participles-in-spanish"" class=""conj-basic-label"">Present Participle:</a><span>&nbsp;</span><span data-toggle=""tooltip"" data-tense=""presentParticiple"" class=""conj-basic-word"">teniendo</span><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=teniendo&amp;key=d95ee87fb453eefeefad4fe7653a60f2"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></div><div><div class=""conj-row conj-basics-row""><a href=""/guide/past-participle-in-spanish"" class=""conj-basic-label"">Past Participle:</a><span>&nbsp;</span><span data-toggle=""tooltip"" data-tense=""pastParticiple"" class=""conj-basic-word"">tenido</span><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tenido&amp;key=a8c70961b0b97b42b4dc04cfca563c5d"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></div><div class=""vtable-header""><div><a href=""/guide/spanish-indicative-mood"" class=""vtable-label vtable-label-link""><span class=""vtable-label-link-text"">Indicative</span><span class=""icon-question-mark""></span></a></div><div class=""conj-row"">Irregularities are in<span class=""conj-irregular""> red</span></div></div><div class=""vtable-wrapper""><table class=""vtable""><tr class=""vtable-head-row""><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""/guide/spanish-present-tense-forms/"" title=""Tense example: *He speaks* Spanish."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Present</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-preterite-tense-forms/"" title=""Tense example: *He spoke* to my sister yesterday."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Preterite</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-imperfect-tense-forms"" title=""Tense example: *He used to speak* Italian when he was a child."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Imperfect</span></a></td><td class=""vtable-title""><a href=""/guide/conditional-tense"" title=""Tense example: *He would speak* French in France."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Conditional</span></a></td><td class=""vtable-title""><a href=""/guide/simple-future-regular-forms-and-tenses"" title=""Tense example: *He will speak* Portuguese on his vacation next week."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Future</span></a></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">yo</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentIndicative"" data-person='0' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>o</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tengo&amp;key=6a7f459c50f52fe74797443ea4b5febd"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritIndicative"" data-person='0' class=""vtable-word-text"">t<span class='conj-irregular'>uve</span></div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuve&amp;key=2e20c367568732918d76d301ea4d76dd"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectIndicative"" data-person='0' class=""vtable-word-text"">tenía</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=ten%C3%ADa&amp;key=04a6513ca93c2e403866211ae31ecc38"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalIndicative"" data-person='0' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>ía</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%ADa&amp;key=25789147780613411488aab75e81f221"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureIndicative"" data-person='0' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>é</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%A9&amp;key=85408326d42d461c5bfaf00729dc77a9"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">tú</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentIndicative"" data-person='1' class=""vtable-word-text"">t<span class='conj-irregular'>i</span>enes</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tienes&amp;key=cdb8122e188f414882f45c06ccffa2ae"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritIndicative"" data-person='1' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iste</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuviste&amp;key=33e6130a26c78d3430fab959fd34f6fc"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectIndicative"" data-person='1' class=""vtable-word-text"">tenías</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=ten%C3%ADas&amp;key=345c9c181428bab34b962ff3480cf43e"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalIndicative"" data-person='1' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>ías</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%ADas&amp;key=6378e8567fcbf4220b6dfb6bb4618b70"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureIndicative"" data-person='1' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>ás</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%A1s&amp;key=007862eea7e8baca8302d65a9023f652"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentIndicative"" data-person='2' class=""vtable-word-text"">t<span class='conj-irregular'>i</span>ene</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tiene&amp;key=4401fff2d25ef5041f6aeebba80ff1e5"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritIndicative"" data-person='2' class=""vtable-word-text"">t<span class='conj-irregular'>uvo</span></div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvo&amp;key=88989ac97980eeaabc552550ab95c686"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectIndicative"" data-person='2' class=""vtable-word-text"">tenía</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=ten%C3%ADa&amp;key=04a6513ca93c2e403866211ae31ecc38"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalIndicative"" data-person='2' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>ía</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%ADa&amp;key=25789147780613411488aab75e81f221"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureIndicative"" data-person='2' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>á</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%A1&amp;key=2d2226724110c7eff2dbdb262bbbb0f7"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentIndicative"" data-person='3' class=""vtable-word-text"">tenemos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tenemos&amp;key=c26a08e18335a0ca1a694458b9d89ef7"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritIndicative"" data-person='3' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>imos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvimos&amp;key=83a3b113ff2c50187e5a3b9ff5375645"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectIndicative"" data-person='3' class=""vtable-word-text"">teníamos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=ten%C3%ADamos&amp;key=5f100bde8a1c60961defc15f7b360dd7"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalIndicative"" data-person='3' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>íamos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%ADamos&amp;key=ff5c70e516b5b89a34547e513f77cb3d"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureIndicative"" data-person='3' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>emos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendremos&amp;key=8859963ca964dc16bbcacaab923f2f26"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentIndicative"" data-person='4' class=""vtable-word-text"">tenéis</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=ten%C3%A9is&amp;key=02164d8c6a3afff67ce74fd3aa9ef681"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritIndicative"" data-person='4' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>isteis</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvisteis&amp;key=0baba1e7997521a50621b85ee7134210"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectIndicative"" data-person='4' class=""vtable-word-text"">teníais</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=ten%C3%ADais&amp;key=e90c07b9131dba0bd4fbe2e93440ae93"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalIndicative"" data-person='4' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>íais</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%ADais&amp;key=ca4130ee827d7a48d9c9a12f1a1d6f06"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureIndicative"" data-person='4' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>éis</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%A9is&amp;key=6200d068a39a2ea486809f54147952f3"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentIndicative"" data-person='5' class=""vtable-word-text"">t<span class='conj-irregular'>i</span>enen</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tienen&amp;key=97fe299ff2586c6be9167eb513f58905"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritIndicative"" data-person='5' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>ieron</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvieron&amp;key=362f8c762d1f050f9f1c9d75fa31e1b0"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectIndicative"" data-person='5' class=""vtable-word-text"">tenían</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=ten%C3%ADan&amp;key=152d2269723a569fa40c17ad4a7bfe01"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalIndicative"" data-person='5' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>ían</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%ADan&amp;key=26374aad8eb26350826c060fc2fef250"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureIndicative"" data-person='5' class=""vtable-word-text"">ten<span class='conj-irregular'>dr</span>án</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tendr%C3%A1n&amp;key=4a05b7f69247dbd8b317c790bb5b9850"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr></table></div><div class=""quiz-promo""><div class=""container--3iGG6""><div class=""container--oSvSv""><div class=""brain--1EYWj""><img class=""brain--3rX5p"" src=""https://n1.global.ssl.fastly.net/img/common/brain.jpg""/></div><div class=""body--3BBOQ""><div><h2 class=""h2--3Cfph "">Want to learn to conjugate?</h2><div class=""description--36QOP"">Try our free practice games</div><button class=""button--2cnGw js-show-conjugation-overlay buttonForDesktop--3DkB2 button--QyutT"" href="""">Select a Conjugation Game</button></div></div></div></div></div><div class=""vtable-header""><div><a href=""/guide/subjunctive-vs-indicative-in-spanish"" class=""vtable-label vtable-label-link""><span class=""vtable-label-link-text"">Subjunctive</span><span class=""icon-question-mark""></span></a></div></div><div class=""vtable-wrapper""><table class=""vtable""><tr class=""vtable-head-row""><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""/guide/spanish-present-subjunctive"" title=""Tense example: It is good that *he speak* to your mother."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Present</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-imperfect-subjunctive"" title=""Tense example: It made me happy that *he spoke* at the wedding."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Imperfect</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-imperfect-subjunctive"" title=""Tense example: It made me happy that *he spoke* at the wedding."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Imperfect 2</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-future-subjunctive"" title=""Tense example: It is possible that *he will speak* to your father."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Future</span></a></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">yo</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentSubjunctive"" data-person='0' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>a</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tenga&amp;key=b04c6e172990c7d5002b218ac8bef2c5"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive"" data-person='0' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iera</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuviera&amp;key=da9b9b498bab6d907adb1bc1c5ed366f"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive2"" data-person='0' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iese</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuviese&amp;key=41edd09f2b4b9e9c75acd3d04d29a156"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureSubjunctive"" data-person='0' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iere</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuviere&amp;key=3a40e78b3f486666413ece15175a7e37"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">tú</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentSubjunctive"" data-person='1' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>as</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tengas&amp;key=3164d5ebaee359fd5079d511a46bc6b0"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive"" data-person='1' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>ieras</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvieras&amp;key=1ae9fab59dd1a0b6e5c175bd986b468f"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive2"" data-person='1' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>ieses</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvieses&amp;key=ad2c26a459eef5f4be60c9959a09e15d"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureSubjunctive"" data-person='1' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>ieres</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvieres&amp;key=d6e36548c44adb321fba1b1217b44750"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentSubjunctive"" data-person='2' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>a</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tenga&amp;key=b04c6e172990c7d5002b218ac8bef2c5"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive"" data-person='2' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iera</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuviera&amp;key=da9b9b498bab6d907adb1bc1c5ed366f"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive2"" data-person='2' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iese</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuviese&amp;key=41edd09f2b4b9e9c75acd3d04d29a156"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureSubjunctive"" data-person='2' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iere</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuviere&amp;key=3a40e78b3f486666413ece15175a7e37"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentSubjunctive"" data-person='3' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>amos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tengamos&amp;key=1a303b9ce9022f28cf847e65a6b99201"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive"" data-person='3' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iéramos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvi%C3%A9ramos&amp;key=3892f7fa3237e508f3785620957ca6bb"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive2"" data-person='3' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iésemos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvi%C3%A9semos&amp;key=32e6bfb356383c8538f1baede9f5a801"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureSubjunctive"" data-person='3' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iéremos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvi%C3%A9remos&amp;key=52332ba91d3417a59d22a5deb5db11de"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentSubjunctive"" data-person='4' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>áis</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=teng%C3%A1is&amp;key=a65ef07a195a23680198633054e17711"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive"" data-person='4' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>ierais</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvierais&amp;key=407bbc1c95700d676ad9948deed93db2"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive2"" data-person='4' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>ieseis</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvieseis&amp;key=0659363cf19660fc6279e83c40c65b4f"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureSubjunctive"" data-person='4' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iereis</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuviereis&amp;key=3d046ccf71a944a8c88be9cd51d004e7"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentSubjunctive"" data-person='5' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>an</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tengan&amp;key=0567149492e3905a6b4e02ae997f86c3"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive"" data-person='5' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>ieran</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvieran&amp;key=e2cf013ada10bf25b1cccc60397a1721"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectSubjunctive2"" data-person='5' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>iesen</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuviesen&amp;key=701c92ca0b9c879bca10faad57688e73"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureSubjunctive"" data-person='5' class=""vtable-word-text"">t<span class='conj-irregular'>uv</span>ieren</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tuvieren&amp;key=660a18866355de867e793d87e745029d"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr></table></div><div class=""vtable-header""><div><a href=""/guide/spanish-imperative-mood"" class=""vtable-label vtable-label-link""><span class=""vtable-label-link-text"">Imperative</span><span class=""icon-question-mark""></span></a></div></div><div class=""vtable-wrapper""><table class=""vtable""><tr class=""vtable-head-row""><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""/guide/spanish-imperative-mood"" title=""Tense example: *Speak!*."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Affirmative</span></a></td><td class=""vtable-title""><a href=""/guide/negative-informal-tu-commands"" title=""Tense example: *Don't Speak!*."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Negative</span></a></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">yo</td><td class=""vtable-word""><div class=""vtable-word-contents"">-</div></td><td class=""vtable-word""><div class=""vtable-word-contents"">-</div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">tú</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperative"" data-person='1' class=""vtable-word-text"">ten<span class='conj-irregular'></span></div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=ten&amp;key=0f425029842f1c653d5febbee449ff67"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""negativeImperative"" data-person='1' class=""vtable-word-text"">no ten<span class='conj-irregular'>g</span>as</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=no-tengas&amp;key=aca7a6030a2d7068824a0bfaff507831"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">Ud.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperative"" data-person='2' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>a</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tenga&amp;key=b04c6e172990c7d5002b218ac8bef2c5"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""negativeImperative"" data-person='2' class=""vtable-word-text"">no ten<span class='conj-irregular'>g</span>a</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=no-tenga&amp;key=c1343a51aab98f2a9f5a226d3bcf6365"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperative"" data-person='3' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>amos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tengamos&amp;key=1a303b9ce9022f28cf847e65a6b99201"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""negativeImperative"" data-person='3' class=""vtable-word-text"">no ten<span class='conj-irregular'>g</span>amos</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=no-tengamos&amp;key=3b1865d84f70116ac786e8b81fd215ba"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperative"" data-person='4' class=""vtable-word-text"">tened</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tened&amp;key=dc725cc486b853bc864bf2f3ec4a3b49"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""negativeImperative"" data-person='4' class=""vtable-word-text"">no ten<span class='conj-irregular'>g</span>áis</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=no-teng%C3%A1is&amp;key=d6ef299959da9c4f85fc97b7b727d70a"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">Uds.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperative"" data-person='5' class=""vtable-word-text"">ten<span class='conj-irregular'>g</span>an</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=tengan&amp;key=0567149492e3905a6b4e02ae997f86c3"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""negativeImperative"" data-person='5' class=""vtable-word-text"">no ten<span class='conj-irregular'>g</span>an</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=no-tengan&amp;key=72b9e1ce86f9a4183e3aa5b95cb05ff7"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr></table></div><div class=""vtable-header""><div><a href=""/guide/spanish-present-progressive-forms"" class=""vtable-label vtable-label-link""><span class=""vtable-label-link-text"">Continuous (Progressive)</span><span class=""icon-question-mark""></span></a></div></div><div class=""vtable-wrapper""><table class=""vtable""><tr class=""vtable-head-row""><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""/guide/spanish-present-progressive-forms"" title=""Tense example: *I'm speaking* to my husband."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Present</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-present-progressive-forms"" title=""Tense example: *They were speaking* to some classmates about the trip not too long ago."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Preterite</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-imperfect-progressive-tense"" title=""Tense example: I have no clue what *he was speaking* about."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Imperfect</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-present-progressive-forms"" title=""Tense example: *You would be speaking* at the conference if you hadn't gotten sick."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Conditional</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-present-progressive-forms"" title=""Tense example: *I will be speaking* to my grandmother tomorrow at this time."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Future</span></a></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">yo</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentContinuous"" data-person='0' class=""vtable-word-text"">estoy teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estoy-teniendo&amp;key=4a1d93229a6b3d17ce2846e6caae2b9a"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritContinuous"" data-person='0' class=""vtable-word-text"">estuve teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estuve-teniendo&amp;key=21e363f6786d32b9a49ec35e9366768e"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectContinuous"" data-person='0' class=""vtable-word-text"">estaba teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estaba-teniendo&amp;key=ae8a74556d9a1c6268a60beceee36c8d"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalContinuous"" data-person='0' class=""vtable-word-text"">estaría teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%ADa-teniendo&amp;key=21702c99a09649a45528718b4af4be6c"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureContinuous"" data-person='0' class=""vtable-word-text"">estaré teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%A9-teniendo&amp;key=70ce4eeab3aaa66426165a19a408edd7"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">tú</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentContinuous"" data-person='1' class=""vtable-word-text"">estás teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=est%C3%A1s-teniendo&amp;key=8b9087668d907301dfb22f82db7bc3ad"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritContinuous"" data-person='1' class=""vtable-word-text"">estuviste teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estuviste-teniendo&amp;key=2c7629d09a694e20a57d76f8c9a91741"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectContinuous"" data-person='1' class=""vtable-word-text"">estabas teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estabas-teniendo&amp;key=be936157359fbb7762a58c087c81e6c0"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalContinuous"" data-person='1' class=""vtable-word-text"">estarías teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%ADas-teniendo&amp;key=ff8eb7a264f046faa3ebcb2f78b02fc3"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureContinuous"" data-person='1' class=""vtable-word-text"">estarás teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%A1s-teniendo&amp;key=a2ffd5e01d460e6505acf1db39f5a8b3"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentContinuous"" data-person='2' class=""vtable-word-text"">está teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=est%C3%A1-teniendo&amp;key=00e0ad7255d0b7666bc913f2f311d1e8"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritContinuous"" data-person='2' class=""vtable-word-text"">estuvo teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estuvo-teniendo&amp;key=177e426016c755b8f3f77996697c1fe0"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectContinuous"" data-person='2' class=""vtable-word-text"">estaba teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estaba-teniendo&amp;key=ae8a74556d9a1c6268a60beceee36c8d"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalContinuous"" data-person='2' class=""vtable-word-text"">estaría teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%ADa-teniendo&amp;key=21702c99a09649a45528718b4af4be6c"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureContinuous"" data-person='2' class=""vtable-word-text"">estará teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%A1-teniendo&amp;key=7db787e7c0d220e4e02d199aabf182e2"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentContinuous"" data-person='3' class=""vtable-word-text"">estamos teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estamos-teniendo&amp;key=b1fafcd56d571ba426f522986ca0b357"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritContinuous"" data-person='3' class=""vtable-word-text"">estuvimos teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estuvimos-teniendo&amp;key=51bee9435c1bf9f0e40752c8dc17abc2"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectContinuous"" data-person='3' class=""vtable-word-text"">estábamos teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=est%C3%A1bamos-teniendo&amp;key=d3c2d3fa3bb82146edf196d84b796c63"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalContinuous"" data-person='3' class=""vtable-word-text"">estaríamos teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%ADamos-teniendo&amp;key=caf732a7482896ecd199ea10151e130f"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureContinuous"" data-person='3' class=""vtable-word-text"">estaremos teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estaremos-teniendo&amp;key=99fa5c60dc8f75b328324b395b1f5401"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentContinuous"" data-person='4' class=""vtable-word-text"">estáis teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=est%C3%A1is-teniendo&amp;key=2064058871ba9fcc6620afb960c77fe3"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritContinuous"" data-person='4' class=""vtable-word-text"">estuvisteis teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estuvisteis-teniendo&amp;key=00b08f6352cabd5975a6e584778f4e2e"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectContinuous"" data-person='4' class=""vtable-word-text"">estabais teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estabais-teniendo&amp;key=b0525100a6b8c802eea2a5c1b64510ef"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalContinuous"" data-person='4' class=""vtable-word-text"">estaríais teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%ADais-teniendo&amp;key=ad282043d0b2553aef395b6c30d63891"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureContinuous"" data-person='4' class=""vtable-word-text"">estaréis teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%A9is-teniendo&amp;key=a627bfb6c925cc84ad547bd88c29cffb"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentContinuous"" data-person='5' class=""vtable-word-text"">están teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=est%C3%A1n-teniendo&amp;key=47bd10e4cbba59aa43a519cf94def3fd"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritContinuous"" data-person='5' class=""vtable-word-text"">estuvieron teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estuvieron-teniendo&amp;key=814b052b994aeed7a043f0905987acc2"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""imperfectContinuous"" data-person='5' class=""vtable-word-text"">estaban teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estaban-teniendo&amp;key=792e3b0bb70987aa269a6f8c4a103b89"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalContinuous"" data-person='5' class=""vtable-word-text"">estarían teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%ADan-teniendo&amp;key=b34728dbd3f052396fab4c7552c59b57"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futureContinuous"" data-person='5' class=""vtable-word-text"">estarán teniendo</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=estar%C3%A1n-teniendo&amp;key=ca0a6feddce545d32568a24b1e3d5518"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr></table></div><div class=""vtable-header""><div><a href=""/guide/spanish-present-perfect-indicative"" class=""vtable-label vtable-label-link""><span class=""vtable-label-link-text"">Perfect</span><span class=""icon-question-mark""></span></a></div></div><div class=""vtable-wrapper""><table class=""vtable""><tr class=""vtable-head-row""><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""/guide/spanish-present-perfect-indicative"" title=""Tense example: *He has spoken* at several meetings."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Present</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-preterite-perfect-tense"" title=""Tense example: *He had (already) spoken* to her when I got there."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Preterite</span></a></td><td class=""vtable-title""><a href=""/guide/past-perfect-forms-and-uses"" title=""Tense example: *He had spoken* a lot before the train left."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Past</span></a></td><td class=""vtable-title""><a href=""/guide/conditional-perfect-forms-and-uses"" title=""Tense example: *He would have spoken*, but he was sick."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Conditional</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-future-perfect-indicative"" title=""Tense example: *He will have spoken* in three different countries by tomorrow."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Future</span></a></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">yo</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfect"" data-person='0' class=""vtable-word-text"">he tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=he-tenido&amp;key=0abc2481cbe55e93e702e796c1dea3ef"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritPerfect"" data-person='0' class=""vtable-word-text"">hube tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hube-tenido&amp;key=4d481f9cadc8f3679f449e71fc54e0b5"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfect"" data-person='0' class=""vtable-word-text"">había tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hab%C3%ADa-tenido&amp;key=3ae016dd0c89913e908e0c0c330e9195"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalPerfect"" data-person='0' class=""vtable-word-text"">habría tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%ADa-tenido&amp;key=ce1f5b4d86ef8bd7110758eb7713a717"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfect"" data-person='0' class=""vtable-word-text"">habré tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%A9-tenido&amp;key=7213a421c28231dc6357dbecb267527d"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">tú</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfect"" data-person='1' class=""vtable-word-text"">has tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=has-tenido&amp;key=50eae34ed96375b7826ea3b58944e815"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritPerfect"" data-person='1' class=""vtable-word-text"">hubiste tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubiste-tenido&amp;key=bc2e1b7a018f85f6e3d418746910a0a4"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfect"" data-person='1' class=""vtable-word-text"">habías tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hab%C3%ADas-tenido&amp;key=275ce92b43d00272bc3129dc5d180d20"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalPerfect"" data-person='1' class=""vtable-word-text"">habrías tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%ADas-tenido&amp;key=2cb7d9622d7f1925b23a07a73af50c40"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfect"" data-person='1' class=""vtable-word-text"">habrás tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%A1s-tenido&amp;key=d18e1abfa9f0135b8de1bcef266cb8b5"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfect"" data-person='2' class=""vtable-word-text"">ha tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=ha-tenido&amp;key=f99c461e65b1716847dc13266f0c44a3"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritPerfect"" data-person='2' class=""vtable-word-text"">hubo tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubo-tenido&amp;key=83dec1b1e02bf4eee2303e3679ef171a"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfect"" data-person='2' class=""vtable-word-text"">había tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hab%C3%ADa-tenido&amp;key=3ae016dd0c89913e908e0c0c330e9195"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalPerfect"" data-person='2' class=""vtable-word-text"">habría tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%ADa-tenido&amp;key=ce1f5b4d86ef8bd7110758eb7713a717"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfect"" data-person='2' class=""vtable-word-text"">habrá tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%A1-tenido&amp;key=d469a4f560f6d405237cfcda15470e41"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfect"" data-person='3' class=""vtable-word-text"">hemos tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hemos-tenido&amp;key=42f4cc69e363c69251b248005b1205a9"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritPerfect"" data-person='3' class=""vtable-word-text"">hubimos tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubimos-tenido&amp;key=bfc3e991747a26d2fc0cf03ac31d49ac"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfect"" data-person='3' class=""vtable-word-text"">habíamos tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hab%C3%ADamos-tenido&amp;key=48a5874a34d2f8d03cc74fd161cc53ad"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalPerfect"" data-person='3' class=""vtable-word-text"">habríamos tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%ADamos-tenido&amp;key=55d6450721b3ae7e1d858f6c187764a9"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfect"" data-person='3' class=""vtable-word-text"">habremos tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habremos-tenido&amp;key=63ba8a33cd3527fbfdce267561c1a3c7"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfect"" data-person='4' class=""vtable-word-text"">habéis tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hab%C3%A9is-tenido&amp;key=049aea83befc4d957663b526696b5259"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritPerfect"" data-person='4' class=""vtable-word-text"">hubisteis tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubisteis-tenido&amp;key=9be7b5069eda376c8d8c4d41a0ed9f11"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfect"" data-person='4' class=""vtable-word-text"">habíais tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hab%C3%ADais-tenido&amp;key=30ac780224cfacef84078a6763e03ac2"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalPerfect"" data-person='4' class=""vtable-word-text"">habríais tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%ADais-tenido&amp;key=025b097a99fe0bdda6c1ba1aac8172b2"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfect"" data-person='4' class=""vtable-word-text"">habréis tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%A9is-tenido&amp;key=a792a33b22cfe521b31fb2c54bdd9da2"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfect"" data-person='5' class=""vtable-word-text"">han tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=han-tenido&amp;key=e792be1b7bc0a99a3efd7ce920c73e18"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""preteritPerfect"" data-person='5' class=""vtable-word-text"">hubieron tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubieron-tenido&amp;key=e12fff9029c4343db9e174313b26349f"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfect"" data-person='5' class=""vtable-word-text"">habían tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hab%C3%ADan-tenido&amp;key=477d210e99a6134cb8eb9ce80acde823"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""conditionalPerfect"" data-person='5' class=""vtable-word-text"">habrían tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%ADan-tenido&amp;key=b56dd5bde123b3357ae4c35169178385"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfect"" data-person='5' class=""vtable-word-text"">habrán tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=habr%C3%A1n-tenido&amp;key=24b8730f1d66f0361c7cd06fa9d521a2"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr></table></div><div class=""vtable-header""><div><a href=""/guide/spanish-present-perfect-subjunctive"" class=""vtable-label vtable-label-link""><span class=""vtable-label-link-text"">Perfect Subjunctive</span><span class=""icon-question-mark""></span></a></div></div><div class=""vtable-wrapper""><table class=""vtable""><tr class=""vtable-head-row""><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""/guide/spanish-present-perfect-subjunctive"" title=""Tense example: I doubt that *he has spoken* to the president."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Present</span></a></td><td class=""vtable-title""><a href=""/guide/past-perfect-subjunctive-forms"" title=""Tense example: She did not believe that *he had spoken* to a unicorn."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Past</span></a></td><td class=""vtable-title""><a href=""/guide/spanish-future-perfect-indicative"" title=""Tense example: It is possible that *he will have spoken* to the whole family."" data-toggle=""tooltip"" class=""vtable-title-text vtable-title-link""><span class=""vtable-title-link-text"">Future</span></a></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">yo</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfectSubjunctive"" data-person='0' class=""vtable-word-text"">haya tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=haya-tenido&amp;key=db7761e30309e1de50c4b90bf417efd8"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfectSubjunctive"" data-person='0' class=""vtable-word-text"">hubiera tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubiera-tenido&amp;key=16404c57be5cddff9fb224a8d512aa99"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfectSubjunctive"" data-person='0' class=""vtable-word-text"">hubiere tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubiere-tenido&amp;key=3f4dc4e212e4a943513b8c744bf0e3bf"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">tú</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfectSubjunctive"" data-person='1' class=""vtable-word-text"">hayas tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hayas-tenido&amp;key=3c369ab11ea91ed7fb9d7d9bcb95447b"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfectSubjunctive"" data-person='1' class=""vtable-word-text"">hubieras tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubieras-tenido&amp;key=5a8652950f3a5f8151a818b158bc2934"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfectSubjunctive"" data-person='1' class=""vtable-word-text"">hubieres tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubieres-tenido&amp;key=48baf8d8024ae40e150356229dc27381"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfectSubjunctive"" data-person='2' class=""vtable-word-text"">haya tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=haya-tenido&amp;key=db7761e30309e1de50c4b90bf417efd8"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfectSubjunctive"" data-person='2' class=""vtable-word-text"">hubiera tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubiera-tenido&amp;key=16404c57be5cddff9fb224a8d512aa99"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfectSubjunctive"" data-person='2' class=""vtable-word-text"">hubiere tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubiere-tenido&amp;key=3f4dc4e212e4a943513b8c744bf0e3bf"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfectSubjunctive"" data-person='3' class=""vtable-word-text"">hayamos tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hayamos-tenido&amp;key=bc92fe053286da3a8dcb5a73e8afe8f0"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfectSubjunctive"" data-person='3' class=""vtable-word-text"">hubiéramos tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubi%C3%A9ramos-tenido&amp;key=3c766bb588fee7907cba84826f0e90f7"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfectSubjunctive"" data-person='3' class=""vtable-word-text"">hubiéremos tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubi%C3%A9remos-tenido&amp;key=98e8136da490c36f4f081149054c4626"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfectSubjunctive"" data-person='4' class=""vtable-word-text"">hayáis tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hay%C3%A1is-tenido&amp;key=1afccb2dfb0019729bcbf1ca42271abc"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfectSubjunctive"" data-person='4' class=""vtable-word-text"">hubierais tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubierais-tenido&amp;key=0b9b14f1972782e276a542dda1f494ec"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfectSubjunctive"" data-person='4' class=""vtable-word-text"">hubiereis tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubiereis-tenido&amp;key=bd8dd3ead3bdf09ca511bca1c34c8bae"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr><tr class=""vtable-body-row""><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""presentPerfectSubjunctive"" data-person='5' class=""vtable-word-text"">hayan tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hayan-tenido&amp;key=0b0a5735350d537c2f1be0df1eebe84c"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""pastPerfectSubjunctive"" data-person='5' class=""vtable-word-text"">hubieran tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubieran-tenido&amp;key=9f3006cee65b702153380541a5399738"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td><td class=""vtable-word""><div class=""vtable-word-contents""><div data-toggle=""tooltip"" data-tense=""futurePerfectSubjunctive"" data-person='5' class=""vtable-word-text"">hubieren tenido</div><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=hubieren-tenido&amp;key=429f537bc5db76cadc7c9ccb7d27637e"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue-small""></span></a></div></td></tr></table></div><div class=""conj-footnote"">This data is provided by Onoma</div></div></div><div class=""card card-megaexamples""><div class=""card-title"">Examples<img src=""//n1.global.ssl.fastly.net/img/common/ajax_loader.gif"" width=""16"" height=""16"" class=""megaexamples-spinner""></div><div class=""megaexamples-container""><svg xmlns=""http://www.w3.org/2000/svg"" class=""megaexamples-loading"" width=""100%"" height=""221"" viewBox=""0 0 606 221"" version=""1.1"" preserveAspectRatio=""none"">
  <g stroke=""none"" stroke-width=""1"" fill=""none"" fill-rule=""evenodd"">
    <g transform=""translate(-254.000000, -945.000000)"">
      <g transform=""translate(222.000000, 80.000000)"">
        <g transform=""translate(12.000000, 755.000000)"">
          <g transform=""translate(20.000000, 110.000000)"">
            <rect fill=""#F3F3F3"" x=""0"" y=""49"" width=""606"" height=""1""/>
            <rect fill=""#F3F3F3"" x=""0"" y=""94"" width=""606"" height=""1""/>
            <rect fill=""#F3F3F3"" x=""0"" y=""157"" width=""606"" height=""1""/>
            <rect fill=""#F3F3F3"" x=""0"" y=""220"" width=""606"" height=""1""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""0"" y=""0"" width=""293"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""0"" y=""18"" width=""243"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#ECECEC"" x=""30"" y=""18"" width=""35"" height=""12""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""0"" y=""66"" width=""243"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#ECECEC"" x=""70"" y=""66"" width=""65"" height=""12""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""313"" y=""66"" width=""243"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#ECECEC"" x=""353"" y=""66"" width=""85"" height=""12""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""0"" y=""111"" width=""293"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""0"" y=""129"" width=""103"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""313"" y=""129"" width=""103"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#ECECEC"" x=""70"" y=""111"" width=""65"" height=""12""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""313"" y=""111"" width=""293"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#ECECEC"" x=""493"" y=""111"" width=""85"" height=""12""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""0"" y=""174"" width=""293"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""0"" y=""192"" width=""213"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""313"" y=""192"" width=""263"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#ECECEC"" x=""200"" y=""174"" width=""65"" height=""12""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""313"" y=""174"" width=""293"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#ECECEC"" x=""493"" y=""174"" width=""85"" height=""12""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#ECECEC"" x=""443"" y=""66"" width=""15"" height=""12""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""313"" y=""0"" width=""293"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#F7F7F7"" x=""313"" y=""18"" width=""243"" height=""12"" rx=""3""/>
            <rect class=""megaexamples-text-placeholder"" fill=""#ECECEC"" x=""353"" y=""18"" width=""115"" height=""12""/>
          </g>
        </g>
      </g>
    </g>
  </g>
</svg>
</div></div><div class=""card""><div class=""card-title"">Search history</div><div class=""search-history-items""></div></div><div class=""card sd-explore""><div class=""card-title"">Explore SpanishDict</div><div class=""sd-explore-text"">Search over 10,000 English and Spanish verb conjugations. Conjugate verbs in tenses including preterite, imperfect, future, conditional, subjunctive, irregular, and more. Enter the infinitive or conjugated form of the verb above to get started.</div><ul class=""sd-explore-sections""><li class=""sd-explore-section""><a href=""/dictionary"" data-sd-track-click-type=""explore_category"" data-sd-track-click-text=""dictionary"" class=""sd-explore-link sd-track-click""><div class=""icon-dictionary sd-explore-icon""></div><div class=""sd-explore-section-title"">Spanish Dictionary</div><div class=""sd-explore-section-desc"">Featuring more than 1 million translations</div></a></li><li class=""sd-explore-section""><a href=""/translation"" data-sd-track-click-type=""explore_category"" data-sd-track-click-text=""translation"" class=""sd-explore-link sd-track-click""><div class=""icon-translator sd-explore-icon""></div><div class=""sd-explore-section-title"">Spanish Translator</div><div class=""sd-explore-section-desc"">Quick and easy results from 3 translators</div></a></li><li class=""sd-explore-section""><a href=""/conjugation"" data-sd-track-click-type=""explore_category"" data-sd-track-click-text=""conjugation"" class=""sd-explore-link sd-track-click""><div class=""icon-conjugation sd-explore-icon""></div><div class=""sd-explore-section-title"">Verb Conjugations</div><div class=""sd-explore-section-desc"">Conjugations for every Spanish verb</div></a></li></ul><ul class=""sd-explore-sections""><li class=""sd-explore-section""><a href=""/flashcards"" data-sd-track-click-type=""explore_category"" data-sd-track-click-text=""flashcards"" class=""sd-explore-link sd-track-click""><div class=""icon-flashcards sd-explore-icon""></div><div class=""sd-explore-section-title"">Interactive Flashcards</div><div class=""sd-explore-section-desc"">Images and audio help you learn faster</div></a></li><li class=""sd-explore-section""><a href=""/guide"" data-sd-track-click-type=""explore_category"" data-sd-track-click-text=""guide"" class=""sd-explore-link sd-track-click""><div class=""icon-grammar sd-explore-icon""></div><div class=""sd-explore-section-title"">Language Guide</div><div class=""sd-explore-section-desc"">Expert articles on how to use the Spanish Language</div></a></li><li class=""sd-explore-section""><a href=""/wordoftheday"" data-sd-track-click-type=""explore_category"" data-sd-track-click-text=""word of the day"" class=""sd-explore-link sd-track-click""><div class=""icon-wotd sd-explore-icon""></div><div class=""sd-explore-section-title"">Word of the Day</div><div class=""sd-explore-section-desc"">Learn a new word each day</div></a></li></ul></div><div class=""card home-fluencia home-fluencia-variation2""><div class=""home-fluencia-left""><a href=""https://www.fluencia.com/learn-spanish/?utm_source=sd_desktop&amp;utm_medium=main_content_results&amp;h=1"" target=""_blank"" rel=""noopener"" data-sd-track-click-type=""fluencia"" class=""home-fluencia-img-link sd-track-click""><img src=""//n1.global.ssl.fastly.net/img/common/fluencia-tablet2.jpg"" class=""home-fluencia-img""></a></div><div class=""home-fluencia-right""><div class=""home-fluencia-title"">Learn Spanish with Fluencia</div><div class=""home-fluencia-description"">Try Fluencia, the new Spanish learning program from SpanishDict.</div><ul class=""home-fluencia-list""><li><div class=""home-fluencia-icn-smiley""></div><div class=""home-fluencia-list-item-title"">Fun and interactive</div></li><li><div class=""home-fluencia-icn-chart""></div><div class=""home-fluencia-list-item-title"">Highly effective</div></li></ul><ul class=""home-fluencia-list""><li><div class=""home-fluencia-icn-wand""></div><div class=""home-fluencia-list-item-title"">Easy to use</div></li><li><div class=""home-fluencia-icn-devices""></div><div class=""home-fluencia-list-item-title"">Works on any device</div></li></ul><a href=""https://www.fluencia.com/learn-spanish/?utm_source=sd_desktop&amp;utm_medium=main_content_results&amp;h=1"" target=""_blank"" rel=""noopener"" data-sd-track-click-type=""fluencia"" class=""home-fluencia-link sd-track-click"">Start Learning</a></div></div><div id=""adContent-container"" data-turbolinks-permanent class=""adContent sd-ad-container""><div class=""adContent ad-wrapper""><div id=""adContent"" class=""adContent ad-unit""></div><div data-target=""adContent"" style=""visibility: hidden; color: #555555 !important; font-size: 12px !important; display: inline-block !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""adContent pubnation sd-track-click ad-widget"">report this ad</div></div></div><div id=""feedback-query"" class=""card""><div class=""card-title"">Did this page answer your question?</div><form><div class=""feedback-1""><a href=""#"" class=""feedback-yes""><div class=""icon-smile""></div><span>Yes</span></a><a href=""#"" class=""feedback-no""><div class=""icon-frown""></div><span>No</span></a></div><div class=""feedback-2""><div id=""feedback-categories""><div class=""feedback-instructions"">The page is...</div><div class=""feedback-categories-select""><label><input type=""radio"" name=""category"" value=""inaccurate"">Inaccurate</label><label><input type=""radio"" name=""category"" value=""unclear"">Unclear</label><label><input type=""radio"" name=""category"" value=""missingTranslations"">Missing translations</label><label><input type=""radio"" name=""category"" value=""missingConjugations"">Missing conjugations</label><label><input type=""radio"" name=""category"" value=""other"">Other</label></div></div><div class=""feedback-instructions"">What can we do to improve?</div><textarea id=""feedback-comment"" placeholder=""Enter your comment..."" required class=""feedback-comment""></textarea><div class=""feedback-instructions"">How can we follow up with you? (Optional)</div><input id=""feedback-query-email"" type=""email"" placeholder=""Enter your email"" class=""feedback-query-email""><button type=""submit"" class=""feedback-2-submit"">Submit</button></div><div class=""feedback-3""><span>Your feedback was sent. We appreciate your comments!</span></div></form></div></div><div class=""sidebar-container""><div id=""adSide1-container"" data-turbolinks-permanent class=""adSide1 sd-ad-container""><div class=""adSide1 ad-wrapper""><div id=""adSide1"" class=""adSide1 ad-unit""></div><div data-target=""adSide1"" style=""visibility: hidden; color: #555555 !important; font-size: 12px !important; display: inline-block !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""adSide1 pubnation sd-track-click ad-widget"">report this ad</div></div></div><div class=""fluencia-sidebar-short card""><div class=""fluencia-sidebar-title-short"">Want to Learn Spanish?</div><a href=""https://www.fluencia.com/learn-spanish/?utm_source=sd_desktop&amp;utm_medium=sidebar&amp;h=2"" target=""_blank"" rel=""noopener"" data-sd-track-click-type=""fluencia"" class=""fluencia-sidebar-button-short sd-track-click"">Try Fluencia</a></div><div id=""adSide2-container"" data-turbolinks-permanent class=""adSide2 sd-ad-container""><div class=""adSide2 ad-wrapper""><div id=""adSide2"" class=""adSide2 ad-unit""></div><div data-target=""adSide2"" style=""visibility: hidden; color: #555555 !important; font-size: 12px !important; display: inline-block !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""adSide2 pubnation sd-track-click ad-widget"">report this ad</div></div></div><div class=""wotd-sidebar card""><div class=""card-title"">Word of the Day</div><div class=""wotd-sidebar-date"">Feb 3</div><a href=""/translate/patinar"" class=""wotd-sidebar-word"">patinar</a><span class=""wotd-sidebar-audio""><a href=""https://audio1.spanishdict.com/audio?lang=es&amp;text=patinar&amp;key=02a0d4b983e6fe5e828555942cefdf64"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start js-audio-refresh""><span class=""audio-blue""></span></a></span><div class=""wotd-sidebar-translation"">to skate</div><a href=""#"" data-sd-track-click-type=""wotd_widget_button"" data-sd-track-click-text=""Get the Word of the Day Email"" data-turbolinks=""false"" data-signup-origin=""WOTD Sidebar"" class=""wotd-sidebar-signup sd-track-click js-launch-wotd-signup"">Get the Word of the Day Email</a></div><div class=""lg-popular-articles card""><a href=""/guide"" class=""lg-popular-articles-title card-title"">Popular Articles</a><div class=""lg-popular-articles-list""><svg xmlns=""http://www.w3.org/2000/svg"" style=""display: none;"">
  <defs>
    <g fill=""none"" id=""article-icon"">
      <path fill=""#FFF"" d=""M0 0h9l1.8 1.7 2.2 2V16H0z""/>
      <path stroke=""#3E86C7"" d=""M.5.5v15h12V4L8.8.5H.5z""/>
      <path fill=""#3E86C7"" d=""M9 0l4 4H9""/>
      <path fill=""#84B4DF"" d=""M3 6h7v1H3zm0 2h4v1H3zm0 2h7v1H3zm0 2h4v1H3z""/>
    </g>
  </defs>
</svg>
<div class=""lg-article-container""><div class=""lg-article""><div class=""lg-article-icon""><svg width=""13"" height=""16"" viewBox=""0 0 13 16"" xmlns=""http://www.w3.org/2000/svg"">
  <use xlink:href=""#article-icon"" />
</svg>
</div><span class=""lg-article-link""><a href=""/guide/how-to-talk-about-years-in-spanish"" data-sd-track-click-type=""lg-popular-article"" data-sd-track-click-text=""How to Talk about Years in Spanish"" class=""sd-track-click"">How to Talk about Years in Spanish</a></span></div></div><div class=""lg-article-container""><div class=""lg-article""><div class=""lg-article-icon""><svg width=""13"" height=""16"" viewBox=""0 0 13 16"" xmlns=""http://www.w3.org/2000/svg"">
  <use xlink:href=""#article-icon"" />
</svg>
</div><span class=""lg-article-link""><a href=""/guide/measurements-in-spanish"" data-sd-track-click-type=""lg-popular-article"" data-sd-track-click-text=""Measurements in Spanish"" class=""sd-track-click"">Measurements in Spanish</a></span></div></div><div class=""lg-article-container""><div class=""lg-article""><div class=""lg-article-icon""><svg width=""13"" height=""16"" viewBox=""0 0 13 16"" xmlns=""http://www.w3.org/2000/svg"">
  <use xlink:href=""#article-icon"" />
</svg>
</div><span class=""lg-article-link""><a href=""/guide/december-word-of-the-day-crossword"" data-sd-track-click-type=""lg-popular-article"" data-sd-track-click-text=""December Word of the Day Crossword"" class=""sd-track-click"">December Word of the Day Crossword</a></span></div></div><div class=""lg-article-container""><div class=""lg-article""><div class=""lg-article-icon""><svg width=""13"" height=""16"" viewBox=""0 0 13 16"" xmlns=""http://www.w3.org/2000/svg"">
  <use xlink:href=""#article-icon"" />
</svg>
</div><span class=""lg-article-link""><a href=""/guide/spanish-subjunctive"" data-sd-track-click-type=""lg-popular-article"" data-sd-track-click-text=""Spanish Subjunctive"" class=""sd-track-click"">Spanish Subjunctive</a></span></div></div><div class=""lg-article-container""><div class=""lg-article""><div class=""lg-article-icon""><svg width=""13"" height=""16"" viewBox=""0 0 13 16"" xmlns=""http://www.w3.org/2000/svg"">
  <use xlink:href=""#article-icon"" />
</svg>
</div><span class=""lg-article-link""><a href=""/guide/ser-vs-estar"" data-sd-track-click-type=""lg-popular-article"" data-sd-track-click-text=""&quot;Ser&quot; vs. &quot;Estar&quot;"" class=""sd-track-click"">&quot;Ser&quot; vs. &quot;Estar&quot;</a>&nbsp;<a href=""/quizzes/41/ser-vs-estar"" class=""lg-quiz-link"">quiz</a></span></div></div></div><a href=""/guide"" class=""lg-popular-articles-view-all"">View all articles&nbsp;
&nbsp;<svg width=""5"" height=""8"" viewBox=""0 0 5 8"" xmlns=""http://www.w3.org/2000/svg""><path d=""M4.13 4.84L.87 8 0 7.16 3.27 4 0 .84.87 0 5 4"" fill=""#3E86C7""/></svg>
</a></div><div id=""adSideLong-container"" data-turbolinks-permanent class=""adSideLong sd-ad-container""><div class=""adSideLong ad-wrapper""><div id=""adSideLong"" class=""adSideLong ad-unit""></div><div data-target=""adSideLong"" style=""visibility: hidden; color: #555555 !important; font-size: 12px !important; display: inline-block !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""adSideLong pubnation sd-track-click ad-widget"">report this ad</div></div></div></div><div id=""adBot-container"" data-turbolinks-permanent class=""adBot sd-ad-container""><div class=""adBot ad-wrapper""><div id=""adBot"" class=""adBot ad-unit""></div><div data-target=""adBot"" style=""visibility: hidden; color: #555555 !important; font-size: 12px !important; display: inline-block !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""adBot pubnation sd-track-click ad-widget"">report this ad</div></div></div></div><div class=""footer-wrapper""><div class=""footer footer-container container""><div class=""footer-row""><div class=""footer-item""><h4>Stay Connected</h4><div class=""footer-social""><div class=""social""><a href=""http://www.facebook.com/pages/SpanishDict/92805940179""><div class=""social-icon social-facebook""></div></a><a href=""http://www.twitter.com/spanishdict""><div class=""social-icon social-twitter""></div></a><a href=""https://plus.google.com/106413713006018165072/posts""><div class=""social-icon social-gplus""></div></a></div></div></div><div class=""footer-item""><h4>iPhone</h4><a href=""https://itunes.apple.com/us/app/spanishdict/id332510494""><div class=""footer-external footer-external-appstore""></div></a></div><div class=""footer-item""><h4>Android</h4><a href=""https://play.google.com/store/apps/details?id=com.spanishdict.spanishdict&amp;referrer=utm_source%3Dsd-footer""><div class=""footer-external footer-external-google-play""></div></a></div><div class=""footer-item footer-item-last""><h4>Windows</h4><a href=""https://www.microsoft.com/store/apps/9nblggh6d17z""><div class=""footer-external footer-external-windows""></div></a></div></div><div class=""footer-site-links""><div class=""footer-half""><div class=""footer-item""><h4 class=""footer-header"">SpanishDict</h4><ul class=""footer-links""><li class=""footer-link""><a href=""/translation"">Spanish Translator</a></li><li class=""footer-link""><a href=""/dictionary"">Spanish Dictionary</a></li><li class=""footer-link""><a href=""/conjugation"">Verb Conjugations</a></li><li class=""footer-link""><a href=""/guide"">Language Guide</a></li><li class=""footer-link""><a href=""/wordoftheday"">Word of the Day</a></li><div class=""footer-additional-links hidden""><li class=""footer-link""><a href=""/learn"">Learn Spanish</a></li><li class=""footer-link""><a href=""/flashcards"">Flashcards</a></li><li class=""footer-link""><a href=""/traductor"">Traductor</a></li></div><a href=""#"" class=""footer-show-more""><span class=""footer-show-text"">More</span><span class=""footer-show-icon footer-show-more-icon""></span></a></ul></div><div class=""footer-item""><h4 class=""footer-header"">Spanish</h4><ul class=""footer-links""><li class=""footer-link""><a href=""/translate/feo"">feo</a></li><li class=""footer-link""><a href=""/translate/cariño"">cariño</a></li><li class=""footer-link""><a href=""/translate/verde"">verde</a></li><li class=""footer-link""><a href=""/translate/guapo"">guapo</a></li><li class=""footer-link""><a href=""/translate/rompecabezas"">rompecabezas</a></li><div class=""footer-additional-links hidden""><li class=""footer-link""><a href=""/translate/belleza"">belleza</a></li><li class=""footer-link""><a href=""/translate/hermosa"">hermosa</a></li><li class=""footer-link""><a href=""/translate/este"">este</a></li><li class=""footer-link""><a href=""/translate/carnicería"">carnicería</a></li><li class=""footer-link""><a href=""/translate/ser"">ser</a></li><li class=""footer-link""><a href=""/translate/leer"">leer</a></li><li class=""footer-link""><a href=""/translate/rico suave"">rico suave</a></li><li class=""footer-link""><a href=""/translate/vaca"">vaca</a></li><li class=""footer-link""><a href=""/translate/princesa"">princesa</a></li><li class=""footer-link""><a href=""/translate/chancla"">chancla</a></li></div><a href=""#"" class=""footer-show-more""><span class=""footer-show-text"">More</span><span class=""footer-show-icon footer-show-more-icon""></span></a></ul></div></div><div class=""footer-half""><div class=""footer-item""><h4 class=""footer-header"">English</h4><ul class=""footer-links""><li class=""footer-link""><a href=""/translate/power"">power</a></li><li class=""footer-link""><a href=""/translate/habitat"">habitat</a></li><li class=""footer-link""><a href=""/translate/indeed"">indeed</a></li><li class=""footer-link""><a href=""/translate/pitbull"">pitbull</a></li><li class=""footer-link""><a href=""/translate/spot"">spot</a></li><div class=""footer-additional-links hidden""><li class=""footer-link""><a href=""/translate/sunset"">sunset</a></li><li class=""footer-link""><a href=""/translate/streaming"">streaming</a></li><li class=""footer-link""><a href=""/translate/score"">score</a></li><li class=""footer-link""><a href=""/translate/eat"">eat</a></li><li class=""footer-link""><a href=""/translate/closet"">closet</a></li><li class=""footer-link""><a href=""/translate/game"">game</a></li><li class=""footer-link""><a href=""/translate/dream"">dream</a></li><li class=""footer-link""><a href=""/translate/feed"">feed</a></li><li class=""footer-link""><a href=""/translate/nafta"">nafta</a></li><li class=""footer-link""><a href=""/translate/light"">light</a></li></div><a href=""#"" class=""footer-show-more""><span class=""footer-show-text"">More</span><span class=""footer-show-icon footer-show-more-icon""></span></a></ul></div><div class=""footer-item""><h4 class=""footer-header"">Conjugations</h4><ul class=""footer-links""><li class=""footer-link""><a href=""/conjugate/escoger"">escoger</a></li><li class=""footer-link""><a href=""/conjugate/abrir"">abrir</a></li><li class=""footer-link""><a href=""/conjugate/enviar"">enviar</a></li><li class=""footer-link""><a href=""/conjugate/recoger"">recoger</a></li><li class=""footer-link""><a href=""/conjugate/proteger"">proteger</a></li><div class=""footer-additional-links hidden""><li class=""footer-link""><a href=""/conjugate/parecer"">parecer</a></li><li class=""footer-link""><a href=""/conjugate/quedarse"">quedarse</a></li><li class=""footer-link""><a href=""/conjugate/jugar"">jugar</a></li><li class=""footer-link""><a href=""/conjugate/correr"">correr</a></li><li class=""footer-link""><a href=""/conjugate/competir"">competir</a></li><li class=""footer-link""><a href=""/conjugate/nacer"">nacer</a></li><li class=""footer-link""><a href=""/conjugate/seguir"">seguir</a></li><li class=""footer-link""><a href=""/conjugate/ver"">ver</a></li><li class=""footer-link""><a href=""/conjugate/acabar"">acabar</a></li><li class=""footer-link""><a href=""/conjugate/huir"">huir</a></li></div><a href=""#"" class=""footer-show-more""><span class=""footer-show-text"">More</span><span class=""footer-show-icon footer-show-more-icon""></span></a></ul></div></div></div><div class=""footer-meta""><div class=""footer-meta-list""><ul><li><a href=""/company/about"">About</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""http://curiositymedia.com/careers"">We're hiring!</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""/company/privacy"">Privacy</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""/company/tos"">Terms</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""/verbos/tener"" data-turbolinks=""false"" class=""go-language"">Español</a></li></ul></div><span class=""footer-copyright"">&copy; Curiosity Media, Inc.</span></div></div></div><script id=""sd_analytics"" data-ustatus=""anon"" data-site=""desktop"" data-q=""tener"" data-qFull=""tener"" data-type=""conjugation"" data-is_found=""1"" data-browser_lang=""en-US""></script><script type=""text/javascript"">var _qevents = _qevents || [];
(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == ""https:"" ? ""https://secure"" : ""http://edge"") + "".quantserve.com/quant.js"";
elem.async = true;
elem.type = ""text/javascript"";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
})();
_qevents.push({
qacct:""p-J6UREPaB79qHH""
});
</script><noscript><div style=""display:none;""><img src=""//pixel.quantserve.com/pixel/p-J6UREPaB79qHH.gif"" style=""border: none;"" height=""1"" width=""1"" alt=""Quantcast""></div></noscript> <script type=""text/javascript"">
   ga('set', 'location', document.location);
   if(window.SD_TURBOLINKS_VISIT) {
     ga('set', 'referrer', null);
   }   ga('set', 'dimension1', 'anonymous');   ga('set', 'dimension2', 'conjugate');   ga('set', 'dimension3', 'false');
   ga('set', 'dimension4', '100');
   var pageCategory = SD_PAGE_CATEGORY === 'guide-nav' ? 'guide' : SD_PAGE_CATEGORY;
   ga('set', 'dimension9', pageCategory);
   ga('require', 'displayfeatures');
   ga('send', 'pageview');
 </script><script type=""text/javascript"">var partnerConfig = {
  ""iid"": ""13359348""
};
var createSvBeacon = function(){
  var p = location.protocol;
  var url = (p === 'https:'? p : 'http:') + ""//ap.lijit.com/www/sovrn_beacon_standalone/sovrn_standalone_beacon.js?iid="" + partnerConfig.iid;
  var scr = document.createElement(""script"");
  scr.id = ""sBeacon"";
  scr.src = url;
  scr.async = false;
  var s0 = document.getElementsByTagName('script')[0];
  s0.parentNode.insertBefore(scr, s0);
};
createSvBeacon();</script><script>var _pnq = [];
_pnq.push(['setId', 'pn-053b5b698be780ef']);
(function() {
  var pnr = document.createElement('script');
  pnr.type = 'text/javascript';
  pnr.async = true;
  pnr.crossOrigin = 'anonymous';
  pnr.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'report-ads-to.pubnation.com/dist/pnr.js?t=pn-053b5b698be780ef';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(pnr, s);
})();</script><div class=""feedback-tab""><button>Feedback</button></div><div id=""feedback-modal"" class=""feedback-modal sd-modal modal fade""><div class=""modal-dialog""><div class=""modal-content""><div class=""modal-body""><div class=""sd-modal-title"">Feedback</div><div class=""sd-modal-close feedback-modal-close""></div><div class=""sd-modal-message feedback-modal-message""></div><div class=""sd-modal-content feedback-modal-content""><div class=""sd-modal-desc"">SpanishDict is devoted to improving our site based on user feedback and introducing new and innovative features that will continue to help people learn and love the Spanish language.&nbsp;Have a suggestion, idea, or comment?&nbsp;Send us your feedback.</div><form><label for=""feedback-name"" class=""sd-modal-label"">Your name</label><input id=""feedback-name"" type=""text"" placeholder=""Your name""><label for=""feedback-email"" class=""sd-modal-label"">Email address</label><input id=""feedback-email"" type=""email"" placeholder=""Email address""><label for=""feedback-subject"" class=""sd-modal-label"">Subject</label><input id=""feedback-subject"" type=""text"" placeholder=""Subject"" required><label for=""feedback-message"" class=""sd-modal-label"">Message</label><textarea id=""feedback-message"" placeholder=""Message"" rows=""5"" required></textarea><input type=""submit"" value=""Send"" class=""sd-modal-submit feedback-modal-submit""></form></div></div></div></div></div><div id=""templates"" class=""hidden""><div id=""wotd-overlay-template""><div class=""hidden modal sd-overlay js-wotd-overlay""><div class=""modal-dialog""><div class=""modal-content""><div class=""modal-body""><button data-dismiss=""modal"" aria-label=""Close"" class=""sd-overlay-close""><span aria-hidden=""true"">&times;</span></button><div class=""sd-overlay-modal-icon""><svg viewBox=""0 0 54 54"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink""><defs><circle id=""a"" cx=""24"" cy=""24"" r=""24""/><path d=""M6.8.1s1 1-.4 4.8-3.5 4.5-3.5 4.5S1.6 4.3.2 4C-1 3.7 4-.7 6.8 0z"" id=""c""/><path d=""M8.6.2s-1.1-.9-4.7 1S0 5.3 0 5.3s5.2.6 5.6 2c.5 1.4 4.2-4.4 3-7z"" id=""d""/><path d=""M4.2 18.8s-2.1.4-3.4-1C-.5 16.4.5 14 .5 14s.3-1.3 1.1-2.7c1-1.8 2-3 2-3C4.3 7.3 5 6.5 6 5.7 11.5.7 15.8 0 17.8 0l1 .1c.2 0 .2.1.2.2 0 .2 1 6-6.5 13.7 0 0-1 1-2.4 2 0 0-1.8 1.2-3.5 2l-2.4.8z"" id=""e""/></defs><g transform=""translate(3 3)"" fill=""none"" fill-rule=""evenodd""><mask id=""b"" fill=""#fff""><use xlink:href=""#a""/></mask><use fill=""#B6DCFE"" xlink:href=""#a""/><circle stroke=""#FFF"" stroke-width=""3"" cx=""24"" cy=""24"" r=""25.5""/><path fill=""#FFF"" opacity="".5"" mask=""url(#b)"" d=""M20.4 32.5L16.9 48l12 .7 1.6-1-3.6-15.4-2 4.2-3.8-1""/><g mask=""url(#b)""><path d=""M27 32.1v1.2c0 3.4-3.3 10-3.3 10s-3.4-6.6-3.4-10l.1-1c.3 0 1.1 2 3.3 2 2.2 0 3-2 3.2-2.2z"" fill=""#FCBD34""/><use fill=""#3E86C7"" xlink:href=""#c"" transform=""rotate(-45 45.6 -14.4)""/><use fill=""#3E86C7"" xlink:href=""#d"" transform=""rotate(-45 41.8 -.6)""/><g transform=""rotate(-45 30.4 -2)""><mask id=""f"" fill=""#fff""><use xlink:href=""#e""/></mask><use fill=""#FFF"" xlink:href=""#e""/><path d=""M20-.4S9.4 5-6.5 26.2c-15.9 21.3 26.2 2.1 26.2 2.1l5.8-30.2-5.7 1.5z"" fill=""#D7D7D7"" opacity="".3"" mask=""url(#f)""/></g><path d=""M25.2 18.8c1-1 1-2.6 0-3.6s-2.7-1-3.7 0a2.6 2.6 0 0 0 3.6 3.6z"" fill=""#3E86C7""/><path d=""M24.4 18c.6-.5.6-1.5 0-2-.6-.6-1.6-.6-2.1 0-.6.5-.6 1.5 0 2 .6.6 1.5.6 2 0z"" fill=""#FFF""/></g><g mask=""url(#b)"" fill=""#FFF""><g transform=""translate(34.5 7.5)""><circle cx=""2.6"" cy=""4.1"" r=""2.6""/><circle cx=""8.6"" cy=""4.1"" r=""2.6""/><circle cx=""5.6"" cy=""3.4"" r=""3.4""/></g></g><g mask=""url(#b)"" fill=""#FFF""><g transform=""translate(-3.8 15)""><circle cx=""3.8"" cy=""3.8"" r=""3.8""/><circle cx=""9.8"" cy=""3.8"" r=""3.8""/><circle cx=""6"" cy=""3.8"" r=""3.8""/></g></g></g></svg></div><div class=""sd-overlay-header"">Boost your vocabulary!</div><div class=""sd-overlay-subheader"">Get the SpanishDict Word of the Day in your inbox</div><div class=""sd-overlay-separator""></div><div class=""js-initial-content""><div class=""wotd-overlay-reasons-wrapper""><ul class=""wotd-overlay-reasons""><li class=""wotd-overlay-reasons-item""><svg viewBox=""0 0 20 20"" xmlns=""http://www.w3.org/2000/svg""><g fill=""none"" fill-rule=""evenodd""><g fill=""#15B549""><g><g><path d=""M10 0C4.5 0 0 4.5 0 10s4.5 10 10 10 10-4.5 10-10c.13-5.5-4.36-10-10-10zm0 18c-4.34 0-8-3.53-8-8 0-4.34 3.53-8 8-8 4.34 0 8 3.53 8 8 0 4.34-3.53 8-8 8zm2.72-10.85l-3 3.1c-.25.26-.6.26-.85 0l-1.2-1.23c-.48-.5-1.2-.5-1.68 0-.5.5-.5 1.24 0 1.74l2.5 2.6c.5.5 1.2.5 1.7 0l.82-.87 3.5-3.6c.47-.5.47-1.26 0-1.76-.6-.37-1.33-.37-1.8 0z""/></g></g></g></g></svg>
<span>A new word each day</span></li><li class=""wotd-overlay-reasons-item""><svg viewBox=""0 0 20 20"" xmlns=""http://www.w3.org/2000/svg""><g fill=""none"" fill-rule=""evenodd""><g fill=""#15B549""><g><g><path d=""M10 0C4.5 0 0 4.5 0 10s4.5 10 10 10 10-4.5 10-10c.13-5.5-4.36-10-10-10zm0 18c-4.34 0-8-3.53-8-8 0-4.34 3.53-8 8-8 4.34 0 8 3.53 8 8 0 4.34-3.53 8-8 8zm2.72-10.85l-3 3.1c-.25.26-.6.26-.85 0l-1.2-1.23c-.48-.5-1.2-.5-1.68 0-.5.5-.5 1.24 0 1.74l2.5 2.6c.5.5 1.2.5 1.7 0l.82-.87 3.5-3.6c.47-.5.47-1.26 0-1.76-.6-.37-1.33-.37-1.8 0z""/></g></g></g></g></svg>
<span>Native speaker examples</span></li><li class=""wotd-overlay-reasons-item""><svg viewBox=""0 0 20 20"" xmlns=""http://www.w3.org/2000/svg""><g fill=""none"" fill-rule=""evenodd""><g fill=""#15B549""><g><g><path d=""M10 0C4.5 0 0 4.5 0 10s4.5 10 10 10 10-4.5 10-10c.13-5.5-4.36-10-10-10zm0 18c-4.34 0-8-3.53-8-8 0-4.34 3.53-8 8-8 4.34 0 8 3.53 8 8 0 4.34-3.53 8-8 8zm2.72-10.85l-3 3.1c-.25.26-.6.26-.85 0l-1.2-1.23c-.48-.5-1.2-.5-1.68 0-.5.5-.5 1.24 0 1.74l2.5 2.6c.5.5 1.2.5 1.7 0l.82-.87 3.5-3.6c.47-.5.47-1.26 0-1.76-.6-.37-1.33-.37-1.8 0z""/></g></g></g></g></svg>
<span>Quick vocabulary challenges</span></li></ul></div><button data-sd-track-click-type=""wotd_overlay"" data-sd-track-click-text=""Get the Word of the Day"" class=""sd-overlay-action js-wotd-signup sd-track-click"">Get the Word of the Day</button></div><div class=""hidden js-signup-content""><a href=""/social/google/signup"" data-sd-track-click-type=""signup_submission"" data-sd-track-click-text=""Signup with Google"" class=""direct-social-button direct-social-button-google js-social-signup-link sd-track-click""><div class=""direct-social-button-icon""><svg width=""26"" height=""26"" viewBox=""0 0 26 26"" xmlns=""http://www.w3.org/2000/svg""><g fill=""none"" fill-rule=""evenodd""><path d=""M25.48 13.295c0-.92-.083-1.808-.236-2.66H13v5.03h6.996c-.3 1.625-1.217 3.002-2.594 3.924v3.26h4.202c2.458-2.264 3.876-5.596 3.876-9.556z"" fill=""#4285F4""/><path d=""M13 26c3.51 0 6.453-1.164 8.604-3.15l-4.202-3.26c-1.164.78-2.653 1.24-4.402 1.24-3.386 0-6.252-2.287-7.274-5.36H1.383v3.368C3.523 23.088 7.918 26 13 26z"" fill=""#34A853""/><path d=""M5.726 15.47c-.26-.78-.408-1.613-.408-2.47 0-.857.148-1.69.408-2.47V7.162H1.383C.503 8.917 0 10.902 0 13s.502 4.083 1.383 5.838l4.343-3.368z"" fill=""#FBBC05""/><path d=""M13 5.17c1.91 0 3.622.656 4.97 1.945l3.728-3.73C19.448 1.29 16.504 0 13 0 7.918 0 3.522 2.913 1.383 7.162l4.343 3.368C6.748 7.457 9.614 5.17 13 5.17z"" fill=""#EA4335""/></g></svg>
</div><div class=""direct-social-button-separator""></div><div class=""direct-social-button-text"">Sign up with Google</div></a><a href=""/social/facebook/signup"" data-sd-track-click-type=""signup_submission"" data-sd-track-click-text=""Signup with Facebook"" class=""direct-social-button direct-social-button-facebook js-social-signup-link sd-track-click""><div class=""direct-social-button-icon""><svg width=""26"" height=""26"" viewBox=""0 0 26 26"" xmlns=""http://www.w3.org/2000/svg""><path d=""M24.565 26H1.435C.642 26 0 25.357 0 24.565V1.435C0 .642.642 0 1.435 0h23.13C25.357 0 26 .642 26 1.435v23.13c0 .792-.643 1.435-1.435 1.435zm-6.625 0V15.932h3.38l.505-3.924H17.94V9.502c0-1.136.315-1.91 1.944-1.91h2.078V4.08c-.36-.048-1.593-.155-3.028-.155-2.996 0-5.047 1.83-5.047 5.187v2.894H10.5v3.924h3.387V26h4.053z"" fill=""#FFF"" fill-rule=""evenodd""/></svg>
</div><div class=""direct-social-button-separator""></div><div class=""direct-social-button-text"">Sign up with Facebook</div></a><div class=""direct-signup-separator""><div class=""direct-signup-separator-bar""></div><div class=""direct-signup-separator-text"">or</div><div class=""direct-signup-separator-bar""></div></div><div class=""alternative-option-text""><a href=""#"" data-sd-track-click-type=""signup_method_selection"" data-sd-track-click-text=""Show Signup with Email"" class=""direct-link js-show-signup-form sd-track-click"">Sign up with email</a></div><form method=""POST"" action=""https://www.spanishdict.com/users/signup"" novalidate class=""direct-form direct-form-signup js-signup-form""><label class=""direct-form-input-group""><div class=""direct-form-text-label"">Username</div><input type=""username"" name=""username"" class=""direct-form-text-input js-signup-username-input""><div class=""direct-form-inline-error js-signup-username-error""></div></label><label class=""direct-form-input-group""><div class=""direct-form-text-label"">Email address</div><input type=""email"" name=""email"" class=""direct-form-text-input js-signup-email-input""><div class=""direct-form-inline-error js-signup-email-error""></div></label><label class=""direct-form-input-group""><div class=""direct-form-text-label"">Password</div><input type=""password"" name=""password"" class=""direct-form-text-input js-signup-password-input""><div class=""direct-form-inline-error js-signup-password-error""></div></label><label class=""direct-form-input-group direct-form-input-group-centered""><input type=""checkbox"" name=""wotdSignup"" value=""true"" checked class=""direct-form-checkbox-input""><span class=""direct-form-checkbox-label"">Get the Word of the Day by email</span></label><button data-sd-track-click-type=""signup_submission"" data-sd-track-click-text=""Signup with Email"" class=""direct-form-button direct-form-button-squished js-signup-button sd-track-click"">Get Started</button></form><div class=""direct-fine-print""><span>By signing up, you agree to our</span>&nbsp;<a href=""/company/privacy"" class=""direct-link"">Privacy Policy</a>&nbsp;<span>and</span>&nbsp;<a href=""/company/tos"" class=""direct-link"">Terms of Service.</a></div></div></div></div></div></div></div><div id=""conjugation-overlay-template""><div class=""hidden modal sd-overlay js-conjugation-overlay""><div class=""modal-dialog""><div class=""modal-content""><div class=""modal-body""><button data-dismiss=""modal"" aria-label=""Close"" class=""sd-overlay-close""><span aria-hidden=""true"">&times;</span></button><div class=""sd-overlay-modal-icon""><img src=""//n1.global.ssl.fastly.net/img/common/brain-medallion.jpg""></div><div class=""sd-overlay-header"">Conjugation Games</div><div class=""sd-overlay-subheader"">Choose a practice game</div><div class=""conjugation-overlay-link-section""><ul class=""conjugation-overlay-items""><li class=""conjugation-overlay-item""><div class=""conjugation-overlay-dice""><svg viewBox=""0 0 16 16"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"">
  <defs>
    <path id=""dice-a"" d=""M0 0h16v16H0z""/>
    <rect id=""dice-c"" x=""3.35"" y=""1.35"" width=""12"" height=""12"" rx=""4""/>
    <rect id=""dice-d"" width=""12"" height=""12"" rx=""4""/>
  </defs>
  <g fill=""none"" fill-rule=""evenodd"">
    <mask id=""dice-b"" fill=""#fff"">
      <use xlink:href=""#dice-a""/>
    </mask>
    <use fill-opacity=""0"" fill=""#D8D8D8"" xlink:href=""#dice-a""/>
    <g mask=""url(#dice-b)"">
      <g transform=""rotate(15 8.848 3.55)"">
        <use fill=""#E2F1FF"" xlink:href=""#dice-c""/>
        <rect stroke=""#3E86C7"" x=""3.85"" y=""1.85"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <g transform=""rotate(-15 17.894 3.702)"">
        <use fill=""#FFF"" xlink:href=""#dice-d""/>
        <rect stroke=""#3E86C7"" x="".5"" y="".5"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <path d=""M7.26 9.97c-.53.14-1.08-.18-1.23-.7-.14-.54.18-1.1.7-1.24.54-.14 1.1.18 1.24.7.14.54-.18 1.1-.7 1.24zm1.4-2.45c-.52.14-1.07-.18-1.2-.7-.15-.54.16-1.1.7-1.24.53-.14 1.08.18 1.22.7.14.55-.17 1.1-.7 1.24zm-2.82 4.9c-.53.14-1.08-.18-1.22-.7-.14-.55.17-1.1.7-1.24.54-.14 1.1.18 1.23.7.14.54-.17 1.1-.7 1.24z"" fill=""#3E86C7""/>
    </g>
  </g>
</svg>
</div><a href=""/quizzes/36/spanish-present-tense-forms"" data-sd-track-click-type=""conjugation_overlay"" data-sd-track-click-text=""Present"">Present</a></li><li class=""conjugation-overlay-item""><div class=""conjugation-overlay-dice""><svg viewBox=""0 0 16 16"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"">
  <defs>
    <path id=""dice-a"" d=""M0 0h16v16H0z""/>
    <rect id=""dice-c"" x=""3.35"" y=""1.35"" width=""12"" height=""12"" rx=""4""/>
    <rect id=""dice-d"" width=""12"" height=""12"" rx=""4""/>
  </defs>
  <g fill=""none"" fill-rule=""evenodd"">
    <mask id=""dice-b"" fill=""#fff"">
      <use xlink:href=""#dice-a""/>
    </mask>
    <use fill-opacity=""0"" fill=""#D8D8D8"" xlink:href=""#dice-a""/>
    <g mask=""url(#dice-b)"">
      <g transform=""rotate(15 8.848 3.55)"">
        <use fill=""#E2F1FF"" xlink:href=""#dice-c""/>
        <rect stroke=""#3E86C7"" x=""3.85"" y=""1.85"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <g transform=""rotate(-15 17.894 3.702)"">
        <use fill=""#FFF"" xlink:href=""#dice-d""/>
        <rect stroke=""#3E86C7"" x="".5"" y="".5"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <path d=""M7.26 9.97c-.53.14-1.08-.18-1.23-.7-.14-.54.18-1.1.7-1.24.54-.14 1.1.18 1.24.7.14.54-.18 1.1-.7 1.24zm1.4-2.45c-.52.14-1.07-.18-1.2-.7-.15-.54.16-1.1.7-1.24.53-.14 1.08.18 1.22.7.14.55-.17 1.1-.7 1.24zm-2.82 4.9c-.53.14-1.08-.18-1.22-.7-.14-.55.17-1.1.7-1.24.54-.14 1.1.18 1.23.7.14.54-.17 1.1-.7 1.24z"" fill=""#3E86C7""/>
    </g>
  </g>
</svg>
</div><a href=""/quizzes/60/preterite-tense-forms"" data-sd-track-click-type=""conjugation_overlay"" data-sd-track-click-text=""Preterite"">Preterite</a></li><li class=""conjugation-overlay-item""><div class=""conjugation-overlay-dice""><svg viewBox=""0 0 16 16"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"">
  <defs>
    <path id=""dice-a"" d=""M0 0h16v16H0z""/>
    <rect id=""dice-c"" x=""3.35"" y=""1.35"" width=""12"" height=""12"" rx=""4""/>
    <rect id=""dice-d"" width=""12"" height=""12"" rx=""4""/>
  </defs>
  <g fill=""none"" fill-rule=""evenodd"">
    <mask id=""dice-b"" fill=""#fff"">
      <use xlink:href=""#dice-a""/>
    </mask>
    <use fill-opacity=""0"" fill=""#D8D8D8"" xlink:href=""#dice-a""/>
    <g mask=""url(#dice-b)"">
      <g transform=""rotate(15 8.848 3.55)"">
        <use fill=""#E2F1FF"" xlink:href=""#dice-c""/>
        <rect stroke=""#3E86C7"" x=""3.85"" y=""1.85"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <g transform=""rotate(-15 17.894 3.702)"">
        <use fill=""#FFF"" xlink:href=""#dice-d""/>
        <rect stroke=""#3E86C7"" x="".5"" y="".5"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <path d=""M7.26 9.97c-.53.14-1.08-.18-1.23-.7-.14-.54.18-1.1.7-1.24.54-.14 1.1.18 1.24.7.14.54-.18 1.1-.7 1.24zm1.4-2.45c-.52.14-1.07-.18-1.2-.7-.15-.54.16-1.1.7-1.24.53-.14 1.08.18 1.22.7.14.55-.17 1.1-.7 1.24zm-2.82 4.9c-.53.14-1.08-.18-1.22-.7-.14-.55.17-1.1.7-1.24.54-.14 1.1.18 1.23.7.14.54-.17 1.1-.7 1.24z"" fill=""#3E86C7""/>
    </g>
  </g>
</svg>
</div><a href=""/quizzes/38/stem-changing-verbs"" data-sd-track-click-type=""conjugation_overlay"" data-sd-track-click-text=""Stem Changes"">Stem Changes</a></li></ul><ul class=""conjugation-overlay-items""><li class=""conjugation-overlay-item""><div class=""conjugation-overlay-dice""><svg viewBox=""0 0 16 16"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"">
  <defs>
    <path id=""dice-a"" d=""M0 0h16v16H0z""/>
    <rect id=""dice-c"" x=""3.35"" y=""1.35"" width=""12"" height=""12"" rx=""4""/>
    <rect id=""dice-d"" width=""12"" height=""12"" rx=""4""/>
  </defs>
  <g fill=""none"" fill-rule=""evenodd"">
    <mask id=""dice-b"" fill=""#fff"">
      <use xlink:href=""#dice-a""/>
    </mask>
    <use fill-opacity=""0"" fill=""#D8D8D8"" xlink:href=""#dice-a""/>
    <g mask=""url(#dice-b)"">
      <g transform=""rotate(15 8.848 3.55)"">
        <use fill=""#E2F1FF"" xlink:href=""#dice-c""/>
        <rect stroke=""#3E86C7"" x=""3.85"" y=""1.85"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <g transform=""rotate(-15 17.894 3.702)"">
        <use fill=""#FFF"" xlink:href=""#dice-d""/>
        <rect stroke=""#3E86C7"" x="".5"" y="".5"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <path d=""M7.26 9.97c-.53.14-1.08-.18-1.23-.7-.14-.54.18-1.1.7-1.24.54-.14 1.1.18 1.24.7.14.54-.18 1.1-.7 1.24zm1.4-2.45c-.52.14-1.07-.18-1.2-.7-.15-.54.16-1.1.7-1.24.53-.14 1.08.18 1.22.7.14.55-.17 1.1-.7 1.24zm-2.82 4.9c-.53.14-1.08-.18-1.22-.7-.14-.55.17-1.1.7-1.24.54-.14 1.1.18 1.23.7.14.54-.17 1.1-.7 1.24z"" fill=""#3E86C7""/>
    </g>
  </g>
</svg>
</div><a href=""/quizzes/85/reflexive-verbs-and-reflexive-pronouns"" data-sd-track-click-type=""conjugation_overlay"" data-sd-track-click-text=""Reflexive"">Reflexive</a></li><li class=""conjugation-overlay-item""><div class=""conjugation-overlay-dice""><svg viewBox=""0 0 16 16"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"">
  <defs>
    <path id=""dice-a"" d=""M0 0h16v16H0z""/>
    <rect id=""dice-c"" x=""3.35"" y=""1.35"" width=""12"" height=""12"" rx=""4""/>
    <rect id=""dice-d"" width=""12"" height=""12"" rx=""4""/>
  </defs>
  <g fill=""none"" fill-rule=""evenodd"">
    <mask id=""dice-b"" fill=""#fff"">
      <use xlink:href=""#dice-a""/>
    </mask>
    <use fill-opacity=""0"" fill=""#D8D8D8"" xlink:href=""#dice-a""/>
    <g mask=""url(#dice-b)"">
      <g transform=""rotate(15 8.848 3.55)"">
        <use fill=""#E2F1FF"" xlink:href=""#dice-c""/>
        <rect stroke=""#3E86C7"" x=""3.85"" y=""1.85"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <g transform=""rotate(-15 17.894 3.702)"">
        <use fill=""#FFF"" xlink:href=""#dice-d""/>
        <rect stroke=""#3E86C7"" x="".5"" y="".5"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <path d=""M7.26 9.97c-.53.14-1.08-.18-1.23-.7-.14-.54.18-1.1.7-1.24.54-.14 1.1.18 1.24.7.14.54-.18 1.1-.7 1.24zm1.4-2.45c-.52.14-1.07-.18-1.2-.7-.15-.54.16-1.1.7-1.24.53-.14 1.08.18 1.22.7.14.55-.17 1.1-.7 1.24zm-2.82 4.9c-.53.14-1.08-.18-1.22-.7-.14-.55.17-1.1.7-1.24.54-.14 1.1.18 1.23.7.14.54-.17 1.1-.7 1.24z"" fill=""#3E86C7""/>
    </g>
  </g>
</svg>
</div><a href=""/quizzes/64/preterite-vs-imperfect-in-spanish"" data-sd-track-click-type=""conjugation_overlay"" data-sd-track-click-text=""Preterite vs. Imperfect"">Preterite vs. Imperfect</a></li><li class=""conjugation-overlay-item""><div class=""conjugation-overlay-dice""><svg viewBox=""0 0 16 16"" xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"">
  <defs>
    <path id=""dice-a"" d=""M0 0h16v16H0z""/>
    <rect id=""dice-c"" x=""3.35"" y=""1.35"" width=""12"" height=""12"" rx=""4""/>
    <rect id=""dice-d"" width=""12"" height=""12"" rx=""4""/>
  </defs>
  <g fill=""none"" fill-rule=""evenodd"">
    <mask id=""dice-b"" fill=""#fff"">
      <use xlink:href=""#dice-a""/>
    </mask>
    <use fill-opacity=""0"" fill=""#D8D8D8"" xlink:href=""#dice-a""/>
    <g mask=""url(#dice-b)"">
      <g transform=""rotate(15 8.848 3.55)"">
        <use fill=""#E2F1FF"" xlink:href=""#dice-c""/>
        <rect stroke=""#3E86C7"" x=""3.85"" y=""1.85"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <g transform=""rotate(-15 17.894 3.702)"">
        <use fill=""#FFF"" xlink:href=""#dice-d""/>
        <rect stroke=""#3E86C7"" x="".5"" y="".5"" width=""11"" height=""11"" rx=""2""/>
      </g>
      <path d=""M7.26 9.97c-.53.14-1.08-.18-1.23-.7-.14-.54.18-1.1.7-1.24.54-.14 1.1.18 1.24.7.14.54-.18 1.1-.7 1.24zm1.4-2.45c-.52.14-1.07-.18-1.2-.7-.15-.54.16-1.1.7-1.24.53-.14 1.08.18 1.22.7.14.55-.17 1.1-.7 1.24zm-2.82 4.9c-.53.14-1.08-.18-1.22-.7-.14-.55.17-1.1.7-1.24.54-.14 1.1.18 1.23.7.14.54-.17 1.1-.7 1.24z"" fill=""#3E86C7""/>
    </g>
  </g>
</svg>
</div><a href=""/quizzes/68/subjunctive-vs-indicative-in-spanish"" data-sd-track-click-type=""conjugation_overlay"" data-sd-track-click-text=""Subjunctive vs. Indicative"">Subjunctive vs. Indicative</a></li></ul></div><a href=""/guide"" data-sd-track-click-type=""conjugation_overlay"" data-sd-track-click-text=""More Games"" class=""sd-overlay-action sd-track-click"">More Games</a></div></div></div></div></div></div></body></html>";

        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************

        public static string Page2=
@"<!DOCTYPE html><html data-placeholder-focus=""false""><head><meta http-equiv=""Content-type"" content=""text/html; charset=utf-8""><link rel=""publisher"" href=""https://plus.google.com/106413713006018165072""><link rel=""search"" title=""SpanishDict.com"" type=""application/opensearchdescription+xml"" href=""http://www.spanishdict.com/toolbar_search.xml""><link rel=""alternate"" hreflang=""en"" href=""http://www.spanishdict.com/conjugate/andar""><link rel=""alternate"" hreflang=""es"" href=""http://www.spanishdict.com/verbos/andar""><title>Andar Conjugation | Conjugate Andar in Spanish</title><meta name=""description"" content=""Conjugate Andar in every Spanish verb tense including preterite, imperfect, future, conditional, and subjunctive.""><meta property=""fb:app_id"" content=""123097127645""><meta property=""fb:admins"" content=""1675823057""><meta property=""og:type"" content=""article""><meta property=""og:site_name"" content=""SpanishDict""><meta property=""og:title"" content=""Check out the conjugation for &quot;andar&quot; on SpanishDict!""><meta property=""og:description"" content=""Find out why SpanishDict is the web's most popular, free Spanish translation, dictionary, and conjugation site.""><meta property=""og:image"" content=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-152x152-precomposed.png""><meta property=""og:url"" content=""http://www.spanishdict.com/conjugate/andar?utm_source=social&amp;utm_medium=facebook&amp;utm_campaign=share""><meta name=""apple-itunes-app"" content=""app-id=332510494, affiliate-data=at=1010l3IG&amp;ct=Smart%20Banner&amp;pt=300896, app-argument=http://www.spanishdict.com/conjugate/andar""><meta name=""google-play-app"" content=""app-id=com.spanishdict.spanishdict&amp;referer=utm_source%3Dsmart-banner""><script type=""application/ld+json"">{
  ""@context"": ""http://schema.org"",
  ""@type"": ""WebSite"",
  ""url"": ""http://www.spanishdict.com/"",
  ""potentialAction"": {
    ""@type"": ""SearchAction"",
    ""target"": ""http://www.spanishdict.com/translate/{search_term}"",
    ""query-input"": ""required name=search_term""
  }
}</script><link rel=""apple-touch-icon-precomposed"" sizes=""152x152"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-152x152-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""144x144"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-144x144-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""120x120"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-120x120-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""114x114"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-114x114-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""76x76"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-76x76-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""72x72"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-72x72-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""57x57"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-57x57-precomposed.png""><link rel=""apple-touch-icon-precomposed"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-precomposed.png""><script>(function(global) {
  global.SD_ENV=""production"";
  global.SD_LOG_LEVEL=""warning"";
  global.SD_LANG=""en"";
  global.SD_GA_UA=""UA-329211-2"";
  global.SD_MP=""0d9d3502c20e893b403e6018cccf0388"";
})(this);</script><link rel=""dns-prefetch"" href=""http://www.google-analytics.com""><link rel=""dns-prefetch"" href=""http://api.mixpanel.com""><link rel=""dns-prefetch"" href=""http://cdn.mxpnl.com""><link rel=""dns-prefetch"" href=""http://suggest1.spanishdict.com""><link rel=""dns-prefetch"" href=""http://translate1.spanishdict.com""><meta name=""viewport"" content=""width=1024""><link rel=""canonical"" href=""http://www.spanishdict.com/conjugate/andar""><script>(function(o){var K=o.$LAB,y=""UseLocalXHR"",z=""AlwaysPreserveOrder"",u=""AllowDuplicates"",A=""CacheBust"",B=""BasePath"",C=/^[^?#]*\//.exec(location.href)[0],D=/^\w+\:\/\/\/?[^\/]+/.exec(C)[0],i=document.head||document.getElementsByTagName(""head""),L=(o.opera&&Object.prototype.toString.call(o.opera)==""[object Opera]"")||(""MozAppearance""in document.documentElement.style),q=document.createElement(""script""),E=typeof q.preload==""boolean"",r=E||(q.readyState&&q.readyState==""uninitialized""),F=!r&&q.async===true,M=!r&&!F&&!L;function G(a){return Object.prototype.toString.call(a)==""[object Function]""}function H(a){return Object.prototype.toString.call(a)==""[object Array]""}function N(a,c){var b=/^\w+\:\/\//;if(/^\/\/\/?/.test(a)){a=location.protocol+a}else if(!b.test(a)&&a.charAt(0)!=""/""){a=(c||"""")+a}return b.test(a)?a:((a.charAt(0)==""/""?D:C)+a)}function s(a,c){for(var b in a){if(a.hasOwnProperty(b)){c[b]=a[b]}}return c}function O(a){var c=false;for(var b=0;b<a.scripts.length;b++){if(a.scripts[b].ready&&a.scripts[b].exec_trigger){c=true;a.scripts[b].exec_trigger();a.scripts[b].exec_trigger=null}}return c}function t(a,c,b,d){a.onload=a.onreadystatechange=function(){if((a.readyState&&a.readyState!=""complete""&&a.readyState!=""loaded"")||c[b])return;a.onload=a.onreadystatechange=null;d()}}function I(a){a.ready=a.finished=true;for(var c=0;c<a.finished_listeners.length;c++){a.finished_listeners[c]()}a.ready_listeners=[];a.finished_listeners=[]}function P(d,f,e,g,h){setTimeout(function(){var a,c=f.real_src,b;if(""item""in i){if(!i[0]){setTimeout(arguments.callee,25);return}i=i[0]}a=document.createElement(""script"");if(f.type)a.type=f.type;if(f.charset)a.charset=f.charset;if(h){if(r){e.elem=a;if(E){a.preload=true;a.onpreload=g}else{a.onreadystatechange=function(){if(a.readyState==""loaded"")g()}}a.src=c}else if(h&&c.indexOf(D)==0&&d[y]){b=new XMLHttpRequest();b.onreadystatechange=function(){if(b.readyState==4){b.onreadystatechange=function(){};e.text=b.responseText+""\n//@ sourceURL=""+c;g()}};b.open(""GET"",c);b.send()}else{a.type=""text/cache-script"";t(a,e,""ready"",function(){i.removeChild(a);g()});a.src=c;i.insertBefore(a,i.firstChild)}}else if(F){a.async=false;t(a,e,""finished"",g);a.src=c;i.insertBefore(a,i.firstChild)}else{t(a,e,""finished"",g);a.src=c;i.insertBefore(a,i.firstChild)}},0)}function J(){var l={},Q=r||M,n=[],p={},m;l[y]=true;l[z]=false;l[u]=false;l[A]=false;l[B]="""";function R(a,c,b){var d;function f(){if(d!=null){d=null;I(b)}}if(p[c.src].finished)return;if(!a[u])p[c.src].finished=true;d=b.elem||document.createElement(""script"");if(c.type)d.type=c.type;if(c.charset)d.charset=c.charset;t(d,b,""finished"",f);if(b.elem){b.elem=null}else if(b.text){d.onload=d.onreadystatechange=null;d.text=b.text}else{d.src=c.real_src}i.insertBefore(d,i.firstChild);if(b.text){f()}}function S(c,b,d,f){var e,g,h=function(){b.ready_cb(b,function(){R(c,b,e)})},j=function(){b.finished_cb(b,d)};b.src=N(b.src,c[B]);b.real_src=b.src+(c[A]?((/\?.*$/.test(b.src)?""&_"":""?_"")+~~(Math.random()*1E9)+""=""):"""");if(!p[b.src])p[b.src]={items:[],finished:false};g=p[b.src].items;if(c[u]||g.length==0){e=g[g.length]={ready:false,finished:false,ready_listeners:[h],finished_listeners:[j]};P(c,b,e,((f)?function(){e.ready=true;for(var a=0;a<e.ready_listeners.length;a++){e.ready_listeners[a]()}e.ready_listeners=[]}:function(){I(e)}),f)}else{e=g[0];if(e.finished){j()}else{e.finished_listeners.push(j)}}}function v(){var e,g=s(l,{}),h=[],j=0,w=false,k;function T(a,c){a.ready=true;a.exec_trigger=c;x()}function U(a,c){a.ready=a.finished=true;a.exec_trigger=null;for(var b=0;b<c.scripts.length;b++){if(!c.scripts[b].finished)return}c.finished=true;x()}function x(){while(j<h.length){if(G(h[j])){try{h[j++]()}catch(err){}continue}else if(!h[j].finished){if(O(h[j]))continue;break}j++}if(j==h.length){w=false;k=false}}function V(){if(!k||!k.scripts){h.push(k={scripts:[],finished:true})}}e={script:function(){for(var f=0;f<arguments.length;f++){(function(a,c){var b;if(!H(a)){c=[a]}for(var d=0;d<c.length;d++){V();a=c[d];if(G(a))a=a();if(!a)continue;if(H(a)){b=[].slice.call(a);b.unshift(d,1);[].splice.apply(c,b);d--;continue}if(typeof a==""string"")a={src:a};a=s(a,{ready:false,ready_cb:T,finished:false,finished_cb:U});k.finished=false;k.scripts.push(a);S(g,a,k,(Q&&w));w=true;if(g[z])e.wait()}})(arguments[f],arguments[f])}return e},wait:function(){if(arguments.length>0){for(var a=0;a<arguments.length;a++){h.push(arguments[a])}k=h[h.length-1]}else k=false;x();return e}};return{script:e.script,wait:e.wait,setOptions:function(a){s(a,g);return e}}}m={setGlobalDefaults:function(a){s(a,l);return m},setOptions:function(){return v().setOptions.apply(null,arguments)},script:function(){return v().script.apply(null,arguments)},wait:function(){return v().wait.apply(null,arguments)},queueScript:function(){n[n.length]={type:""script"",args:[].slice.call(arguments)};return m},queueWait:function(){n[n.length]={type:""wait"",args:[].slice.call(arguments)};return m},runQueue:function(){var a=m,c=n.length,b=c,d;for(;--b>=0;){d=n.shift();a=a[d.type].apply(null,d.args)}return a},noConflict:function(){o.$LAB=K;return m},sandbox:function(){return J()}};return m}o.$LAB=J();(function(a,c,b){if(document.readyState==null&&document[a]){document.readyState=""loading"";document[a](c,b=function(){document.removeEventListener(c,b,false);document.readyState=""complete""},false)}})(""addEventListener"",""DOMContentLoaded"")})(this);</script><script>var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];

googletag.cmd.push(function() {
  googletag.pubads().setTargeting('sd_is_mobile', 'false');
  googletag.pubads().setTargeting('test_group', '99');
  if("""" !== """"){
    googletag.pubads().setTargeting('sd_adtest', '');
  }
  googletag.pubads().disableInitialLoad();
});

//- Index.
var indexUrl = ""http://js.indexww.com/ht/curiosity.js"";

//- Set up sonobi queue for async loading.
var sbi_morpheus = sbi_morpheus || {registration: {}};
sbi_morpheus.cmd = sbi_morpheus.cmd || [];
var sonobiUrl = ""http://mtrx.go.sonobi.com/morpheus.spanishdict.js"";

//- Quirk of openx implementation; different script for mobile than desktop.
var openXUrl = ""//ox-d.spanishdict.servedbyopenx.com/w/1.0/jstag?nc=1027916-SpanishDict"";
if(""false"" === ""true"") {
  openXUrl += ""-Mob"";
}

//- Criteo.
var criteoUrl = 'http://n1.sdcdns.com/js/vendor/criteo-ads.js';

//- Amazon.
var amznUrl = '//c.amazon-adsystem.com/aax2/amzn_ads.js';

//- Sovrn.
var sovrnUrl = 'http://n1.sdcdns.com/js/sovrn.js';
var sovrnAds = sovrnAds || {
  ""id"": ""1000"",
  ""imp"": [
  ],
  ""site"": {
      ""domain"": ""spanishdict.com"",
      ""page"": window.location.pathname
  }
};
var sovrnAuctionData = sovrnAuctionData || {};

//- Yieldbot.
var yieldbotUrl = '//cdn.yldbt.com/js/yieldbot.intent.js';
var yieldbotAds = [];

$LAB
  .script(indexUrl, sonobiUrl, openXUrl, amznUrl, criteoUrl, sovrnUrl, yieldbotUrl)
  .wait(function () {
  
    //- Yieldbot post load setup.
    var yieldbotPubId= false ? ""68cc"" : ""9146"";
    
    yieldbot.pub(yieldbotPubId);
    for (var i = 0; i < yieldbotAds.length; i++) {
      var adUnit = yieldbotAds[i];
      yieldbot.defineSlot(adUnit.name, { sizes: [
        [adUnit.width, adUnit.height]
      ]});
    }
    yieldbot.enableAsync();
    yieldbot.go();
    
    googletag.cmd.push(function() {
      var criteria = yieldbot.getPageCriteria().split(',');
      for(var i = 0; i < criteria.length; i++){
        var yieldbotKey = 'ybot_' + criteria[i].split(':')[0];
        var yieldbotVal = criteria[i].split(':')[2];
        yieldbotVal = (50 * Math.round(yieldbotVal / 50) / 100).toFixed(2);
        googletag.pubads().setTargeting(yieldbotKey, yieldbotVal);
      }
    });
    
    //- Google script must be loaded by dynamic insertion because it
    //- includes document.write calls which will not work in async scripts.
    var gads = document.createElement(""script"");
    gads.async = true;
    gads.type = ""text/javascript"";
    var useSSL = ""https:"" == document.location.protocol;
    gads.src = (useSSL ? ""https:"" : ""http:"") + ""//www.googletagservices.com/tag/js/gpt.js"";
    var node = document.getElementsByTagName(""script"")[0];
    
    //- Arbitrary timeout to give more time for header bidders to return
    //- bids before we load gpt.js.
    var timeoutLength = 500;
    
    setTimeout(function(){
      node.parentNode.insertBefore(gads, node);
    }, timeoutLength);
    
    googletag.cmd.push(function() {
      //- Calls googletag.pubads().refresh().
      sbi_morpheus.callOperator();
    });
    
    //- Amazon post load setup.
    try {
      amznads.getAdsCallback('3286', function(){
        amznads.setTargetingForGPTAsync('amznslots');
      });
    } catch (e) { /* Ignore. */ }
    
  });
  </script><script>var adDimensions = [728, 90];
if(""adTop"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_728_90_HEADER"", adDimensions, ""adTop"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adTop""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adTop"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adTop"",
  ""banner"": {
    ""w"": ""728"",
    ""h"": ""90""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adTop""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adTop""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adTop""],
    width: 728,
    height: 90
  });
};</script><script>var adDimensions = [728, 90];
if(""adBot"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_728_90_FOOTER"", adDimensions, ""adBot"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adBot""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adBot"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adBot"",
  ""banner"": {
    ""w"": ""728"",
    ""h"": ""90""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adBot""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adBot""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adBot""],
    width: 728,
    height: 90
  });
};</script><script>var adDimensions = [300, 250];
if(""adSide1"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_300_250_SIDEBAR"", adDimensions, ""adSide1"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adSide1""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adSide1"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adSide1"",
  ""banner"": {
    ""w"": ""300"",
    ""h"": ""250""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adSide1""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adSide1""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adSide1""],
    width: 300,
    height: 250
  });
};</script><script>var adDimensions = [300, 250];
if(""adSide2"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_300_250_SIDEBAR_2"", adDimensions, ""adSide2"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adSide2""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adSide2"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adSide2"",
  ""banner"": {
    ""w"": ""300"",
    ""h"": ""250""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adSide2""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adSide2""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adSide2""],
    width: 300,
    height: 250
  });
};</script><script>var adDimensions = [160, 600];
if(""adSideLong"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_160_600_SIDEBAR"", adDimensions, ""adSideLong"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adSideLong""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adSideLong"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adSideLong"",
  ""banner"": {
    ""w"": ""160"",
    ""h"": ""600""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adSideLong""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adSideLong""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adSideLong""],
    width: 160,
    height: 600
  });
};</script><script>var adDimensions = [300, 250];
if(""adContent"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_300_250_CONTENT"", adDimensions, ""adContent"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adContent""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adContent"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adContent"",
  ""banner"": {
    ""w"": ""300"",
    ""h"": ""250""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adContent""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adContent""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adContent""],
    width: 300,
    height: 250
  });
};</script><script>googletag.cmd.push(function() {
  googletag.pubads().enableSingleRequest();
  googletag.enableServices();
});</script><script>(function(global) {
  global.SD_MEGASUGGEST=""false"" === 'true' ? true : false;
  global.SD_EXAMPLES=true;
})(this);</script><link rel=""stylesheet"" href=""http://n1.sdcdns.com/dist/desktop-27c13aa86b1864d16cb9909542b5f38b.gz.css""><!--[if lte IE 8]><link rel=""stylesheet"" type=""text/css"" href=""http://n1.sdcdns.com/dist/desktop-ie8-f7fed1e39cc45ed918639c01178e6dc6.gz.css""><![endif]--></head><body data-datetime=""2015-10-15T07:44:06"" data-is-mobile=""false"" data-asset-host=""http://n1.sdcdns.com"" class=""lang-en js-transition-off-during-page-load""><div class=""header-wrapper""><div class=""header header-container container""><div class=""menu-bar""><div class=""menu-nav menu-collapsed""><div class=""menu-container""><div class=""menu-selector""><div class=""icon-menu-nav-selector""></div><div class=""menu-selector-text"">Menu</div><div class=""menu-collapse""></div></div><div class=""menu""><a href=""http://www.spanishdict.com/""><div class=""menu-item"">Home</div></a><a href=""http://www.spanishdict.com/blog""><div class=""menu-item"">Blog</div></a><a href=""http://www.spanishdict.com/translation""><div class=""menu-item"">Translation</div></a><a href=""http://www.spanishdict.com/conjugation""><div class=""menu-item"">Conjugation</div></a><a href=""http://www.spanishdict.com/learn""><div class=""menu-item"">Learn Spanish</div></a><a href=""http://www.spanishdict.com/flashcards""><div class=""menu-item"">Flashcards</div></a><a href=""http://www.spanishdict.com/grammar""><div class=""menu-item"">Grammar</div></a><a href=""http://www.spanishdict.com/answers""><div class=""menu-item"">Q &amp; A</div></a><a href=""http://www.spanishdict.com/wordoftheday""><div class=""menu-item"">Word of the Day</div></a><a href=""#""><div class=""menu-item menu-divider go-language"">Ver en español</div></a><div class=""menu-fluencia menu-divider""><div class=""menu-fluencia-title"">Want to learn Spanish?</div><div class=""menu-fluencia-text""><span>Try Fluencia, an amazing learning program from SpanishDict.</span></div><a href=""http://www.fluencia.com/learn-spanish/?utm_source=sd_desktop&amp;utm_medium=nav_menu&amp;h=1"" target=""_blank"" data-sd-track-click-type=""fluencia"" class=""menu-fluencia-link sd-track-click""><div class=""menu-fluencia-button"">Yes!</div></a></div></div></div></div><div class=""menu-user menu-collapsed""><div class=""menu-container""><div class=""menu-selector unauthed""><a href=""http://www.spanishdict.com/users/login"" class=""menu-selector-text"">Sign in</a></div></div></div><a href=""http://www.spanishdict.com/"" class=""header-logo""></a></div></div></div><div class=""content-container container""><div class=""ad-wrapper-wide""><div class=""ad-728-90 adTop-results ad-wrapper""><div id=""adTop"" class=""adTop""><script>googletag.cmd.push(function() {
  googletag.display(""adTop"");
});</script></div><div data-target=""adTop"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div></div><div class=""main-container""><div class=""search""><div class=""search-component search-form""><form action=""/conjugation"" method=""post"" accept-charset=""utf-8"" data-action=""conjugation"" class=""page-action search_query_to_url save-history""><span id=""hidden-search""></span><div class=""search-form-parts search-collapse""><input id=""hidden-input"" name=""isExpand"" type=""hidden"" value=""false""><textarea name=""text"" id=""query"" type=""search"" data-suggest-host=""suggest1.spanishdict.com:80"" data-suggest-path=""/dictionary/conjugate_auto_suggest"" data-suggest-variation=""original"" placeholder=""Enter a Spanish verb&hellip;"" autocomplete=""off"" autocorrect=""off"" autocapitalize=""off"" class=""search-form-input"">andar</textarea><div class=""accents""><span data-toggle=""tooltip"" title=""Add á to your text"" class=""accents-letter"">á</span><span data-toggle=""tooltip"" title=""Add é to your text"" class=""accents-letter"">é</span><span data-toggle=""tooltip"" title=""Add í to your text"" class=""accents-letter"">í</span><span data-toggle=""tooltip"" title=""Add ó to your text"" class=""accents-letter"">ó</span><span data-toggle=""tooltip"" title=""Add ú to your text"" class=""accents-letter"">ú</span><span data-toggle=""tooltip"" title=""Add ñ to your text"" class=""accents-letter"">ñ</span><span data-toggle=""tooltip"" title=""Add ¿ to your text"" class=""accents-letter accents-descender"">¿</span><span data-toggle=""tooltip"" title=""Add ¡ to your text"" class=""accents-letter accents-descender"">¡</span><span data-toggle=""tooltip"" title=""Hear audio of the words"" class=""accents-audio-btn"">&nbsp;</span></div><div data-toggle=""tooltip"" title=""Show the accent buttons and expand the search box."" class=""search-expand-icon""></div><div data-toggle=""tooltip"" title=""Collapse the search box"" class=""search-collapse-icon""></div><button type=""submit"" class=""search-form-button"">Conjugate</button></div></form></div></div><div class=""conjugation card""><div class=""quickdef""><div class=""source""><h1 class=""source-text"">andar</h1><a href=""http://audio1.spanishdict.com/audio?lang=es&amp;text=andar"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start""><span class=""audio-blue"">&nbsp;</span></a><a href=""http://www.spanishdict.com/videos/andar""><span data-toggle=""tooltip"" title=""Watch a video translation"" class=""video-blue"">&nbsp;</span></a></div><div class=""lang""><div class=""el""><a href=""http://www.spanishdict.com/translate/to"">to</a> <a href=""http://www.spanishdict.com/translate/walk"">walk</a>, <a href=""http://www.spanishdict.com/translate/to"">to</a> <a href=""http://www.spanishdict.com/translate/take"">take</a></div><span class=""media-links""><a href=""http://audio1.spanishdict.com/audio?lang=en&amp;text=to-walk%2C-to-take"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start""><span class=""audio-blue"">&nbsp;</span></a></span></div></div><div class=""nav-content""><div class=""nav-content-item""><a href=""http://www.spanishdict.com/translate/andar""><span class=""nav-content-item-text"">Dictionary</span></a></div><div class=""nav-content-item nav-active""><a><span class=""nav-content-item-text"">Conjugation</span></a></div><div class=""nav-content-item""><a href=""http://www.spanishdict.com/examples/andar""><span class=""nav-content-item-text"">Examples</span></a><span class=""nav-content-item-label"">new</span></div><div class=""nav-content-item""><a href=""http://www.spanishdict.com/videos/andar""><span class=""nav-content-item-text"">Video</span></a></div><div class=""social-small""><a target=""_blank"" href=""https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.spanishdict.com%2Ftranslate%2Fandar%3Futm_source%3Dsocial%26utm_medium%3Dfacebook%26utm_campaign%3Dshare"" class=""social-small-icon social-small-facebook""></a><a target=""_blank"" href=""https://twitter.com/intent/tweet?text=Check%20out%20the%20translation%20for%20%22andar%22%20on%20%40SpanishDict!&amp;amp;url=http%3A%2F%2Fspanishdict.com%2ftranslate%2Fandar%3Futm_source%3Dsocial%26utm_medium%3Dtwitter%26utm_campaign%3Dshare"" class=""social-small-icon social-small-twitter""></a><a target=""_blank"" href=""https://plus.google.com/share?url=http%3A%2F%2Fspanishdict.com%2Ftranslate%2Fandar%3Futm_source%3Dsocial%26utm_medium%3Dgoogleplus%26utm_campaign%3Dshare"" class=""social-small-icon social-small-gplus""></a></div></div><div class=""conj-row""><a href=""http://www.spanishdict.com/answers/100043/gerund-form"" target=""_blank"" class=""conj-basic-label"">Gerund:</a><span>&nbspandando</span></div><div class=""conj-row""><a href=""http://www.spanishdict.com/answers/100044/past-participles"" target=""_blank"" class=""conj-basic-label"">Participle:</a><span>&nbspandado</span></div><div class=""conj-row""><span>The irregular conjugations of this verb are in</span><span>&nbsp;</span><span class=""conj-irregular"">red</span><span>&#46;</span></div><a href=""http://www.spanishdict.com/topics/show/112"" class=""vtable-label"">Indicative<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100045/present-tense"" title=""Tense example: *He speaks* Spanish."" data-toggle=""tooltip"" class=""vtable-title-link"">Present<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100046/preterite-simple-past"" title=""Tense example: *He spoke* to my sister yesterday."" data-toggle=""tooltip"" class=""vtable-title-link"">Preterite<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100048/imperfect-past"" title=""Tense example: *He used to speak* Italian when he was a child."" data-toggle=""tooltip"" class=""vtable-title-link"">Imperfect<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100050/conditional"" title=""Tense example: *He would speak* French in France."" data-toggle=""tooltip"" class=""vtable-title-link"">Conditional<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100049/future"" title=""Tense example: *He will speak* Portuguese on his vacation next week."" data-toggle=""tooltip"" class=""vtable-title-link"">Future<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">ando</td><td class=""vtable-word"">and<span class='conj-irregular'>uve</span></td><td class=""vtable-word  "">andaba</td><td class=""vtable-word  "">andaría</td><td class=""vtable-word  "">andaré</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">andas</td><td class=""vtable-word"">and<span class='conj-irregular'>uvi</span>ste</td><td class=""vtable-word  "">andabas</td><td class=""vtable-word  "">andarías</td><td class=""vtable-word  "">andarás</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">anda</td><td class=""vtable-word"">and<span class='conj-irregular'>uvo</span></td><td class=""vtable-word  "">andaba</td><td class=""vtable-word  "">andaría</td><td class=""vtable-word  "">andará</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">andamos</td><td class=""vtable-word"">and<span class='conj-irregular'>uvi</span>mos</td><td class=""vtable-word  "">andábamos</td><td class=""vtable-word  "">andaríamos</td><td class=""vtable-word  "">andaremos</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">andáis</td><td class=""vtable-word"">and<span class='conj-irregular'>uvi</span>steis</td><td class=""vtable-word  "">andabais</td><td class=""vtable-word  "">andaríais</td><td class=""vtable-word  "">andaréis</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">andan</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>ron</td><td class=""vtable-word  "">andaban</td><td class=""vtable-word  "">andarían</td><td class=""vtable-word  "">andarán</td></tr></table></div><a href=""http://www.spanishdict.com/topics/show/68"" class=""vtable-label"">Subjunctive<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100057/present-subjunctive"" title=""Tense example: It is good that *he speak* to your mother."" data-toggle=""tooltip"" class=""vtable-title-link"">Present<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100058/imperfect-subjunctive"" title=""Tense example: It made me happy that *he spoke* at the wedding."" data-toggle=""tooltip"" class=""vtable-title-link"">Imperfect<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100058/imperfect-subjunctive"" title=""Tense example: It made me happy that *he spoke* at the wedding."" data-toggle=""tooltip"" class=""vtable-title-link"">Imperfect 2<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100059/future-subjunctive"" title=""Tense example: It is possible that *he will speak* to your father."" data-toggle=""tooltip"" class=""vtable-title-link"">Future<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">ande</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>ra</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>se</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>re</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">andes</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>ras</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>ses</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>res</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">ande</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>ra</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>se</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>re</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">andemos</td><td class=""vtable-word"">and<span class='conj-irregular'>uvié</span>ramos</td><td class=""vtable-word"">and<span class='conj-irregular'>uvié</span>semos</td><td class=""vtable-word"">and<span class='conj-irregular'>uvié</span>remos</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">andéis</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>rais</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>seis</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>reis</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">anden</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>ran</td><td class=""vtable-word"">and<span class='conj-irregular'>uvie</span>sen</td><td class=""vtable-word"">and<span class='conj-irregular'>uviere</span>n</td></tr></table></div><a href=""http://www.spanishdict.com/topics/show/113"" class=""vtable-label"">Imperative<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100063/imperative"" title=""Tense example: *Speak!*."" data-toggle=""tooltip"" class=""vtable-title-link"">Imperative<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">-</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">anda</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">ande</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">andemos</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">andad</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">anden</td></tr></table></div><a href=""http://www.spanishdict.com/topics/show/35"" class=""vtable-label"">Perfect<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100051/present-perfect"" title=""Tense example: *He has spoken* at several meetings."" data-toggle=""tooltip"" class=""vtable-title-link"">Present<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/173873/preterit-perfect"" title=""Tense example: *He had (already) spoken* to her when I got there."" data-toggle=""tooltip"" class=""vtable-title-link"">Preterite<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100052/past-perfect"" title=""Tense example: *He had spoken* a lot before the train left."" data-toggle=""tooltip"" class=""vtable-title-link"">Past<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100054/conditional-perfect"" title=""Tense example: *He would have spoken*, but he was sick."" data-toggle=""tooltip"" class=""vtable-title-link"">Conditional<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100053/future-perfect"" title=""Tense example: *He will have spoken* in three different countries by tomorrow."" data-toggle=""tooltip"" class=""vtable-title-link"">Future<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">he andado</td><td class=""vtable-word  "">hube andado</td><td class=""vtable-word  "">había andado</td><td class=""vtable-word  "">habría andado</td><td class=""vtable-word  "">habré andado</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">has andado</td><td class=""vtable-word  "">hubiste andado</td><td class=""vtable-word  "">habías andado</td><td class=""vtable-word  "">habrías andado</td><td class=""vtable-word  "">habrás andado</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">ha andado</td><td class=""vtable-word  "">hubo andado</td><td class=""vtable-word  "">había andado</td><td class=""vtable-word  "">habría andado</td><td class=""vtable-word  "">habrá andado</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">hemos andado</td><td class=""vtable-word  "">hubimos andado</td><td class=""vtable-word  "">habíamos andado</td><td class=""vtable-word  "">habríamos andado</td><td class=""vtable-word  "">habremos andado</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">habéis andado</td><td class=""vtable-word  "">hubisteis andado</td><td class=""vtable-word  "">habíais andado</td><td class=""vtable-word  "">habríais andado</td><td class=""vtable-word  "">habréis andado</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">han andado</td><td class=""vtable-word  "">hubieron andado</td><td class=""vtable-word  "">habían andado</td><td class=""vtable-word  "">habrían andado</td><td class=""vtable-word  "">habrán andado</td></tr></table></div><a href=""http://www.spanishdict.com/topics/show/102"" class=""vtable-label"">Perfect Subjunctive<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100060/present-perfect-subjunctive"" title=""Tense example: I doubt that *he has spoken* to the president."" data-toggle=""tooltip"" class=""vtable-title-link"">Present<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100061/past-perfect-subjunctive"" title=""Tense example: She did not believe that *he had spoken* to a unicorn."" data-toggle=""tooltip"" class=""vtable-title-link"">Past<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100062/future-perfect-subjunctive"" title=""Tense example: It is possible that *he will have spoken* to the whole family."" data-toggle=""tooltip"" class=""vtable-title-link"">Future<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">haya andado</td><td class=""vtable-word  "">hubiera andado</td><td class=""vtable-word  "">hubiere andado</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">hayas andado</td><td class=""vtable-word  "">hubieras andado</td><td class=""vtable-word  "">hubieres andado</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">haya andado</td><td class=""vtable-word  "">hubiera andado</td><td class=""vtable-word  "">hubiere andado</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">hayamos andado</td><td class=""vtable-word  "">hubiéramos andado</td><td class=""vtable-word  "">hubiéremos andado</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">hayáis andado</td><td class=""vtable-word  "">hubierais andado</td><td class=""vtable-word  "">hubiereis andado</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">hayan andado</td><td class=""vtable-word  "">hubieran andado</td><td class=""vtable-word  "">hubieren andado</td></tr></table></div><div class=""conj-footnote"">This can be reflexive depending on the meaning.</div><div class=""conj-footnote"">This verb is recognized by the Real Academia Española.</div><div class=""conj-footnote"">This data is provided by Onoma.</div></div><div class=""card""><div class=""card-title"">Search history</div><div class=""search-history-items""></div></div><div class=""ad-300-250 adContent-results ad-wrapper""><div id=""adContent"" class=""adContent""><script>googletag.cmd.push(function() {
  googletag.display(""adContent"");
});</script></div><div data-target=""adContent"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div><div id=""feedback-query"" class=""card""><div class=""card-title"">Did this page answer your question?</div><form><div class=""feedback-1""><a href=""#"" class=""feedback-yes""><div class=""icon-smile""></div><span>Yes</span></a><a href=""#"" class=""feedback-no""><div class=""icon-frown""></div><span>No</span></a></div><div class=""feedback-2""><div id=""feedback-categories""><div class=""feedback-instructions"">The page is...</div><div class=""feedback-categories-select""><label><input type=""radio"" name=""category"" value=""inaccurate"">Inaccurate</label><label><input type=""radio"" name=""category"" value=""unclear"">Unclear</label><label><input type=""radio"" name=""category"" value=""missingTranslations"">Missing translations</label><label><input type=""radio"" name=""category"" value=""missingConjugations"">Missing conjugations</label><label><input type=""radio"" name=""category"" value=""other"">Other</label></div></div><div class=""feedback-instructions"">What can we do to improve?</div><textarea id=""feedback-comment"" placeholder=""Enter your comment..."" required class=""feedback-comment""></textarea><div class=""feedback-instructions"">How can we follow up with you? (Optional)</div><input id=""feedback-query-email"" type=""email"" placeholder=""Enter your email"" class=""feedback-query-email""><button type=""submit"" class=""feedback-2-submit"">Submit</button></div><div class=""feedback-3""><span>Your feedback was sent. We appreciate your comments!</span></div></form></div></div><div class=""sidebar-container""><div class=""fluencia-sidebar card""><div class=""card-title"">Want to speak Spanish?</div><div class=""fluencia-sidebar-text"">Try Fluencia, an amazing learning program from SpanishDict.</div><a href=""http://www.fluencia.com/learn-spanish/?utm_source=sd_desktop&amp;utm_medium=sidebar&amp;h=2"" target=""_blank"" data-sd-track-click-type=""fluencia"" class=""fluencia-sidebar-button sd-track-click"">Join now</a></div><div class=""ad-300-250 adSide-results sidebar-box ad-wrapper""><div id=""adSide1"" class=""adSide1""><script>googletag.cmd.push(function() {
  googletag.display(""adSide1"");
});</script></div><div data-target=""adSide1"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div><div class=""wotd-sidebar card""><div class=""card-title"">Word of the Day</div><div class=""wotd-sidebar-date"">Oct 15</div><a href=""http://www.spanishdict.com/wordoftheday/2544/agregar"" class=""wotd-sidebar-word"">agregar</a><a href=""http://audio1.spanishdict.com/audio?lang=es&amp;text=agregar"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start""><span class=""audio-blue"">&nbsp;</span></a><div class=""wotd-sidebar-translation"">to add; to gather</div><div class=""wotd-sidebar-prompt"">Get the free word of the day!</div><form action=""/users/signup_email_verify"" method=""post"" class=""wotd-sidebar-form""><input name=""email"" type=""email"" placeholder=""email"" class=""wotd-sidebar-input""><button type=""submit"" data-sd-track-click-type=""wotd"" class=""wotd-sidebar-button sd-track-click"">Subscribe</button></form></div><div class=""ad-300-250 adSide-results sidebar-box ad-wrapper""><div id=""adSide2"" class=""adSide2""><script>googletag.cmd.push(function() {
  googletag.display(""adSide2"");
});</script></div><div data-target=""adSide2"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div><div class=""social-sidebar card""><div class=""card-title"">Stay Connected</div><a href=""https://www.facebook.com/pages/SpanishDict/92805940179"" target=""_blank"" data-sd-track-click-type=""social"" data-sd-track-click-text=""facebook"" class=""social-sidebar-button facebook sd-track-click""><div class=""social-logo social-facebook-logo""></div></a><a href=""https://plus.google.com/106413713006018165072/posts"" target=""_blank"" data-sd-track-click-type=""social"" data-sd-track-click-text=""google"" class=""social-sidebar-button gplus sd-track-click""><div class=""social-logo social-gplus-logo""></div></a><a href=""https://twitter.com/spanishdict"" target=""_blank"" data-sd-track-click-type=""social"" data-sd-track-click-text=""twitter"" class=""social-sidebar-button twitter sd-track-click""><div class=""social-logo social-twitter-logo""></div></a></div><div class=""ad-160-600 adSide-long-results sidebar-box-long ad-wrapper""><div id=""adSideLong"" class=""adSideLong""><script>googletag.cmd.push(function() {
  googletag.display(""adSideLong"");
});</script></div><div data-target=""adSideLong"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div></div><div class=""ad-wrapper-wide""><div class=""ad-728-90 adBot-results ad-wrapper""><div id=""adBot"" class=""adBot""><script>googletag.cmd.push(function() {
  googletag.display(""adBot"");
});</script></div><div data-target=""adBot"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div></div></div><div class=""footer-wrapper""><div class=""footer footer-container container""><div class=""footer-row""><div class=""footer-item""><h4>Stay Connected</h4><div class=""footer-social""><div class=""social""><a href=""http://www.facebook.com/pages/SpanishDict/92805940179""><div class=""social-icon social-facebook""></div></a><a href=""http://www.twitter.com/spanishdict""><div class=""social-icon social-twitter""></div></a><a href=""https://plus.google.com/106413713006018165072/posts""><div class=""social-icon social-gplus""></div></a></div></div></div><div class=""footer-item""><h4>iPhone</h4><a href=""http://itunes.apple.com/us/app/spanishdict/id332510494""><div class=""footer-external footer-external-appstore""></div></a></div><div class=""footer-item""><h4>Android</h4><a href=""https://play.google.com/store/apps/details?id=com.spanishdict.spanishdict&amp;referrer=utm_source%3Dsd-footer""><div class=""footer-external footer-external-google-play""></div></a></div><div class=""footer-item footer-item-last""><h4>Kindle</h4><a href=""http://www.amazon.com/SpanishDict-Spanish-English-Dictionary-and-More/dp/B004UO8ZNQ""><div class=""footer-external footer-external-amazon""></div></a></div></div><div class=""footer-row""><div class=""footer-item""><h4>Spanish Reference</h4><ul><li><a href=""http://www.spanishdict.com/translation"">English to Spanish translation</a></li><li><a href=""http://www.spanishdict.com/translation"">Spanish to English translation</a></li><li><a href=""http://www.spanishdict.com/dictionary"">English to Spanish dictionary</a></li><li><a href=""http://www.spanishdict.com/dictionary"">Spanish to English dictionary</a></li></ul></div><div class=""footer-item""><h4>Learn Spanish</h4><ul><li><a href=""http://www.spanishdict.com/learn"">Learn Spanish</a></li><li><a href=""http://www.spanishdict.com/flashcards"">Spanish Flashcards</a></li><li><a href=""http://www.spanishdict.com/conjugation"">Spanish Verb Conjugation</a></li><li><a href=""http://www.spanishdict.com/answers"">Spanish-English Q&amp;A</a></li></ul></div><div class=""footer-item""><h4>Popular Translations</h4><ul><li><a href=""http://www.spanishdict.com/translate/happy"">happy in Spanish</a></li><li><a href=""http://www.spanishdict.com/translate/I love you"">I love you in Spanish</a></li><li><a href=""http://www.spanishdict.com/translate/yes"">yes in Spanish</a></li><li><a href=""http://www.spanishdict.com/translate/I miss you"">I miss you in Spanish</a></li></ul></div><div class=""footer-item footer-item-last""><h4>Popular Conjugations</h4><ul><li><a href=""http://www.spanishdict.com/conjugate/llegar"">llegar conjugation</a></li><li><a href=""http://www.spanishdict.com/conjugate/haber"">haber conjugation</a></li><li><a href=""http://www.spanishdict.com/conjugate/pasar"">pasar conjugation</a></li><li><a href=""http://www.spanishdict.com/conjugate/pedir"">pedir conjugation</a></li></ul></div></div><div class=""footer-meta""><div class=""footer-meta-list""><ul><li><a href=""http://www.spanishdict.com/company/about"">About</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""http://www.fluencia.com/about-us/careers/"">We're hiring!</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""http://www.spanishdict.com/company/privacy"">Privacy</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""http://www.spanishdict.com/company/tos"">Terms</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""#"" class=""go-language"">Español</a></li></ul></div><span class=""footer-copyright"">&copy; Curiosity Media, Inc.</span><div class=""footer-exclamation-logo-wrapper""><div class=""footer-exclamation-container""><img src=""http://n1.sdcdns.com/img/desktop/logoAnimation.gif"" class=""footer-exclamation-logo""></div></div></div></div></div><div class=""feedback-tab""><button data-toggle=""modal"" data-target=""#feedback-modal"">Feedback</button></div><div id=""feedback-modal"" class=""feedback-modal sd-modal modal fade""><div class=""modal-dialog""><div class=""modal-content""><div class=""modal-body""><div class=""sd-modal-title"">Feedback</div><div class=""sd-modal-close feedback-modal-close""></div><div class=""sd-modal-message feedback-modal-message""></div><div class=""sd-modal-content feedback-modal-content""><div class=""sd-modal-desc"">SpanishDict is devoted to improving our site based on user feedback and introducing new and innovative features that will continue to help people learn and love the Spanish language.&nbsp;Have a suggestion, idea, or comment?&nbsp;Send us your feedback.</div><form><label for=""feedback-name"" class=""sd-modal-label"">Your name</label><input id=""feedback-name"" type=""text"" placeholder=""Your name""><label for=""feedback-email"" class=""sd-modal-label"">Email address</label><input id=""feedback-email"" type=""email"" placeholder=""Email address""><label for=""feedback-subject"" class=""sd-modal-label"">Subject</label><input id=""feedback-subject"" type=""text"" placeholder=""Subject"" required><label for=""feedback-message"" class=""sd-modal-label"">Message</label><textarea id=""feedback-message"" placeholder=""Message"" rows=""5"" required></textarea><input type=""submit"" value=""Send"" class=""sd-modal-submit feedback-modal-submit""></form></div></div></div></div></div><div id=""survey-modal"" class=""survey-modal modal""><div class=""modal-dialog""><div class=""modal-content""><div class=""modal-body""><div class=""survey-modal-icon""></div><div class=""survey-modal-title"">Make Your Voice Heard</div><div id=""survey-modal-close"" class=""survey-modal-close""></div><div class=""survey-modal-message""></div><div class=""survey-modal-content""><div class=""survey-modal-desc"">You've been randomly selected to participate in a short, 5 question
survey on your experience with SpanishDict. We'll use your comments
to help improve your experience.<br>Will you participate?</div><a id=""survey-modal-submit"" href=""#"" target=""_blank"" class=""survey-modal-submit"">Take 5 Question Survey</a></div></div></div></div></div><script id=""sd_analytics"" type=""application/json"">{&quot;site&quot;:&quot;desktop&quot;,&quot;q&quot;:&quot;andar&quot;,&quot;type&quot;:&quot;conjugation&quot;,&quot;is_found&quot;:1,&quot;browser_lang&quot;:&quot;en-GB&quot;}</script><script src=""http://n1.sdcdns.com/dist/vendor/jquery-min-1-11-1-8101d596b2b8fa35fe3a634ea342d7c3.gz.js"" type=""text/javascript"" crossorigin=""anonymous""></script><script src=""http://n1.sdcdns.com/dist/desktop-min-bce67d1edeeab5cafef6722f110d944a.gz.js"" type=""text/javascript"" crossorigin=""anonymous""></script><!--[if lte IE 9]><script>(function() {
  var app = document.createElement('script'); app.type = 'text/javascript'; app.async = true;
  app.src = 'http://n1.sdcdns.com/js/vendor/placeholders.min.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(app, s);
})();</script><![endif]--> <script type=""text/javascript"">
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');   ga('set', 'user_status1', 'anonymous');   ga('set', 'section2', 'conjugate');   ga('set', 'sd_is_mobile3', 'false');
   ga('require', 'displayfeatures');
   ga('create', 'UA-329211-2', 'auto');
   ga('send', 'pageview');
 </script><script>var _pnq = _pnq || [];
_pnq.push(['setId', 'pn-053b5b698be780ef']);
(function() {
  var pnr = document.createElement('script');
  pnr.type = 'text/javascript';
  pnr.async = true;
  pnr.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'report-ads-to.pubnation.com/dist/pnr.js?t=pn-053b5b698be780ef';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(pnr, s);
})();</script><script>(function() {
  if(SD_ENV != ""production"") return;
  function async_load(script_url){
    var protocol = ('https:' == document.location.protocol ? 'https://' : 'http://');
    var s = document.createElement('script'); s.src = protocol + script_url;
    var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
  }
  bm_website_code = '7BF624AB557B448F';
  jQuery(document).ready(function(){async_load('asset.pagefair.com/measure.min.js')});
  jQuery(document).ready(function(){async_load('asset.pagefair.net/ads.min.js')});
})();</script></body></html>";
                //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************
        //****************************************************************************************************************************

        public static string Page3  =
@"<!DOCTYPE html><html data-placeholder-focus=""false""><head><meta http-equiv=""Content-type"" content=""text/html; charset=utf-8""><link rel=""publisher"" href=""https://plus.google.com/106413713006018165072""><link rel=""search"" title=""SpanishDict.com"" type=""application/opensearchdescription+xml"" href=""http://www.spanishdict.com/toolbar_search.xml""><link rel=""alternate"" hreflang=""en"" href=""http://www.spanishdict.com/conjugate/añadir""><link rel=""alternate"" hreflang=""es"" href=""http://www.spanishdict.com/verbos/añadir""><title>Añadir Conjugation | Conjugate Añadir in Spanish</title><meta name=""description"" content=""Conjugate Añadir in every Spanish verb tense including preterite, imperfect, future, conditional, and subjunctive.""><meta property=""fb:app_id"" content=""123097127645""><meta property=""fb:admins"" content=""1675823057""><meta property=""og:type"" content=""article""><meta property=""og:site_name"" content=""SpanishDict""><meta property=""og:title"" content=""Check out the conjugation for &quot;añadir&quot; on SpanishDict!""><meta property=""og:description"" content=""Find out why SpanishDict is the web's most popular, free Spanish translation, dictionary, and conjugation site.""><meta property=""og:image"" content=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-152x152-precomposed.png""><meta property=""og:url"" content=""http://www.spanishdict.com/conjugate/a%C3%B1adir?utm_source=social&amp;utm_medium=facebook&amp;utm_campaign=share""><meta name=""apple-itunes-app"" content=""app-id=332510494, affiliate-data=at=1010l3IG&amp;ct=Smart%20Banner&amp;pt=300896, app-argument=http://www.spanishdict.com/conjugate/a%c3%b1adir""><meta name=""google-play-app"" content=""app-id=com.spanishdict.spanishdict&amp;referer=utm_source%3Dsmart-banner""><script type=""application/ld+json"">{
  ""@context"": ""http://schema.org"",
  ""@type"": ""WebSite"",
  ""url"": ""http://www.spanishdict.com/"",
  ""potentialAction"": {
    ""@type"": ""SearchAction"",
    ""target"": ""http://www.spanishdict.com/translate/{search_term}"",
    ""query-input"": ""required name=search_term""
  }
}</script><link rel=""apple-touch-icon-precomposed"" sizes=""152x152"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-152x152-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""144x144"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-144x144-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""120x120"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-120x120-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""114x114"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-114x114-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""76x76"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-76x76-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""72x72"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-72x72-precomposed.png""><link rel=""apple-touch-icon-precomposed"" sizes=""57x57"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-57x57-precomposed.png""><link rel=""apple-touch-icon-precomposed"" href=""http://n1.sdcdns.com/img/common/apple-touch-icons/apple-touch-icon-precomposed.png""><script>(function(global) {
  global.SD_ENV=""production"";
  global.SD_LOG_LEVEL=""warning"";
  global.SD_LANG=""en"";
  global.SD_GA_UA=""UA-329211-2"";
  global.SD_MP=""0d9d3502c20e893b403e6018cccf0388"";
})(this);</script><link rel=""dns-prefetch"" href=""http://www.google-analytics.com""><link rel=""dns-prefetch"" href=""http://api.mixpanel.com""><link rel=""dns-prefetch"" href=""http://cdn.mxpnl.com""><link rel=""dns-prefetch"" href=""http://suggest1.spanishdict.com""><link rel=""dns-prefetch"" href=""http://translate1.spanishdict.com""><meta name=""viewport"" content=""width=1024""><link rel=""canonical"" href=""http://www.spanishdict.com/conjugate/a%c3%b1adir""><script>(function(o){var K=o.$LAB,y=""UseLocalXHR"",z=""AlwaysPreserveOrder"",u=""AllowDuplicates"",A=""CacheBust"",B=""BasePath"",C=/^[^?#]*\//.exec(location.href)[0],D=/^\w+\:\/\/\/?[^\/]+/.exec(C)[0],i=document.head||document.getElementsByTagName(""head""),L=(o.opera&&Object.prototype.toString.call(o.opera)==""[object Opera]"")||(""MozAppearance""in document.documentElement.style),q=document.createElement(""script""),E=typeof q.preload==""boolean"",r=E||(q.readyState&&q.readyState==""uninitialized""),F=!r&&q.async===true,M=!r&&!F&&!L;function G(a){return Object.prototype.toString.call(a)==""[object Function]""}function H(a){return Object.prototype.toString.call(a)==""[object Array]""}function N(a,c){var b=/^\w+\:\/\//;if(/^\/\/\/?/.test(a)){a=location.protocol+a}else if(!b.test(a)&&a.charAt(0)!=""/""){a=(c||"""")+a}return b.test(a)?a:((a.charAt(0)==""/""?D:C)+a)}function s(a,c){for(var b in a){if(a.hasOwnProperty(b)){c[b]=a[b]}}return c}function O(a){var c=false;for(var b=0;b<a.scripts.length;b++){if(a.scripts[b].ready&&a.scripts[b].exec_trigger){c=true;a.scripts[b].exec_trigger();a.scripts[b].exec_trigger=null}}return c}function t(a,c,b,d){a.onload=a.onreadystatechange=function(){if((a.readyState&&a.readyState!=""complete""&&a.readyState!=""loaded"")||c[b])return;a.onload=a.onreadystatechange=null;d()}}function I(a){a.ready=a.finished=true;for(var c=0;c<a.finished_listeners.length;c++){a.finished_listeners[c]()}a.ready_listeners=[];a.finished_listeners=[]}function P(d,f,e,g,h){setTimeout(function(){var a,c=f.real_src,b;if(""item""in i){if(!i[0]){setTimeout(arguments.callee,25);return}i=i[0]}a=document.createElement(""script"");if(f.type)a.type=f.type;if(f.charset)a.charset=f.charset;if(h){if(r){e.elem=a;if(E){a.preload=true;a.onpreload=g}else{a.onreadystatechange=function(){if(a.readyState==""loaded"")g()}}a.src=c}else if(h&&c.indexOf(D)==0&&d[y]){b=new XMLHttpRequest();b.onreadystatechange=function(){if(b.readyState==4){b.onreadystatechange=function(){};e.text=b.responseText+""\n//@ sourceURL=""+c;g()}};b.open(""GET"",c);b.send()}else{a.type=""text/cache-script"";t(a,e,""ready"",function(){i.removeChild(a);g()});a.src=c;i.insertBefore(a,i.firstChild)}}else if(F){a.async=false;t(a,e,""finished"",g);a.src=c;i.insertBefore(a,i.firstChild)}else{t(a,e,""finished"",g);a.src=c;i.insertBefore(a,i.firstChild)}},0)}function J(){var l={},Q=r||M,n=[],p={},m;l[y]=true;l[z]=false;l[u]=false;l[A]=false;l[B]="""";function R(a,c,b){var d;function f(){if(d!=null){d=null;I(b)}}if(p[c.src].finished)return;if(!a[u])p[c.src].finished=true;d=b.elem||document.createElement(""script"");if(c.type)d.type=c.type;if(c.charset)d.charset=c.charset;t(d,b,""finished"",f);if(b.elem){b.elem=null}else if(b.text){d.onload=d.onreadystatechange=null;d.text=b.text}else{d.src=c.real_src}i.insertBefore(d,i.firstChild);if(b.text){f()}}function S(c,b,d,f){var e,g,h=function(){b.ready_cb(b,function(){R(c,b,e)})},j=function(){b.finished_cb(b,d)};b.src=N(b.src,c[B]);b.real_src=b.src+(c[A]?((/\?.*$/.test(b.src)?""&_"":""?_"")+~~(Math.random()*1E9)+""=""):"""");if(!p[b.src])p[b.src]={items:[],finished:false};g=p[b.src].items;if(c[u]||g.length==0){e=g[g.length]={ready:false,finished:false,ready_listeners:[h],finished_listeners:[j]};P(c,b,e,((f)?function(){e.ready=true;for(var a=0;a<e.ready_listeners.length;a++){e.ready_listeners[a]()}e.ready_listeners=[]}:function(){I(e)}),f)}else{e=g[0];if(e.finished){j()}else{e.finished_listeners.push(j)}}}function v(){var e,g=s(l,{}),h=[],j=0,w=false,k;function T(a,c){a.ready=true;a.exec_trigger=c;x()}function U(a,c){a.ready=a.finished=true;a.exec_trigger=null;for(var b=0;b<c.scripts.length;b++){if(!c.scripts[b].finished)return}c.finished=true;x()}function x(){while(j<h.length){if(G(h[j])){try{h[j++]()}catch(err){}continue}else if(!h[j].finished){if(O(h[j]))continue;break}j++}if(j==h.length){w=false;k=false}}function V(){if(!k||!k.scripts){h.push(k={scripts:[],finished:true})}}e={script:function(){for(var f=0;f<arguments.length;f++){(function(a,c){var b;if(!H(a)){c=[a]}for(var d=0;d<c.length;d++){V();a=c[d];if(G(a))a=a();if(!a)continue;if(H(a)){b=[].slice.call(a);b.unshift(d,1);[].splice.apply(c,b);d--;continue}if(typeof a==""string"")a={src:a};a=s(a,{ready:false,ready_cb:T,finished:false,finished_cb:U});k.finished=false;k.scripts.push(a);S(g,a,k,(Q&&w));w=true;if(g[z])e.wait()}})(arguments[f],arguments[f])}return e},wait:function(){if(arguments.length>0){for(var a=0;a<arguments.length;a++){h.push(arguments[a])}k=h[h.length-1]}else k=false;x();return e}};return{script:e.script,wait:e.wait,setOptions:function(a){s(a,g);return e}}}m={setGlobalDefaults:function(a){s(a,l);return m},setOptions:function(){return v().setOptions.apply(null,arguments)},script:function(){return v().script.apply(null,arguments)},wait:function(){return v().wait.apply(null,arguments)},queueScript:function(){n[n.length]={type:""script"",args:[].slice.call(arguments)};return m},queueWait:function(){n[n.length]={type:""wait"",args:[].slice.call(arguments)};return m},runQueue:function(){var a=m,c=n.length,b=c,d;for(;--b>=0;){d=n.shift();a=a[d.type].apply(null,d.args)}return a},noConflict:function(){o.$LAB=K;return m},sandbox:function(){return J()}};return m}o.$LAB=J();(function(a,c,b){if(document.readyState==null&&document[a]){document.readyState=""loading"";document[a](c,b=function(){document.removeEventListener(c,b,false);document.readyState=""complete""},false)}})(""addEventListener"",""DOMContentLoaded"")})(this);</script><script>var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];

googletag.cmd.push(function() {
  googletag.pubads().setTargeting('sd_is_mobile', 'false');
  googletag.pubads().setTargeting('test_group', '99');
  if("""" !== """"){
    googletag.pubads().setTargeting('sd_adtest', '');
  }
  googletag.pubads().disableInitialLoad();
});

//- Index.
var indexUrl = ""http://js.indexww.com/ht/curiosity.js"";

//- Set up sonobi queue for async loading.
var sbi_morpheus = sbi_morpheus || {registration: {}};
sbi_morpheus.cmd = sbi_morpheus.cmd || [];
var sonobiUrl = ""http://mtrx.go.sonobi.com/morpheus.spanishdict.js"";

//- Quirk of openx implementation; different script for mobile than desktop.
var openXUrl = ""//ox-d.spanishdict.servedbyopenx.com/w/1.0/jstag?nc=1027916-SpanishDict"";
if(""false"" === ""true"") {
  openXUrl += ""-Mob"";
}

//- Criteo.
var criteoUrl = 'http://n1.sdcdns.com/js/vendor/criteo-ads.js';

//- Amazon.
var amznUrl = '//c.amazon-adsystem.com/aax2/amzn_ads.js';

//- Sovrn.
var sovrnUrl = 'http://n1.sdcdns.com/js/sovrn.js';
var sovrnAds = sovrnAds || {
  ""id"": ""1000"",
  ""imp"": [
  ],
  ""site"": {
      ""domain"": ""spanishdict.com"",
      ""page"": window.location.pathname
  }
};
var sovrnAuctionData = sovrnAuctionData || {};

//- Yieldbot.
var yieldbotUrl = '//cdn.yldbt.com/js/yieldbot.intent.js';
var yieldbotAds = [];

$LAB
  .script(indexUrl, sonobiUrl, openXUrl, amznUrl, criteoUrl, sovrnUrl, yieldbotUrl)
  .wait(function () {
  
    //- Yieldbot post load setup.
    var yieldbotPubId= false ? ""68cc"" : ""9146"";
    
    yieldbot.pub(yieldbotPubId);
    for (var i = 0; i < yieldbotAds.length; i++) {
      var adUnit = yieldbotAds[i];
      yieldbot.defineSlot(adUnit.name, { sizes: [
        [adUnit.width, adUnit.height]
      ]});
    }
    yieldbot.enableAsync();
    yieldbot.go();
    
    googletag.cmd.push(function() {
      var criteria = yieldbot.getPageCriteria().split(',');
      for(var i = 0; i < criteria.length; i++){
        var yieldbotKey = 'ybot_' + criteria[i].split(':')[0];
        var yieldbotVal = criteria[i].split(':')[2];
        yieldbotVal = (50 * Math.round(yieldbotVal / 50) / 100).toFixed(2);
        googletag.pubads().setTargeting(yieldbotKey, yieldbotVal);
      }
    });
    
    //- Google script must be loaded by dynamic insertion because it
    //- includes document.write calls which will not work in async scripts.
    var gads = document.createElement(""script"");
    gads.async = true;
    gads.type = ""text/javascript"";
    var useSSL = ""https:"" == document.location.protocol;
    gads.src = (useSSL ? ""https:"" : ""http:"") + ""//www.googletagservices.com/tag/js/gpt.js"";
    var node = document.getElementsByTagName(""script"")[0];
    
    //- Arbitrary timeout to give more time for header bidders to return
    //- bids before we load gpt.js.
    var timeoutLength = 500;
    
    setTimeout(function(){
      node.parentNode.insertBefore(gads, node);
    }, timeoutLength);
    
    googletag.cmd.push(function() {
      //- Calls googletag.pubads().refresh().
      sbi_morpheus.callOperator();
    });
    
    //- Amazon post load setup.
    try {
      amznads.getAdsCallback('3286', function(){
        amznads.setTargetingForGPTAsync('amznslots');
      });
    } catch (e) { /* Ignore. */ }
    
  });
  </script><script>var adDimensions = [728, 90];
if(""adTop"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_728_90_HEADER"", adDimensions, ""adTop"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adTop""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adTop"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adTop"",
  ""banner"": {
    ""w"": ""728"",
    ""h"": ""90""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adTop""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adTop""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adTop""],
    width: 728,
    height: 90
  });
};</script><script>var adDimensions = [728, 90];
if(""adBot"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_728_90_FOOTER"", adDimensions, ""adBot"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adBot""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adBot"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adBot"",
  ""banner"": {
    ""w"": ""728"",
    ""h"": ""90""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adBot""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adBot""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adBot""],
    width: 728,
    height: 90
  });
};</script><script>var adDimensions = [300, 250];
if(""adSide1"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_300_250_SIDEBAR"", adDimensions, ""adSide1"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adSide1""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adSide1"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adSide1"",
  ""banner"": {
    ""w"": ""300"",
    ""h"": ""250""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adSide1""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adSide1""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adSide1""],
    width: 300,
    height: 250
  });
};</script><script>var adDimensions = [300, 250];
if(""adSide2"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_300_250_SIDEBAR_2"", adDimensions, ""adSide2"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adSide2""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adSide2"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adSide2"",
  ""banner"": {
    ""w"": ""300"",
    ""h"": ""250""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adSide2""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adSide2""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adSide2""],
    width: 300,
    height: 250
  });
};</script><script>var adDimensions = [160, 600];
if(""adSideLong"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_160_600_SIDEBAR"", adDimensions, ""adSideLong"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adSideLong""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adSideLong"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adSideLong"",
  ""banner"": {
    ""w"": ""160"",
    ""h"": ""600""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adSideLong""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adSideLong""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adSideLong""],
    width: 160,
    height: 600
  });
};</script><script>var adDimensions = [300, 250];
if(""adContent"" === ""adHesion"") {
  adDimensions = [adDimensions, [1, 1]];
}

(function(adDimensions) {
  googletag.cmd.push(function() {
    googletag.defineSlot(""/1027916/""+""BSM_300_250_CONTENT"", adDimensions, ""adContent"").addService(googletag.pubads());
  });
})(adDimensions);

//- Register ads for Sonobi.
var SD_SONOBI_MAPPINGS;
if(false) {
  SD_SONOBI_MAPPINGS = {
    ""adTop"": ""69edf6046870456f1f5c"",
    ""adMiddle"": ""e03e03ff332ca6a08e6a"",
    ""adBot"": ""3f79b8e08e9284acd186"",
    ""adHesion"": ""30527c2377694ff84760""
  };
} else {
  SD_SONOBI_MAPPINGS = {
    ""adSideLong"": ""533144e843156b8400b1"",
    ""adSide1"": ""56dc3bb5f6c8e8dfe346"",
    ""adSide2"": ""9999176c8b412a283a3c"",
    ""adContent"": ""43b92720a5ba058f12ff"",
    ""adTop"": ""f975d667234a26f83004"",
    ""adBot"": ""7c4e209f4fabee27e9e1""
  };
}
var sonobiId = SD_SONOBI_MAPPINGS[""adContent""];
if(sonobiId) {
  sbi_morpheus.cmd.push(function() {
    sbi_morpheus.register(""adContent"", sonobiId);
  });
}

//- Register ads for Sovrn.
var SD_SOVRN_MAPPINGS;
if(false) {
  SD_SOVRN_MAPPINGS = {
    ""adHesion"": ""306551"",
    ""adTop"": ""306552"",
    ""adBot"": ""306553"",
    ""adMiddle"": ""307552""
  };
} else {
  SD_SOVRN_MAPPINGS = {
    ""adSide1"": ""306545"",
    ""adSide2"": ""306546"",
    ""adContent"": ""306547"",
    ""adTop"": ""306548"",
    ""adBot"": ""306549"",
    ""adSideLong"": ""306550""
  };
}
sovrnAds.imp.push({
  ""id"": ""adContent"",
  ""banner"": {
    ""w"": ""300"",
    ""h"": ""250""
  },
  ""tagid"": SD_SOVRN_MAPPINGS[""adContent""]
});

//- Register ads for Yieldbot.
var SD_YIELDBOT_MAPPINGS;
if(false) {
  SD_YIELDBOT_MAPPINGS = {
    ""adTop"": ""MLB"",
    ""adBot"": ""mobrec"",
    ""adHesion"": ""adhesion"",
    ""adMiddle"": ""mobrec2""
  };
} else {
  SD_YIELDBOT_MAPPINGS = {
    ""adSideLong"": ""sky"",
    ""adSide1"": ""SB"",
    ""adSide2"": ""SB2"",
    ""adTop"": ""LB""
  };
}
if(SD_YIELDBOT_MAPPINGS[""adContent""]) {
  yieldbotAds.push({
    name: SD_YIELDBOT_MAPPINGS[""adContent""],
    width: 300,
    height: 250
  });
};</script><script>googletag.cmd.push(function() {
  googletag.pubads().enableSingleRequest();
  googletag.enableServices();
});</script><script>(function(global) {
  global.SD_MEGASUGGEST=""false"" === 'true' ? true : false;
  global.SD_EXAMPLES=true;
})(this);</script><link rel=""stylesheet"" href=""http://n1.sdcdns.com/dist/desktop-27c13aa86b1864d16cb9909542b5f38b.gz.css""><!--[if lte IE 8]><link rel=""stylesheet"" type=""text/css"" href=""http://n1.sdcdns.com/dist/desktop-ie8-f7fed1e39cc45ed918639c01178e6dc6.gz.css""><![endif]--></head><body data-datetime=""2015-10-15T07:48:30"" data-is-mobile=""false"" data-asset-host=""http://n1.sdcdns.com"" class=""lang-en js-transition-off-during-page-load""><div class=""header-wrapper""><div class=""header header-container container""><div class=""menu-bar""><div class=""menu-nav menu-collapsed""><div class=""menu-container""><div class=""menu-selector""><div class=""icon-menu-nav-selector""></div><div class=""menu-selector-text"">Menu</div><div class=""menu-collapse""></div></div><div class=""menu""><a href=""http://www.spanishdict.com/""><div class=""menu-item"">Home</div></a><a href=""http://www.spanishdict.com/blog""><div class=""menu-item"">Blog</div></a><a href=""http://www.spanishdict.com/translation""><div class=""menu-item"">Translation</div></a><a href=""http://www.spanishdict.com/conjugation""><div class=""menu-item"">Conjugation</div></a><a href=""http://www.spanishdict.com/learn""><div class=""menu-item"">Learn Spanish</div></a><a href=""http://www.spanishdict.com/flashcards""><div class=""menu-item"">Flashcards</div></a><a href=""http://www.spanishdict.com/grammar""><div class=""menu-item"">Grammar</div></a><a href=""http://www.spanishdict.com/answers""><div class=""menu-item"">Q &amp; A</div></a><a href=""http://www.spanishdict.com/wordoftheday""><div class=""menu-item"">Word of the Day</div></a><a href=""#""><div class=""menu-item menu-divider go-language"">Ver en español</div></a><div class=""menu-fluencia menu-divider""><div class=""menu-fluencia-title"">Want to learn Spanish?</div><div class=""menu-fluencia-text""><span>Try Fluencia, an amazing learning program from SpanishDict.</span></div><a href=""http://www.fluencia.com/learn-spanish/?utm_source=sd_desktop&amp;utm_medium=nav_menu&amp;h=1"" target=""_blank"" data-sd-track-click-type=""fluencia"" class=""menu-fluencia-link sd-track-click""><div class=""menu-fluencia-button"">Yes!</div></a></div></div></div></div><div class=""menu-user menu-collapsed""><div class=""menu-container""><div class=""menu-selector unauthed""><a href=""http://www.spanishdict.com/users/login"" class=""menu-selector-text"">Sign in</a></div></div></div><a href=""http://www.spanishdict.com/"" class=""header-logo""></a></div></div></div><div class=""content-container container""><div class=""ad-wrapper-wide""><div class=""ad-728-90 adTop-results ad-wrapper""><div id=""adTop"" class=""adTop""><script>googletag.cmd.push(function() {
  googletag.display(""adTop"");
});</script></div><div data-target=""adTop"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div></div><div class=""main-container""><div class=""search""><div class=""search-component search-form""><form action=""/conjugation"" method=""post"" accept-charset=""utf-8"" data-action=""conjugation"" class=""page-action search_query_to_url save-history""><span id=""hidden-search""></span><div class=""search-form-parts search-collapse""><input id=""hidden-input"" name=""isExpand"" type=""hidden"" value=""false""><textarea name=""text"" id=""query"" type=""search"" data-suggest-host=""suggest1.spanishdict.com:80"" data-suggest-path=""/dictionary/conjugate_auto_suggest"" data-suggest-variation=""original"" placeholder=""Enter a Spanish verb&hellip;"" autocomplete=""off"" autocorrect=""off"" autocapitalize=""off"" class=""search-form-input"">añadir</textarea><div class=""accents""><span data-toggle=""tooltip"" title=""Add á to your text"" class=""accents-letter"">á</span><span data-toggle=""tooltip"" title=""Add é to your text"" class=""accents-letter"">é</span><span data-toggle=""tooltip"" title=""Add í to your text"" class=""accents-letter"">í</span><span data-toggle=""tooltip"" title=""Add ó to your text"" class=""accents-letter"">ó</span><span data-toggle=""tooltip"" title=""Add ú to your text"" class=""accents-letter"">ú</span><span data-toggle=""tooltip"" title=""Add ñ to your text"" class=""accents-letter"">ñ</span><span data-toggle=""tooltip"" title=""Add ¿ to your text"" class=""accents-letter accents-descender"">¿</span><span data-toggle=""tooltip"" title=""Add ¡ to your text"" class=""accents-letter accents-descender"">¡</span><span data-toggle=""tooltip"" title=""Hear audio of the words"" class=""accents-audio-btn"">&nbsp;</span></div><div data-toggle=""tooltip"" title=""Show the accent buttons and expand the search box."" class=""search-expand-icon""></div><div data-toggle=""tooltip"" title=""Collapse the search box"" class=""search-collapse-icon""></div><button type=""submit"" class=""search-form-button"">Conjugate</button></div></form></div></div><div class=""conjugation card""><div class=""quickdef""><div class=""source""><h1 class=""source-text"">añadir</h1><a href=""http://audio1.spanishdict.com/audio?lang=es&amp;text=a%C3%B1adir"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start""><span class=""audio-blue"">&nbsp;</span></a><a href=""http://www.spanishdict.com/videos/añadir""><span data-toggle=""tooltip"" title=""Watch a video translation"" class=""video-blue"">&nbsp;</span></a></div><div class=""lang""><div class=""el""><a href=""http://www.spanishdict.com/translate/to"">to</a> <a href=""http://www.spanishdict.com/translate/add"">add</a></div><span class=""media-links""><a href=""http://audio1.spanishdict.com/audio?lang=en&amp;text=to-add"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start""><span class=""audio-blue"">&nbsp;</span></a></span></div></div><div class=""nav-content""><div class=""nav-content-item""><a href=""http://www.spanishdict.com/translate/a%C3%B1adir""><span class=""nav-content-item-text"">Dictionary</span></a></div><div class=""nav-content-item nav-active""><a><span class=""nav-content-item-text"">Conjugation</span></a></div><div class=""nav-content-item""><a href=""http://www.spanishdict.com/examples/a%C3%B1adir""><span class=""nav-content-item-text"">Examples</span></a><span class=""nav-content-item-label"">new</span></div><div class=""nav-content-item""><a href=""http://www.spanishdict.com/videos/a%C3%B1adir""><span class=""nav-content-item-text"">Video</span></a></div><div class=""social-small""><a target=""_blank"" href=""https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.spanishdict.com%2Ftranslate%2Fa%C3%B1adir%3Futm_source%3Dsocial%26utm_medium%3Dfacebook%26utm_campaign%3Dshare"" class=""social-small-icon social-small-facebook""></a><a target=""_blank"" href=""https://twitter.com/intent/tweet?text=Check%20out%20the%20translation%20for%20%22a%C3%B1adir%22%20on%20%40SpanishDict!&amp;amp;url=http%3A%2F%2Fspanishdict.com%2ftranslate%2Fa%25C3%25B1adir%3Futm_source%3Dsocial%26utm_medium%3Dtwitter%26utm_campaign%3Dshare"" class=""social-small-icon social-small-twitter""></a><a target=""_blank"" href=""https://plus.google.com/share?url=http%3A%2F%2Fspanishdict.com%2Ftranslate%2Fa%25C3%25B1adir%3Futm_source%3Dsocial%26utm_medium%3Dgoogleplus%26utm_campaign%3Dshare"" class=""social-small-icon social-small-gplus""></a></div></div><div class=""conj-row""><a href=""http://www.spanishdict.com/answers/100043/gerund-form"" target=""_blank"" class=""conj-basic-label"">Gerund:</a><span>&nbspañadiendo</span></div><div class=""conj-row""><a href=""http://www.spanishdict.com/answers/100044/past-participles"" target=""_blank"" class=""conj-basic-label"">Participle:</a><span>&nbspañadido</span></div><a href=""http://www.spanishdict.com/topics/show/112"" class=""vtable-label"">Indicative<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100045/present-tense"" title=""Tense example: *He speaks* Spanish."" data-toggle=""tooltip"" class=""vtable-title-link"">Present<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100046/preterite-simple-past"" title=""Tense example: *He spoke* to my sister yesterday."" data-toggle=""tooltip"" class=""vtable-title-link"">Preterite<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100048/imperfect-past"" title=""Tense example: *He used to speak* Italian when he was a child."" data-toggle=""tooltip"" class=""vtable-title-link"">Imperfect<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100050/conditional"" title=""Tense example: *He would speak* French in France."" data-toggle=""tooltip"" class=""vtable-title-link"">Conditional<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100049/future"" title=""Tense example: *He will speak* Portuguese on his vacation next week."" data-toggle=""tooltip"" class=""vtable-title-link"">Future<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">añado</td><td class=""vtable-word  "">añadí</td><td class=""vtable-word  "">añadía</td><td class=""vtable-word  "">añadiría</td><td class=""vtable-word  "">añadiré</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">añades</td><td class=""vtable-word  "">añadiste</td><td class=""vtable-word  "">añadías</td><td class=""vtable-word  "">añadirías</td><td class=""vtable-word  "">añadirás</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">añade</td><td class=""vtable-word  "">añadió</td><td class=""vtable-word  "">añadía</td><td class=""vtable-word  "">añadiría</td><td class=""vtable-word  "">añadirá</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">añadimos</td><td class=""vtable-word  "">añadimos</td><td class=""vtable-word  "">añadíamos</td><td class=""vtable-word  "">añadiríamos</td><td class=""vtable-word  "">añadiremos</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">añadís</td><td class=""vtable-word  "">añadisteis</td><td class=""vtable-word  "">añadíais</td><td class=""vtable-word  "">añadiríais</td><td class=""vtable-word  "">añadiréis</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">añaden</td><td class=""vtable-word  "">añadieron</td><td class=""vtable-word  "">añadían</td><td class=""vtable-word  "">añadirían</td><td class=""vtable-word  "">añadirán</td></tr></table></div><a href=""http://www.spanishdict.com/topics/show/68"" class=""vtable-label"">Subjunctive<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100057/present-subjunctive"" title=""Tense example: It is good that *he speak* to your mother."" data-toggle=""tooltip"" class=""vtable-title-link"">Present<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100058/imperfect-subjunctive"" title=""Tense example: It made me happy that *he spoke* at the wedding."" data-toggle=""tooltip"" class=""vtable-title-link"">Imperfect<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100058/imperfect-subjunctive"" title=""Tense example: It made me happy that *he spoke* at the wedding."" data-toggle=""tooltip"" class=""vtable-title-link"">Imperfect 2<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100059/future-subjunctive"" title=""Tense example: It is possible that *he will speak* to your father."" data-toggle=""tooltip"" class=""vtable-title-link"">Future<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">añada</td><td class=""vtable-word  "">añadiera</td><td class=""vtable-word  "">añadiese</td><td class=""vtable-word  "">añadiere</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">añadas</td><td class=""vtable-word  "">añadieras</td><td class=""vtable-word  "">añadieses</td><td class=""vtable-word  "">añadieres</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">añada</td><td class=""vtable-word  "">añadiera</td><td class=""vtable-word  "">añadiese</td><td class=""vtable-word  "">añadiere</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">añadamos</td><td class=""vtable-word  "">añadiéramos</td><td class=""vtable-word  "">añadiésemos</td><td class=""vtable-word  "">añadiéremos</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">añadáis</td><td class=""vtable-word  "">añadierais</td><td class=""vtable-word  "">añadieseis</td><td class=""vtable-word  "">añadiereis</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">añadan</td><td class=""vtable-word  "">añadieran</td><td class=""vtable-word  "">añadiesen</td><td class=""vtable-word  "">añadieren</td></tr></table></div><a href=""http://www.spanishdict.com/topics/show/113"" class=""vtable-label"">Imperative<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100063/imperative"" title=""Tense example: *Speak!*."" data-toggle=""tooltip"" class=""vtable-title-link"">Imperative<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">-</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">añade</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">añada</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">añadamos</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">añadid</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">añadan</td></tr></table></div><a href=""http://www.spanishdict.com/topics/show/35"" class=""vtable-label"">Perfect<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100051/present-perfect"" title=""Tense example: *He has spoken* at several meetings."" data-toggle=""tooltip"" class=""vtable-title-link"">Present<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/173873/preterit-perfect"" title=""Tense example: *He had (already) spoken* to her when I got there."" data-toggle=""tooltip"" class=""vtable-title-link"">Preterite<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100052/past-perfect"" title=""Tense example: *He had spoken* a lot before the train left."" data-toggle=""tooltip"" class=""vtable-title-link"">Past<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100054/conditional-perfect"" title=""Tense example: *He would have spoken*, but he was sick."" data-toggle=""tooltip"" class=""vtable-title-link"">Conditional<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100053/future-perfect"" title=""Tense example: *He will have spoken* in three different countries by tomorrow."" data-toggle=""tooltip"" class=""vtable-title-link"">Future<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">he añadido</td><td class=""vtable-word  "">hube añadido</td><td class=""vtable-word  "">había añadido</td><td class=""vtable-word  "">habría añadido</td><td class=""vtable-word  "">habré añadido</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">has añadido</td><td class=""vtable-word  "">hubiste añadido</td><td class=""vtable-word  "">habías añadido</td><td class=""vtable-word  "">habrías añadido</td><td class=""vtable-word  "">habrás añadido</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">ha añadido</td><td class=""vtable-word  "">hubo añadido</td><td class=""vtable-word  "">había añadido</td><td class=""vtable-word  "">habría añadido</td><td class=""vtable-word  "">habrá añadido</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">hemos añadido</td><td class=""vtable-word  "">hubimos añadido</td><td class=""vtable-word  "">habíamos añadido</td><td class=""vtable-word  "">habríamos añadido</td><td class=""vtable-word  "">habremos añadido</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">habéis añadido</td><td class=""vtable-word  "">hubisteis añadido</td><td class=""vtable-word  "">habíais añadido</td><td class=""vtable-word  "">habríais añadido</td><td class=""vtable-word  "">habréis añadido</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">han añadido</td><td class=""vtable-word  "">hubieron añadido</td><td class=""vtable-word  "">habían añadido</td><td class=""vtable-word  "">habrían añadido</td><td class=""vtable-word  "">habrán añadido</td></tr></table></div><a href=""http://www.spanishdict.com/topics/show/102"" class=""vtable-label"">Perfect Subjunctive<span class=""icon-question-mark""></span></a><div class=""vtable-wrapper""><table class=""vtable""><tr><td class=""vtable-empty""></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100060/present-perfect-subjunctive"" title=""Tense example: I doubt that *he has spoken* to the president."" data-toggle=""tooltip"" class=""vtable-title-link"">Present<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100061/past-perfect-subjunctive"" title=""Tense example: She did not believe that *he had spoken* to a unicorn."" data-toggle=""tooltip"" class=""vtable-title-link"">Past<span class=""icon-question-mark""></span></a></td><td class=""vtable-title""><a href=""http://www.spanishdict.com/answers/100062/future-perfect-subjunctive"" title=""Tense example: It is possible that *he will have spoken* to the whole family."" data-toggle=""tooltip"" class=""vtable-title-link"">Future<span class=""icon-question-mark""></span></a></td></tr><tr><td class=""vtable-pronoun"">yo</td><td class=""vtable-word  "">haya añadido</td><td class=""vtable-word  "">hubiera añadido</td><td class=""vtable-word  "">hubiere añadido</td></tr><tr><td class=""vtable-pronoun"">tú</td><td class=""vtable-word  "">hayas añadido</td><td class=""vtable-word  "">hubieras añadido</td><td class=""vtable-word  "">hubieres añadido</td></tr><tr><td class=""vtable-pronoun"">él/ella/Ud.</td><td class=""vtable-word  "">haya añadido</td><td class=""vtable-word  "">hubiera añadido</td><td class=""vtable-word  "">hubiere añadido</td></tr><tr><td class=""vtable-pronoun"">nosotros</td><td class=""vtable-word  "">hayamos añadido</td><td class=""vtable-word  "">hubiéramos añadido</td><td class=""vtable-word  "">hubiéremos añadido</td></tr><tr><td class=""vtable-pronoun"">vosotros</td><td class=""vtable-word  "">hayáis añadido</td><td class=""vtable-word  "">hubierais añadido</td><td class=""vtable-word  "">hubiereis añadido</td></tr><tr><td class=""vtable-pronoun"">ellos/ellas/Uds.</td><td class=""vtable-word  "">hayan añadido</td><td class=""vtable-word  "">hubieran añadido</td><td class=""vtable-word  "">hubieren añadido</td></tr></table></div><div class=""conj-footnote"">This verb cannot be reflexive.</div><div class=""conj-footnote"">This verb is recognized by the Real Academia Española.</div><div class=""conj-footnote"">This data is provided by Onoma.</div></div><div class=""card""><div class=""card-title"">Search history</div><div class=""search-history-items""></div></div><div class=""ad-300-250 adContent-results ad-wrapper""><div id=""adContent"" class=""adContent""><script>googletag.cmd.push(function() {
  googletag.display(""adContent"");
});</script></div><div data-target=""adContent"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div><div id=""feedback-query"" class=""card""><div class=""card-title"">Did this page answer your question?</div><form><div class=""feedback-1""><a href=""#"" class=""feedback-yes""><div class=""icon-smile""></div><span>Yes</span></a><a href=""#"" class=""feedback-no""><div class=""icon-frown""></div><span>No</span></a></div><div class=""feedback-2""><div id=""feedback-categories""><div class=""feedback-instructions"">The page is...</div><div class=""feedback-categories-select""><label><input type=""radio"" name=""category"" value=""inaccurate"">Inaccurate</label><label><input type=""radio"" name=""category"" value=""unclear"">Unclear</label><label><input type=""radio"" name=""category"" value=""missingTranslations"">Missing translations</label><label><input type=""radio"" name=""category"" value=""missingConjugations"">Missing conjugations</label><label><input type=""radio"" name=""category"" value=""other"">Other</label></div></div><div class=""feedback-instructions"">What can we do to improve?</div><textarea id=""feedback-comment"" placeholder=""Enter your comment..."" required class=""feedback-comment""></textarea><div class=""feedback-instructions"">How can we follow up with you? (Optional)</div><input id=""feedback-query-email"" type=""email"" placeholder=""Enter your email"" class=""feedback-query-email""><button type=""submit"" class=""feedback-2-submit"">Submit</button></div><div class=""feedback-3""><span>Your feedback was sent. We appreciate your comments!</span></div></form></div></div><div class=""sidebar-container""><div class=""fluencia-sidebar card""><div class=""card-title"">Want to speak Spanish?</div><div class=""fluencia-sidebar-text"">Try Fluencia, an amazing learning program from SpanishDict.</div><a href=""http://www.fluencia.com/learn-spanish/?utm_source=sd_desktop&amp;utm_medium=sidebar&amp;h=2"" target=""_blank"" data-sd-track-click-type=""fluencia"" class=""fluencia-sidebar-button sd-track-click"">Join now</a></div><div class=""ad-300-250 adSide-results sidebar-box ad-wrapper""><div id=""adSide1"" class=""adSide1""><script>googletag.cmd.push(function() {
  googletag.display(""adSide1"");
});</script></div><div data-target=""adSide1"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div><div class=""wotd-sidebar card""><div class=""card-title"">Word of the Day</div><div class=""wotd-sidebar-date"">Oct 15</div><a href=""http://www.spanishdict.com/wordoftheday/2544/agregar"" class=""wotd-sidebar-word"">agregar</a><a href=""http://audio1.spanishdict.com/audio?lang=es&amp;text=agregar"" data-toggle=""tooltip"" title=""Listen to an audio pronunciation"" class=""audio-start""><span class=""audio-blue"">&nbsp;</span></a><div class=""wotd-sidebar-translation"">to add; to gather</div><div class=""wotd-sidebar-prompt"">Get the free word of the day!</div><form action=""/users/signup_email_verify"" method=""post"" class=""wotd-sidebar-form""><input name=""email"" type=""email"" placeholder=""email"" class=""wotd-sidebar-input""><button type=""submit"" data-sd-track-click-type=""wotd"" class=""wotd-sidebar-button sd-track-click"">Subscribe</button></form></div><div class=""ad-300-250 adSide-results sidebar-box ad-wrapper""><div id=""adSide2"" class=""adSide2""><script>googletag.cmd.push(function() {
  googletag.display(""adSide2"");
});</script></div><div data-target=""adSide2"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div><div class=""social-sidebar card""><div class=""card-title"">Stay Connected</div><a href=""https://www.facebook.com/pages/SpanishDict/92805940179"" target=""_blank"" data-sd-track-click-type=""social"" data-sd-track-click-text=""facebook"" class=""social-sidebar-button facebook sd-track-click""><div class=""social-logo social-facebook-logo""></div></a><a href=""https://plus.google.com/106413713006018165072/posts"" target=""_blank"" data-sd-track-click-type=""social"" data-sd-track-click-text=""google"" class=""social-sidebar-button gplus sd-track-click""><div class=""social-logo social-gplus-logo""></div></a><a href=""https://twitter.com/spanishdict"" target=""_blank"" data-sd-track-click-type=""social"" data-sd-track-click-text=""twitter"" class=""social-sidebar-button twitter sd-track-click""><div class=""social-logo social-twitter-logo""></div></a></div><div class=""ad-160-600 adSide-long-results sidebar-box-long ad-wrapper""><div id=""adSideLong"" class=""adSideLong""><script>googletag.cmd.push(function() {
  googletag.display(""adSideLong"");
});</script></div><div data-target=""adSideLong"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div></div><div class=""ad-wrapper-wide""><div class=""ad-728-90 adBot-results ad-wrapper""><div id=""adBot"" class=""adBot""><script>googletag.cmd.push(function() {
  googletag.display(""adBot"");
});</script></div><div data-target=""adBot"" style=""visibility: hidden; color: #555555 !important"" data-sd-track-click-type=""pubnation"" data-sd-track-click-text=""report this ad"" class=""pubnation sd-track-click""><img src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAAAXNSR0IArs4c6QAABERJREFUSA2tlltoXFUUhjOXRDOxoRAlBScUoRKs2ATxpRIfpBSVUs2NtIwQCdQG+pCiUh+kl/ECbZ/EPAhRK0JjmoYkE8EHhbZKlRZEREqlFmKhN8SH6ODUJs1MZvz+nbOGk+RMMilZsM5ee93+vfb1hCqWoYGBgcq6urqnw+Hws/ATuDbCDfC6QqFQq9BQKPQvTQa+CV/N5/NX4AtTU1O/9Pb2ZtEFUmixNpVKrSdpGwnF27DHFvuU2b9LnrNwijyptra2tD+uCDw6OvpMJBLZj1MnDg/6ndZAnmEAo3Nzcx91dnb+rHwOeGJi4hvkF0oBEHSdAV3UNDLlk/hNwZpe0Tq4DtsmLQe+W/Hd6CzBn29bW1tfjHq2JaAk+BEehr/u6Oi4HpwjWDs2NraRQezEuotBtPi9yLddfas478kFRv5JLpf7sKur66o/4H7lkZGRxmg0+gYD2SsMgLOsd5UBa/ep+gzT4Hbr/QKVimM5tfu1LDNgVIc9x5zXWt/rrl1Dpa5IMroj5tYYZY61EEqkHCiO3Pv475EvsZ8xdYdWisPfFaWplq9tLqvY+ivleRyHDZ6T5HLIFcUAZuXsRkFnxossq+JyUAJ8XG4qdlhW4bTnGOrv73+gr6/vnj+QnfkoO3MfO9Oma4vPvoWNc1R9TkSeE/ExJ+K2z15BvEAXYLkOo/ibqh+Tczwef4hmAXBVVZXO4juyi/CdF+Zl3eHiCo0L30uIp9U3mp2drUFv3X8k2FTfMC1J9QisKQEat4Tkd1hW/qQZqP5J5F+tr5Y79jzVJAlyA0XVActP9Bs8JoFYzfZ5yX7iDdhsfezXJNtUnyHpASkAeInmS8lGXJl/Ir9rfY5TI/4OGLDLHKcjZgtqvZzORNw5Ca6CdDqtUd6VgkQ7BgcH1+z24k2PAfayl/tONpv9QbID7unp0bM1LAVO62tqavZLXoauYLvhseSSVF9fvw/jw57DaXa8O8fF7cmW31RZWfk7wBEGkWYtnmKKb5XMWIZhaGioPhaLXRYwOedoG1mWPxTqKpbASCYBdWurqlmXU8lk0jafXFZFxIarq6tPEmTVDhqoEhWB1aHKN2nc4Qe8pampqV8JZFstNTc3HyPHdi/uNhW/5c9RnGpT8og/T7Xa5Q6QgK8ymcyr3d3d/5nPcq1uvoaGhs+JT8iPeL3126j2e/WNllTDun6HsXg8SPBKbW3tT1yLS/5SLIm1+DwH6AUDlR7gw4tBpV9SsZQi7+k7ON+b/5JEf43H29vbz6ApmG18fLwFsLdh/e4UCd8PAA18MksCKxrwLpJ9irjgXJPwGvpTtHppEsjurlaMR/rb2MufxoI724xqlwWWA8fsEe7a9wB5HQD3tEkfRPjoyJyYnp4+nEgk/gryMd2KwObIADYzgCTJ2xcPAJ020ATtEZZB53ZFKhvYMrHr4wC/Bu+Go4ANcwy/WO0v8P8H+qpWV2S+gQAAAABJRU5ErkJggg=="" style=""height: 13px; width: 13px; vertical-align: middle; padding-right: 3px;"">report this ad</div></div></div></div><div class=""footer-wrapper""><div class=""footer footer-container container""><div class=""footer-row""><div class=""footer-item""><h4>Stay Connected</h4><div class=""footer-social""><div class=""social""><a href=""http://www.facebook.com/pages/SpanishDict/92805940179""><div class=""social-icon social-facebook""></div></a><a href=""http://www.twitter.com/spanishdict""><div class=""social-icon social-twitter""></div></a><a href=""https://plus.google.com/106413713006018165072/posts""><div class=""social-icon social-gplus""></div></a></div></div></div><div class=""footer-item""><h4>iPhone</h4><a href=""http://itunes.apple.com/us/app/spanishdict/id332510494""><div class=""footer-external footer-external-appstore""></div></a></div><div class=""footer-item""><h4>Android</h4><a href=""https://play.google.com/store/apps/details?id=com.spanishdict.spanishdict&amp;referrer=utm_source%3Dsd-footer""><div class=""footer-external footer-external-google-play""></div></a></div><div class=""footer-item footer-item-last""><h4>Kindle</h4><a href=""http://www.amazon.com/SpanishDict-Spanish-English-Dictionary-and-More/dp/B004UO8ZNQ""><div class=""footer-external footer-external-amazon""></div></a></div></div><div class=""footer-row""><div class=""footer-item""><h4>Spanish Reference</h4><ul><li><a href=""http://www.spanishdict.com/translation"">English to Spanish translation</a></li><li><a href=""http://www.spanishdict.com/translation"">Spanish to English translation</a></li><li><a href=""http://www.spanishdict.com/dictionary"">English to Spanish dictionary</a></li><li><a href=""http://www.spanishdict.com/dictionary"">Spanish to English dictionary</a></li></ul></div><div class=""footer-item""><h4>Learn Spanish</h4><ul><li><a href=""http://www.spanishdict.com/learn"">Learn Spanish</a></li><li><a href=""http://www.spanishdict.com/flashcards"">Spanish Flashcards</a></li><li><a href=""http://www.spanishdict.com/conjugation"">Spanish Verb Conjugation</a></li><li><a href=""http://www.spanishdict.com/answers"">Spanish-English Q&amp;A</a></li></ul></div><div class=""footer-item""><h4>Popular Translations</h4><ul><li><a href=""http://www.spanishdict.com/translate/yes"">yes in Spanish</a></li><li><a href=""http://www.spanishdict.com/translate/I miss you"">I miss you in Spanish</a></li><li><a href=""http://www.spanishdict.com/translate/beautiful"">beautiful in Spanish</a></li><li><a href=""http://www.spanishdict.com/translate/stop"">stop in Spanish</a></li></ul></div><div class=""footer-item footer-item-last""><h4>Popular Conjugations</h4><ul><li><a href=""http://www.spanishdict.com/conjugate/ser"">ser conjugation</a></li><li><a href=""http://www.spanishdict.com/conjugate/saber"">saber conjugation</a></li><li><a href=""http://www.spanishdict.com/conjugate/ir"">ir conjugation</a></li><li><a href=""http://www.spanishdict.com/conjugate/hacer"">hacer conjugation</a></li></ul></div></div><div class=""footer-meta""><div class=""footer-meta-list""><ul><li><a href=""http://www.spanishdict.com/company/about"">About</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""http://www.fluencia.com/about-us/careers/"">We're hiring!</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""http://www.spanishdict.com/company/privacy"">Privacy</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""http://www.spanishdict.com/company/tos"">Terms</a><span class=""footer-meta-list-item-bg""></span></li><li><a href=""#"" class=""go-language"">Español</a></li></ul></div><span class=""footer-copyright"">&copy; Curiosity Media, Inc.</span><div class=""footer-exclamation-logo-wrapper""><div class=""footer-exclamation-container""><img src=""http://n1.sdcdns.com/img/desktop/logoAnimation.gif"" class=""footer-exclamation-logo""></div></div></div></div></div><div class=""feedback-tab""><button data-toggle=""modal"" data-target=""#feedback-modal"">Feedback</button></div><div id=""feedback-modal"" class=""feedback-modal sd-modal modal fade""><div class=""modal-dialog""><div class=""modal-content""><div class=""modal-body""><div class=""sd-modal-title"">Feedback</div><div class=""sd-modal-close feedback-modal-close""></div><div class=""sd-modal-message feedback-modal-message""></div><div class=""sd-modal-content feedback-modal-content""><div class=""sd-modal-desc"">SpanishDict is devoted to improving our site based on user feedback and introducing new and innovative features that will continue to help people learn and love the Spanish language.&nbsp;Have a suggestion, idea, or comment?&nbsp;Send us your feedback.</div><form><label for=""feedback-name"" class=""sd-modal-label"">Your name</label><input id=""feedback-name"" type=""text"" placeholder=""Your name""><label for=""feedback-email"" class=""sd-modal-label"">Email address</label><input id=""feedback-email"" type=""email"" placeholder=""Email address""><label for=""feedback-subject"" class=""sd-modal-label"">Subject</label><input id=""feedback-subject"" type=""text"" placeholder=""Subject"" required><label for=""feedback-message"" class=""sd-modal-label"">Message</label><textarea id=""feedback-message"" placeholder=""Message"" rows=""5"" required></textarea><input type=""submit"" value=""Send"" class=""sd-modal-submit feedback-modal-submit""></form></div></div></div></div></div><div id=""survey-modal"" class=""survey-modal modal""><div class=""modal-dialog""><div class=""modal-content""><div class=""modal-body""><div class=""survey-modal-icon""></div><div class=""survey-modal-title"">Make Your Voice Heard</div><div id=""survey-modal-close"" class=""survey-modal-close""></div><div class=""survey-modal-message""></div><div class=""survey-modal-content""><div class=""survey-modal-desc"">You've been randomly selected to participate in a short, 5 question
survey on your experience with SpanishDict. We'll use your comments
to help improve your experience.<br>Will you participate?</div><a id=""survey-modal-submit"" href=""#"" target=""_blank"" class=""survey-modal-submit"">Take 5 Question Survey</a></div></div></div></div></div><script id=""sd_analytics"" type=""application/json"">{&quot;site&quot;:&quot;desktop&quot;,&quot;q&quot;:&quot;añadir&quot;,&quot;type&quot;:&quot;conjugation&quot;,&quot;is_found&quot;:1,&quot;browser_lang&quot;:&quot;en-GB&quot;}</script><script src=""http://n1.sdcdns.com/dist/vendor/jquery-min-1-11-1-8101d596b2b8fa35fe3a634ea342d7c3.gz.js"" type=""text/javascript"" crossorigin=""anonymous""></script><script src=""http://n1.sdcdns.com/dist/desktop-min-bce67d1edeeab5cafef6722f110d944a.gz.js"" type=""text/javascript"" crossorigin=""anonymous""></script><!--[if lte IE 9]><script>(function() {
  var app = document.createElement('script'); app.type = 'text/javascript'; app.async = true;
  app.src = 'http://n1.sdcdns.com/js/vendor/placeholders.min.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(app, s);
})();</script><![endif]--> <script type=""text/javascript"">
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');   ga('set', 'user_status1', 'anonymous');   ga('set', 'section2', 'conjugate');   ga('set', 'sd_is_mobile3', 'false');
   ga('require', 'displayfeatures');
   ga('create', 'UA-329211-2', 'auto');
   ga('send', 'pageview');
 </script><script>var _pnq = _pnq || [];
_pnq.push(['setId', 'pn-053b5b698be780ef']);
(function() {
  var pnr = document.createElement('script');
  pnr.type = 'text/javascript';
  pnr.async = true;
  pnr.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'report-ads-to.pubnation.com/dist/pnr.js?t=pn-053b5b698be780ef';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(pnr, s);
})();</script><script>(function() {
  if(SD_ENV != ""production"") return;
  function async_load(script_url){
    var protocol = ('https:' == document.location.protocol ? 'https://' : 'http://');
    var s = document.createElement('script'); s.src = protocol + script_url;
    var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
  }
  bm_website_code = '7BF624AB557B448F';
  jQuery(document).ready(function(){async_load('asset.pagefair.com/measure.min.js')});
  jQuery(document).ready(function(){async_load('asset.pagefair.net/ads.min.js')});
})();</script></body></html>";

















    }
}



